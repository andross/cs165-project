#include "client_context.h"

ClientContext* init_client_context(void) {
  ClientContext* c = malloc(sizeof(ClientContext));
  c->variable_count = 0;
  c->variable_capacity = DEFAULT_VARIABLE_CAPACITY;
  c->variables = malloc(c->variable_capacity * sizeof(ClientVariable*));
  c->current_batch = NULL;
  return c;
}

Result* get_client_variable(ClientContext* c, char* variable_name) {
  for (int i = (int)c->variable_count - 1; i >= 0; i--) // newest first
    if (strcmp(c->variables[i]->name, variable_name) == 0)
      return c->variables[i]->value;
  return NULL;
}

int has_client_variable(ClientContext* c, char* variable_name) {
  for (size_t i = 0; i < c->variable_count; i++)
    if (strcmp(c->variables[i]->name, variable_name) == 0)
      return 1;
  return 0;
}

void set_client_variable(ClientContext* c, char* name, Result* res) {
  ClientVariable* var = malloc(sizeof(ClientVariable));
  strcpy(var->name, name);
  var->value = res;
  add_client_variable(c, var);
}

void add_client_variable(ClientContext* c, ClientVariable* variable) {
  if (c->variable_count >= c->variable_capacity) {
    log_info("Resizing variables array\n");
    c->variable_capacity *= 2;
    for (size_t i = 0; i < c->variable_count; i++) {
      ClientVariable** more = malloc(c->variable_capacity * sizeof(ClientVariable*));
      memcpy(more, c->variables, c->variable_count * sizeof(ClientVariable*));
      free(c->variables);
      c->variables = more;
    }
  }

  c->variables[c->variable_count++] = variable;
}

void free_client_variable(ClientVariable* v) {
  free(v->value->values);
  free(v->value);
  free(v);
}

void free_client_context(ClientContext* c) {
  for (size_t i = 0; i < c->variable_count; i++)
    free_client_variable(c->variables[i]);
  free(c->variables);
  free(c);
}
