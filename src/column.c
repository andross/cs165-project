#include "column.h"

Column* init_column(char* column_name, size_t row_capacity) {
  Column* column = malloc(sizeof(Column));
  strcpy(column->name, column_name);
  column->data = malloc(row_capacity * sizeof(int));
  column->length = 0;
  column->is_sorted = 0;
  column->sorted_until = 0;
  column->btree = NULL;
  return column;
}

Column* replicate_column(Column* column, size_t row_capacity) {
  Column* replica = init_column(column->name, row_capacity);
  replica->length = column->length;
  memcpy(replica->data, column->data, column->length * sizeof(int));
  return replica;
}

Column* load_column(char* table_dir, char* column_name, size_t row_count, size_t row_capacity, int is_sorted, int has_btree) {
  Column* column = init_column(column_name, row_capacity);
  column->length = row_count;
  column->is_sorted = is_sorted;
  if (is_sorted)
    column->sorted_until = row_count;

  char filename[1024];
  filename_join(filename, 3, table_dir, column_name, "data");

  FILE* file = fopen(filename, "r");
  fread(column->data, sizeof(int), row_count, file);
  fclose(file);

  if (has_btree) {
    char index_fname[1024];
    filename_join(index_fname, 3, table_dir, column_name, "index");
    column->btree = btree_load(index_fname);
  }

  return column;
}

void rebuild_column_index(Column* column) {
  if (column->btree != NULL) {
    btree_free(column->btree);
    column->btree = btree_bulk_load(column->data, column->length);
  }
}

void sync_column(Column* column, char* table_dir, size_t row_count) {
  char col_dir[1024];
  filename_join(col_dir, 2, table_dir, column->name);
  system_mkdir_p(col_dir);

  char filename[1024];
  filename_join(filename, 2, col_dir, "data");

  // Write data
  FILE* file = fopen(filename, "w");
  fwrite(column->data, sizeof(int), row_count, file);
  fclose(file);

  if (column->btree != NULL) {
    char index_fname[1024];
    filename_join(index_fname, 2, col_dir, "index");
    btree_sync(column->btree, index_fname);
  }
}

void free_column(Column* column) {
  if (column->btree != NULL)
    btree_free(column->btree);
  free(column->data);
  free(column);
}
