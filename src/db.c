#include "db.h"

Db* init_db(char* db_name, size_t table_capacity) {
  Db* db = malloc(sizeof(Db));
  strcpy(db->name, db_name);
  db->table_count = (size_t)0;
  db->table_capacity = table_capacity;
  db->tables = malloc(table_capacity * sizeof(Table*));
  return db;
}

Db* load_db(char* store_dir, char* db_name, size_t table_capacity) {
  Db* db = init_db(db_name, table_capacity);

  log_info("loading db...\n");

  char db_dir[1024];
  filename_join(db_dir, 2, store_dir, db_name);

  char filename[1024];
  filename_join(filename, 2, db_dir, "tables.csv");

  CsvReader* csv = csv_reader(filename);
  while (read_csv_row(csv)) {
    char table_name[1024]; read_csv_cell(table_name, "table_name", csv);
    char column_cap[1024]; read_csv_cell(column_cap, "column_capacity", csv);
    char row_count[1024]; read_csv_cell(row_count, "row_count", csv);
    char rep_count[1024]; read_csv_cell(rep_count, "replica_count", csv);
    char row_cap[1024]; read_csv_cell(row_cap, "row_capacity", csv);
    db->tables[csv->n_rows_read-1] = load_table(db_dir,
        table_name,
        (size_t)atoi(column_cap),
        (size_t)atoi(row_count),
        (size_t)atoi(row_cap),
        (size_t)atoi(rep_count));
  }
  db->table_count = csv->n_rows_read;
  free_csv_reader(csv);

  return db;
}

void sync_db(Db* db, char* store_dir) {
  char db_dir[1024];
  filename_join(db_dir, 2, store_dir, db->name);
  system_mkdir_p(db_dir);

  char filename[1024];
  filename_join(filename, 2, db_dir, "tables.csv");

  CsvWriter* csv = csv_writer(filename, 5, "table_name", "column_capacity", "row_count", "row_capacity", "replica_count");
  for (size_t i = 0; i < db->table_count; i++) {
    rebuild_before_syncing_if_necessary(db->tables[i]);

    char col_cap[64]; sprintf(col_cap, "%d", (int)db->tables[i]->column_capacity);
    char row_cap[64]; sprintf(row_cap, "%d", (int)db->tables[i]->row_capacity);
    char row_cnt[64]; sprintf(row_cnt, "%d", (int)db->tables[i]->row_count);
    char rep_cnt[64]; sprintf(rep_cnt, "%d", (int)db->tables[i]->replica_count);
    write_csv_cell(db->tables[i]->name, "table_name", csv);
    write_csv_cell(col_cap, "column_capacity", csv);
    write_csv_cell(row_cap, "row_capacity", csv);
    write_csv_cell(row_cnt, "row_count", csv);
    write_csv_cell(rep_cnt, "replica_count", csv);
    write_csv_row(csv);
  }
  free_csv_writer(csv);

  for (size_t i = 0; i < db->table_count; i++)
    sync_table(db->tables[i], db_dir);
}

void free_db(Db* db) {
  for (size_t i = 0; i < db->table_count; i++)
    free_table(db->tables[i]);
  free(db->tables);
  free(db);
}

Table* create_table(Db* db, char* table_name, size_t column_capacity) {
  if (db->table_count >= db->table_capacity) {
    printf("Oh crud, need to resize tables array\n");
    return NULL;
  }

  Table* table = init_table(table_name, column_capacity, (size_t)DEFAULT_ROW_CAPACITY);
  db->tables[db->table_count] = table;
  db->table_count++;

  return table;
}

Table* find_table(Db* db, char* table_name) {
  for (size_t i = 0; i < db->table_count; i++)
    if (!strcmp(db->tables[i]->name, table_name))
      return db->tables[i];
  return NULL;
}
