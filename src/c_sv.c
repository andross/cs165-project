#include "c_sv.h"

CsvReader* csv_reader(char* filename) {
  CsvReader* csv = malloc(sizeof(CsvReader));
  csv->n_columns = 0;
  csv->n_rows_read = 0;

  // Open csv file
  csv->file = fopen(filename, "r");

  // Parse header row
  fgets(csv->current_row, MAX_LINE_LENGTH, csv->file);

  char *header_row = malloc((strlen(csv->current_row)+1) * sizeof(char));
  strcpy(header_row, csv->current_row);

  char *header = strtok(header_row, ",\n");
  while (header) {
    strcpy(csv->headers[csv->n_columns], header);
    csv->n_columns++;
    header = strtok(NULL, ",\n");
  }

  // Free tmp vars
  free(header_row);
  free(header);

  return csv;
}

void free_csv_reader(CsvReader* csv) {
  fclose(csv->file);
  free(csv);
}

char* read_csv_row(CsvReader* csv) {
  char* row = fgets(csv->current_row, MAX_LINE_LENGTH, csv->file);
  if (row)
    csv->n_rows_read++;
  return row;
}

int csv_cell_read_index(char* cell_name, CsvReader* csv) {
  for (int i = 0; i < csv->n_columns; i++)
    if (!strcmp(cell_name, csv->headers[i]))
      return i;
  return -1;
}

void read_csv_cell(char* result, char* cell_name, CsvReader* csv) {
  int index = csv_cell_read_index(cell_name, csv);
  char* cell;
  char *line = malloc((strlen(csv->current_row)+1) * sizeof(char));
  strcpy(line, csv->current_row);
  cell = strtok(line, ",\n");
  while (index) {
    cell = strtok(NULL, ",\n");
    index--;
  }
  strcpy(result, cell);
  free(line);
}

CsvWriter* csv_writer(char* filename, int n_columns, ...) {
  CsvWriter* csv = malloc(sizeof(CsvWriter));
  csv->n_columns = n_columns;

  // Ensure directory exists
  char mkdir_p[1024];
  sprintf(mkdir_p, "mkdir -p $(dirname %s)", filename);
  system(mkdir_p);

  // Open csv file
  csv->file = fopen(filename, "w");

  // load headers
  va_list args;
  va_start(args, n_columns);
  for (int i = 0; i < n_columns; i++) {
    strcpy(csv->headers[i], va_arg(args, char*));
    strcpy(csv->rowvals[i], csv->headers[i]);
  }
  va_end(args);

  // write headers
  write_csv_row(csv);

  return csv;
}

void free_csv_writer(CsvWriter* csv) {
  fclose(csv->file);
  free(csv);
}

int csv_cell_write_index(char* cell_name, CsvWriter* csv) {
  for (int i = 0; i < csv->n_columns; i++)
    if (!strcmp(cell_name, csv->headers[i]))
      return i;
  return -1;
}

void write_csv_cell(char* value, char* cell_name, CsvWriter* csv) {
  int index = csv_cell_write_index(cell_name, csv);
  strcpy(csv->rowvals[index], value);
}

void write_csv_row(CsvWriter* csv) {
  int i;
  for (i = 0; i < csv->n_columns-1; i++) {
    fputs(csv->rowvals[i], csv->file);
    putc(',', csv->file);
    strcpy(csv->rowvals[i], "");
  }
  fputs(csv->rowvals[i], csv->file);
  putc('\n', csv->file);
  strcpy(csv->rowvals[i], "");
}
