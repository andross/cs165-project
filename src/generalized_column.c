#include "generalized_column.h"

int* generalized_column_values(GeneralizedColumn c) {
  if (c.column_type == COLUMN) {
    return c.column_pointer.column->data;
  } else if (c.column_type == RESULT) {
    if (c.column_pointer.result->value_type != INT) {
      printf("trying to interpret a non-integer result as an integer\n");
    }
    return (int*)c.column_pointer.result->values;
  } else {
    return NULL;
  }
}

size_t generalized_column_length(GeneralizedColumn c) {
  if (c.column_type == COLUMN)
    return c.column_pointer.column->length;
  else
    return c.column_pointer.result->value_count;
}
