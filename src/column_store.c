#include "column_store.h"

ColumnStore* load_column_store(char* data_dir) {
  ColumnStore* store = malloc(sizeof(ColumnStore));
  strcpy(store->data_dir, data_dir);
  store->db_capacity = (size_t)DEFAULT_DB_CAPACITY;
  store->dbs = malloc(DEFAULT_DB_CAPACITY * sizeof(Db*));

  char filename[1024];
  filename_join(filename, 2, data_dir, "dbs.csv");

  // if there's no CSV file, we're done for now
  if (!file_exists(filename))
    return store;

  CsvReader* csv = csv_reader(filename);
  while (read_csv_row(csv)) {
    char db_name[64]; read_csv_cell(db_name, "db_name", csv);
    char tbl_cap[64]; read_csv_cell(tbl_cap, "table_capacity", csv);
    store->dbs[csv->n_rows_read-1] = load_db(data_dir, db_name, (size_t)atoi(tbl_cap));
  }
  store->db_count = csv->n_rows_read;
  free_csv_reader(csv);

  return store;
}

void sync_column_store(ColumnStore* store) {
  char filename[1024];
  filename_join(filename, 2, store->data_dir, "dbs.csv");

  char mkdir_p[1024];
  sprintf(mkdir_p, "mkdir -p $(dirname %s)", filename);
  system(mkdir_p);

  CsvWriter* csv = csv_writer(filename, 2, "db_name", "table_capacity");
  for (size_t i = 0; i < store->db_count; i++) {
    write_csv_cell(store->dbs[i]->name, "db_name", csv);
    char tbl_cap[64]; sprintf(tbl_cap, "%d", (int)store->dbs[i]->table_capacity);
    write_csv_cell(tbl_cap, "table_capacity", csv);
    write_csv_row(csv);
  }
  free_csv_writer(csv);

  for (size_t i = 0; i < store->db_count; i++)
    sync_db(store->dbs[i], store->data_dir);
}

void free_column_store(ColumnStore* store) {
  for (size_t i = 0; i < store->db_count; i++)
    free_db(store->dbs[i]);
  free(store->dbs);
  free(store);
}

Db* create_db(ColumnStore* store, char* db_name) {
  if (store->db_count >= store->db_capacity) {
    printf("Oh crud, need to resize dbs array\n");
    return NULL;
  }

  Db* db = init_db(db_name, (size_t)DEFAULT_TABLE_CAPACITY);
  store->dbs[store->db_count] = db;
  store->db_count++;

  return db;
}

Db* find_db(ColumnStore* store, char* db_name) {
  for (size_t i = 0; i < store->db_count; i++)
    if (!strcmp(store->dbs[i]->name, db_name))
      return store->dbs[i];
  return NULL;
}

// don't check for errors and assume columns are in order
void load_data_csv(ColumnStore* store, char* filename) {
  CsvReader* csv = csv_reader(filename);
  char* header = malloc(strlen(csv->headers[0])+1 * sizeof(char));
  strcpy(header, csv->headers[0]);
  char* db_name = strtok(header, ".");
  char* table_name = strtok(NULL, ".");
  Db* db = find_db(store, db_name);
  Table* table = find_table(db, table_name);

  int row[table->column_count];
  char line[1024];
  size_t i;
  while (read_csv_row(csv)) {
    strcpy(line, csv->current_row);
    row[0] = atoi(strtok(line, ",\n"));
    i = 1;
    while (i < table->column_count)
      row[i++] = atoi(strtok(NULL, ",\n"));
    relational_insert(table, row);
  }
  free(header);
  free_csv_reader(csv);
}
