#include <sys/time.h>
#include <stdlib.h>
#include "cs165_api.h"
#include "parse.h"

#define DATA_SIZE 100000000
#define ITERS 25

double get_time() {
  struct timeval t;
  struct timezone tzp;
  gettimeofday(&t, &tzp);
  return t.tv_sec + t.tv_usec*1e-6;
}

#define BENCHMARK(t, block) double t; do {\
  double t1 = clock();\
  block;\
  t = (clock() - t1) / ((double)CLOCKS_PER_SEC);\
} while(0);

#define BENCHMARK_WALL_CLOCK(t, block) double t; do {\
  double t1 = get_time();\
  block;\
  t = (get_time() - t1);\
} while(0);

ColumnStore* cs;
Db* db;
Table* tbl;
Column* col1;
Column* col2;

Result* empty_result(size_t n) {
  Result* res = malloc(sizeof(Result));
  res->value_count = n;
  res->value_type = INT;
  res->values = malloc(n * sizeof(int));
  return res;
}

void setup() {
  cs = load_column_store("bench");
  if (cs->db_count) {
    db = find_db(cs, "db");
    tbl = find_table(db, "tbl");
    col1 = find_column(tbl, "col1");
    col2 = find_column(tbl, "col2");
  } else {
    db = create_db(cs, "db");
    tbl = create_table(db, "tbl", (size_t)2);
    col1 = create_column(tbl, "col1");
    col2 = create_column(tbl, "col2");
    tbl->row_count = DATA_SIZE;
    tbl->row_capacity = DATA_SIZE;
    col1->length = DATA_SIZE;
    col2->length = DATA_SIZE;
    col1->data = malloc(DATA_SIZE * sizeof(int));
    col2->data = malloc(DATA_SIZE * sizeof(int));
    for (int i = 0; i < DATA_SIZE; i++)
      col1->data[i] = i;
    memcpy(col2->data, col1->data, DATA_SIZE * sizeof(int));
    shuffle(col2->data, DATA_SIZE);
    create_secondary_index(tbl, "col2");
    sync_column_store(cs);
  }
}

void benchmark_vector_at_a_time() {
  printf("vec_size,vec_time\n");
  Comparator c;
  c.min_comparator_type = NO_COMPARISON;
  c.max_comparator_type = LESS_THAN;
  c.max_value = DATA_SIZE/2;
  Result* r = malloc(sizeof(Result));
  r->values = malloc(DATA_SIZE * sizeof(int));
  r->value_count = 0;
  size_t chunk, todo, done;

  for (size_t vector_size = 256; vector_size < 65536; vector_size += 256) {
    BENCHMARK(t_vec, {
      todo = DATA_SIZE;
      done = 0;
      r->value_count = 0;
      while (todo > 0) {
        chunk = MIN(vector_size, todo);
        r->value_count += vanilla_select(
            col2->data + done, (int*)r->values + r->value_count, c, chunk, done);
        done += chunk;
        todo -= chunk;
      }
    });

    printf("%d,%.4f\n", (int)vector_size, t_vec);
  }
}

void benchmark_btree_scans() {
  printf("selectivity,tree_time,leaf_fanout,branch_fanout\n");
  int* res = malloc(DATA_SIZE * sizeof(int));

  BTree* btree = btree_bulk_load_sorted(col1->data, NULL, col1->length);

  for (int i = 1; i <= 100; i++) {
    double s = i / (double)100;
    int max = (int)(s * DATA_SIZE);
    BENCHMARK(t_tree, { btree_scan_less_than(btree, res, max); });
    printf("%.2f,%.4f,%d,%d\n", s, t_tree, LEAF_FANOUT, BRANCH_FANOUT);
  }
}

void benchmark_scans() {
  /*printf("selectivity,scan_time,scan_time_using_if,tree_time,sort_time,vec_time\n");*/
  printf("selectivity,scan_time,tree_time,sort_time,vec_time\n");
  Comparator c;
  c.min_comparator_type = NO_COMPARISON;
  c.max_comparator_type = LESS_THAN;
  Result* r = malloc(sizeof(Result));
  r->values = malloc(DATA_SIZE * sizeof(int));
  r->value_count = 0;
  size_t vector_size = 1024;
  size_t chunk, todo, done;

  for (int i = 1; i <= 100; i++) {
    double s = i / (double)100;
    c.max_value = (long int)(s * DATA_SIZE);

    BENCHMARK(t_scan, { vanilla_select(col2->data, r->values, c, DATA_SIZE, 0); });
    /*BENCHMARK(t_scan_noif, { vanilla_select(col2->data, r->values, c, DATA_SIZE, 0); });*/
    /*BENCHMARK(t_scan_if, { ifselect_less_than(c.max_value, col2->data, r->values, DATA_SIZE, 0); });*/
    BENCHMARK(t_tree, { execute_btree_scan(col2, r, c); });
    BENCHMARK(t_sort, { execute_sorted_scan(col1, r, c); });
    BENCHMARK(t_vec, {
      todo = DATA_SIZE;
      done = 0;
      r->value_count = 0;
      while (todo > 0) {
        chunk = MIN(vector_size, todo);
        r->value_count += vanilla_select(
            col2->data + done, (int*)r->values + r->value_count, c, chunk, done);
        done += chunk;
        todo -= chunk;
      }
    });

    /*printf("%.2f,%.4f,%.4f,%.4f,%.4f,%.4f\n", s, t_scan_noif, t_scan_if, t_tree, t_sort, t_vec);*/
    printf("%.2f,%.4f,%.4f,%.4f,%.4f\n", s, t_scan, t_tree, t_sort, t_vec);
  }
}

void benchmark_threaded_scans() {
  struct message msg;
  size_t vec_size = 1024;

  printf("n_scanners,t_threaded,t_sequential\n");

  for (int n = 1; n <= 8; n++) {
    ClientContext* context  = init_client_context();
    SharedScan* ss = init_shared_scan(col2, vec_size);
    for (int i = 0; i < n; i++) {
      char command[1024];
      sprintf(command, "s%d=select(db.tbl.col2,%d,%d)", i, i, DATA_SIZE-i);
      add_shared_scanner(ss, parse_command(command, cs, &msg, 0, context));
    }
    BENCHMARK_WALL_CLOCK(t_threaded, { do_threaded_scan(ss); });
    free_shared_scan(ss);
    free_client_context(context);

    context = init_client_context();
    DbOperator* operators[n];
    for (int i = 0; i < n; i++) {
      char command[1024];
      sprintf(command, "s%d=select(db.tbl.col2,%d,%d)", i, i, DATA_SIZE-i);
      operators[i] = parse_command(command, cs, &msg, 0, context);
    }
    BENCHMARK_WALL_CLOCK(t_sequential, { for (int i = 0; i < n; i++) execute_db_operator(operators[i]); });
    free_client_context(context);

    printf("%d,%.4f,%.4f\n", n, t_threaded, t_sequential);
  }
}

void benchmark_shared_scans() {
  struct message msg;
  size_t vec_size = 4096;

  printf("n_scanners,t_shared,t_sequential\n");

  for (int n = 1; n <= 12; n++) {
    ClientContext* context  = init_client_context();
    SharedScan* ss = init_shared_scan(col2, vec_size);
    for (int i = 0; i < n; i++) {
      char command[1024];
      sprintf(command, "s%d=select(db.tbl.col2,%d,%d)", i, i, DATA_SIZE-i);
      add_shared_scanner(ss, parse_command(command, cs, &msg, 0, context));
    }
    BENCHMARK(t_shared, { do_shared_scan(ss); });
    free_shared_scan(ss);
    free_client_context(context);

    context = init_client_context();
    DbOperator* operators[n];
    for (int i = 0; i < n; i++) {
      char command[1024];
      sprintf(command, "s%d=select(db.tbl.col2,%d,%d)", i, i, DATA_SIZE-i);
      operators[i] = parse_command(command, cs, &msg, 0, context);
    }
    BENCHMARK(t_sequential, { for (int i = 0; i < n; i++) execute_db_operator(operators[i]); });
    free_client_context(context);

    printf("%d,%.4f,%.4f\n", n, t_shared, t_sequential);
  }
}

void benchmark_joins() {
  struct message msg;
  printf("large_colsize,small_colsize,nested_loop,hash");

  /*size_t n_sizes = 6;*/
  /*size_t block_sizes[6] = { 50, 100, 500, 1000, 5000, 10000 };*/
  size_t n_sizes = 1;
  size_t block_sizes[1] = { 4096 };
  for (size_t i = 0; i < n_sizes; i++)
    printf(",nested_block_%d", (int)block_sizes[i]);
  printf("\n");

  for (double s = 0.000025; s <= 0.00025; s += 0.2 * s) {
    int max = (int)(s * DATA_SIZE);
    /*printf("%d\n", max);*/
    char p2cmd[1024];
    sprintf(p2cmd, "p2=select(db.tbl.col2,null,%d)", max);
    char p1cmd[1024];
    sprintf(p1cmd, "p1=select(db.tbl.col1,null,%d)", (int)(0.01*DATA_SIZE));
    ClientContext* context  = init_client_context();
    execute_db_operator(parse_command(p1cmd, cs, &msg, 0, context));
    execute_db_operator(parse_command(p2cmd, cs, &msg, 0, context));
    execute_db_operator(parse_command("v1=fetch(db.tbl.col1,p1)", cs, &msg, 0, context));
    execute_db_operator(parse_command("v2=fetch(db.tbl.col2,p2)", cs, &msg, 0, context));
    Result* p1 = get_client_variable(context, "p1");
    Result* p2 = get_client_variable(context, "p2");
    Result* v1 = get_client_variable(context, "v1");
    Result* v2 = get_client_variable(context, "v2");
    printf("%d,%d", (int)p1->value_count, (int)p2->value_count);
    Result* r1 = empty_result(max);
    Result* r2 = empty_result(max);

    BENCHMARK(loop_time, { nested_loop_join(v1,p1,v2,p2,r1,r2); });
    printf(",%.4f", loop_time);
    BENCHMARK(hash_time, { hash_join(v1,p1,v2,p2,r1,r2); });
    printf(",%.4f", hash_time);
    for (size_t i = 0; i < n_sizes; i++) {
      BENCHMARK(block_time, { block_nested_loop_join(v1,p1,v2,p2,r1,r2,block_sizes[i]); });
      printf(",%.4f", block_time);
    }
    printf("\n");
  }
}

void benchmark_hash_joins() {
  struct message msg;
  printf("L_size,R_size,hash_join_time,vals_per_node\n");
  for (double s = 0.000025; s <= 0.00025; s += 0.2 * s) {
    int max = (int)(s * DATA_SIZE);
    char p2cmd[1024];
    sprintf(p2cmd, "p2=select(db.tbl.col2,null,%d)", max);
    char p1cmd[1024];
    sprintf(p1cmd, "p1=select(db.tbl.col1,null,%d)", (int)(0.01*DATA_SIZE));
    ClientContext* context  = init_client_context();
    execute_db_operator(parse_command(p1cmd, cs, &msg, 0, context));
    execute_db_operator(parse_command(p2cmd, cs, &msg, 0, context));
    execute_db_operator(parse_command("v1=fetch(db.tbl.col1,p1)", cs, &msg, 0, context));
    execute_db_operator(parse_command("v2=fetch(db.tbl.col2,p2)", cs, &msg, 0, context));
    Result* p1 = get_client_variable(context, "p1");
    Result* p2 = get_client_variable(context, "p2");
    Result* v1 = get_client_variable(context, "v1");
    Result* v2 = get_client_variable(context, "v2");
    printf("%d,%d", (int)p1->value_count, (int)p2->value_count);
    Result* r1 = empty_result(max);
    Result* r2 = empty_result(max);
    BENCHMARK(hash_time, { hash_join(v1,p1,v2,p2,r1,r2); });
    printf(",%.4f,%d\n", hash_time, VALS_PER_NODE);
  }
}

void benchmark_btree_loads() {
  // already sorted
  int* values = col1->data;
  size_t length = col1->length;
  int* indexes = malloc(length * sizeof(int));
  for (size_t i = 0; i < length; i++) indexes[i] = i;

  BENCHMARK(one_at_a_time, {
    BTree* b1 = btree_init();
    for (size_t i = 0; i < length; i++)
      btree_insert(values[i], i, b1);
  });

  BENCHMARK(bulk_load_time_sorted, {
    btree_bulk_load_sorted(values, indexes, length);
  });

  BENCHMARK(bulk_load_time_total, {
    btree_bulk_load(col2->data, length);
  });

  printf("one_at_a_time,bulk_load_time_sorted,bulk_load_time_total\n%.4f,%.4f,%.4f\n", one_at_a_time, bulk_load_time_sorted, bulk_load_time_total);
}

// later try benchmarking hash join vs. grace join as we increase the data size. They should start out more or less equal,
// then grace join should start beating hash join.

int main() {
  setup();
  benchmark_shared_scans();
  /*benchmark_scans();*/
  /*benchmark_hash_joins();*/
  /*benchmark_vector_at_a_time();*/
  /*benchmark_joins();*/
  /*benchmark_btree_loads();*/
  /*benchmark_btree_scans();*/
  return 0;
}
