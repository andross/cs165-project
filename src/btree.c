#include "btree.h"

BTree* btree_init() {
  BTree* tree = malloc(sizeof(BTree));
  tree->root_is_leaf = true;
  tree->root = bleaf_init();
  tree->size = 0;
  tree->depth = 1;
  tree->min_val = 0;
  tree->max_val = 0;
  return tree;
}

BLeaf* bleaf_init() {
  BLeaf* leaf = malloc(sizeof(BLeaf));
  leaf->length = 0;
  return leaf;
}

BBranch* bbranch_init(bool kids_are_leaves) {
  BBranch* branch = malloc(sizeof(BBranch));
  branch->length = 0;
  branch->kids_are_leaves = kids_are_leaves;
  return branch;
}

float btree_selectivity(BTree* tree, Comparator comp) {
  ComparatorType minc = comp.min_comparator_type;
  ComparatorType maxc = comp.max_comparator_type;
  int tree_min = tree->min_val;
  int tree_max = tree->max_val;
  int comp_min = (minc == NO_COMPARISON ? tree_min : comp.min_value);
  int comp_max = (maxc == NO_COMPARISON ? tree_max : comp.max_value);
  float coverage = (MIN(comp_max, tree_max) - MAX(comp_min, tree_min)) / (float)(tree_max - tree_min);
  return MAX(coverage, 0);
}

size_t btree_scan_between(BTree* tree, int* results, int min, int max) {
  if (tree->size == 0) return (size_t)0;

  int all_gte_min = (tree->min_val >= min);
  int all_lt_max = (tree->max_val < max);

  if (all_gte_min && all_lt_max) {
    if (tree->root_is_leaf)
      return bleaf_scan_all((BLeaf*)tree->root, results);
    else
      return bbranch_scan_all((BBranch*)tree->root, results);
  } else if (all_gte_min) {
    if (tree->root_is_leaf)
      return bleaf_scan_less_than((BLeaf*)tree->root, results, max);
    else
      return bbranch_scan_less_than((BBranch*)tree->root, results, max);
  } else if (all_lt_max) {
    if (tree->root_is_leaf)
      return bleaf_scan_at_least((BLeaf*)tree->root, results, min);
    else
      return bbranch_scan_at_least((BBranch*)tree->root, results, min);
  } else {
    if (tree->root_is_leaf)
      return bleaf_scan_between((BLeaf*)tree->root, results, min, max);
    else
      return bbranch_scan_between((BBranch*)tree->root, results, min, max);
  }
}

size_t btree_scan_at_least(BTree* tree, int* results, int min) {
  if (tree->size == 0) return (size_t)0;

  int all_gte_min = (tree->min_val >= min);

  if (all_gte_min) {
    if (tree->root_is_leaf)
      return bleaf_scan_all((BLeaf*)tree->root, results);
    else
      return bbranch_scan_all((BBranch*)tree->root, results);
  } else {
    if (tree->root_is_leaf)
      return bleaf_scan_at_least((BLeaf*)tree->root, results, min);
    else
      return bbranch_scan_at_least((BBranch*)tree->root, results, min);
  }
}

size_t btree_scan_less_than(BTree* tree, int* results, int max) {
  if (tree->size == 0) return (size_t)0;

  int all_lt_max = (tree->max_val < max);

  if (all_lt_max) {
    if (tree->root_is_leaf)
      return bleaf_scan_all((BLeaf*)tree->root, results);
    else
      return bbranch_scan_all((BBranch*)tree->root, results);
  } else {
    if (tree->root_is_leaf)
      return bleaf_scan_less_than((BLeaf*)tree->root, results, max);
    else
      return bbranch_scan_less_than((BBranch*)tree->root, results, max);
  }
}

size_t bbranch_scan_between(BBranch* branch, int* results, int min, int max) {
  int min_i = first_index_at_least(min, branch->keys, branch->length-1);
  int max_i = first_index_at_least(max, branch->keys, branch->length-1) + 1;
  size_t size = 0;
  if (max_i - min_i == 1) {
    if (branch->kids_are_leaves)
      size += bleaf_scan_between((BLeaf*)branch->kids[min_i], results+size, min, max);
    else
      size += bbranch_scan_between((BBranch*)branch->kids[min_i], results+size, min, max);
  } else {
    // min
    if (branch->kids_are_leaves)
      size += bleaf_scan_at_least((BLeaf*)branch->kids[min_i], results+size, min);
    else
      size += bbranch_scan_at_least((BBranch*)branch->kids[min_i], results+size, min);

    // mid
    if (branch->kids_are_leaves) {
      for (int i = min_i+1; i < max_i-1; i++)
        size += bleaf_scan_all((BLeaf*)branch->kids[i], results+size);
    } else {
      for (int i = min_i+1; i < max_i-1; i++)
        size += bbranch_scan_all((BBranch*)branch->kids[i], results+size);
    }

    // max
    if (branch->kids_are_leaves)
      size += bleaf_scan_less_than((BLeaf*)branch->kids[max_i-1], results+size, max);
    else
      size += bbranch_scan_less_than((BBranch*)branch->kids[max_i-1], results+size, max);
  }
  return size;
}

size_t bbranch_scan_at_least(BBranch* branch, int* results, int min) {
  int min_i = first_index_at_least(min+1, branch->keys, branch->length-1);
  size_t size = 0;
  if (branch->kids_are_leaves) {
    size += bleaf_scan_at_least((BLeaf*)branch->kids[min_i], results+size, min);
    for (size_t i = min_i+1; i < branch->length; i++)
      size += bleaf_scan_all((BLeaf*)branch->kids[i], results+size);
  } else {
    size += bbranch_scan_at_least((BBranch*)branch->kids[min_i], results+size, min);
    for (size_t i = min_i+1; i < branch->length; i++)
      size += bbranch_scan_all((BBranch*)branch->kids[i], results+size);
  }
  return size;
}

size_t bbranch_scan_less_than(BBranch* branch, int* results, int max) {
  int max_i = last_index_less_than(max, branch->keys, branch->length-1) + 1;
  size_t size = 0;
  if (branch->kids_are_leaves) {
    for (int i = 0; i < max_i; i++)
      size += bleaf_scan_all((BLeaf*)branch->kids[i], results+size);
    size += bleaf_scan_less_than((BLeaf*)branch->kids[max_i], results+size, max);
  } else {
    for (int i = 0; i < max_i; i++)
      size += bbranch_scan_all((BBranch*)branch->kids[i], results+size);
    size += bbranch_scan_less_than((BBranch*)branch->kids[max_i], results+size, max);
  }
  return size;
}

size_t bleaf_scan_between(BLeaf* leaf, int* results, int min, int max) {
  int min_i = first_index_at_least(min, leaf->vals, leaf->length);
  int max_i = last_index_less_than(max, leaf->vals, leaf->length);
  size_t size = max_i - min_i + 1;
  memcpy(results, leaf->idxs+min_i, size*sizeof(int));
  return size;
}

size_t bleaf_scan_at_least(BLeaf* leaf, int* results, int min) {
  int min_i = first_index_at_least(min, leaf->vals, leaf->length);
  int max_i = leaf->length - 1;
  size_t size = max_i - min_i + 1;
  memcpy(results, leaf->idxs+min_i, size*sizeof(int));
  return size;
}

size_t bleaf_scan_less_than(BLeaf* leaf, int* results, int max) {
  int min_i = 0;
  int max_i = last_index_less_than(max, leaf->vals, leaf->length);
  size_t size = max_i - min_i + 1;
  memcpy(results, leaf->idxs, size*sizeof(int));
  return size;
}

size_t bbranch_scan_all(BBranch* branch, int* results) {
  size_t size = 0;
  if (branch->kids_are_leaves) {
    for (size_t i = 0; i < branch->length; i++)
      size += bleaf_scan_all((BLeaf*)branch->kids[i], results+size);
  } else {
    for (size_t i = 0; i < branch->length; i++)
      size += bbranch_scan_all((BBranch*)branch->kids[i], results+size);
  }
  return size;
}

size_t bleaf_scan_all(BLeaf* leaf, int* results) {
  size_t size = leaf->length;
  memcpy(results, leaf->idxs, size*sizeof(int));
  return size; 
}

void btree_sync(BTree* tree, char* filename) {
  char mkdir_p[1024];
  sprintf(mkdir_p, "mkdir -p $(dirname %s)", filename);
  system(mkdir_p);

  FILE* file = fopen(filename, "w");
  fwrite(&tree->root_is_leaf, sizeof(bool), 1, file);
  fwrite(&tree->size, sizeof(size_t), 1, file);
  fwrite(&tree->depth, sizeof(size_t), 1, file);
  fwrite(&tree->min_val, sizeof(int), 1, file);
  fwrite(&tree->max_val, sizeof(int), 1, file);
  if (tree->root_is_leaf)
    bleaf_sync((BLeaf*)tree->root, file);
  else
    bbranch_sync((BBranch*)tree->root, file);
  fclose(file);
}

BTree* btree_load(char* filename) {
  FILE* file = fopen(filename, "r");
  BTree* tree = malloc(sizeof(BTree));
  fread(&tree->root_is_leaf, sizeof(bool), 1, file);
  fread(&tree->size, sizeof(size_t), 1, file);
  fread(&tree->depth, sizeof(size_t), 1, file);
  fread(&tree->min_val, sizeof(int), 1, file);
  fread(&tree->max_val, sizeof(int), 1, file);
  if (tree->root_is_leaf)
    tree->root = bleaf_load(file);
  else
    tree->root = bbranch_load(file);
  fclose(file);
  return tree;
}

void bleaf_sync(BLeaf* leaf, FILE* file) {
  fwrite(&leaf->length, sizeof(size_t), 1, file);
  fwrite(leaf->vals, sizeof(int), leaf->length, file);
  fwrite(leaf->idxs, sizeof(int), leaf->length, file);
}

BLeaf* bleaf_load(FILE* file) {
  BLeaf* leaf = malloc(sizeof(BLeaf));
  fread(&leaf->length, sizeof(size_t), 1, file);
  fread(leaf->vals, sizeof(int), leaf->length, file);
  fread(leaf->idxs, sizeof(int), leaf->length, file);
  return leaf;
}

void bbranch_sync(BBranch* branch, FILE* file) {
  fwrite(&branch->length, sizeof(size_t), 1, file);
  fwrite(&branch->min_val, sizeof(int), 1, file);
  fwrite(&branch->kids_are_leaves, sizeof(bool), 1, file);
  fwrite(branch->keys, sizeof(int), branch->length-1, file);

  if (branch->kids_are_leaves) {
    for (size_t i = 0; i < branch->length; i++)
      bleaf_sync((BLeaf*)branch->kids[i], file);
  } else {
    for (size_t i = 0; i < branch->length; i++)
      bbranch_sync((BBranch*)branch->kids[i], file);
  }
}

BBranch* bbranch_load(FILE* file) {
  BBranch* branch = malloc(sizeof(BBranch));
  fread(&branch->length, sizeof(size_t), 1, file);
  fread(&branch->min_val, sizeof(int), 1, file);
  fread(&branch->kids_are_leaves, sizeof(bool), 1, file);
  fread(branch->keys, sizeof(int), branch->length-1, file);

  if (branch->kids_are_leaves) {
    for (size_t i = 0; i < branch->length; i++)
      branch->kids[i] = bleaf_load(file);
  } else {
    for (size_t i = 0; i < branch->length; i++)
      branch->kids[i] = bbranch_load(file);
  }

  return branch;
}

void bleaf_free(BLeaf* leaf) {
  free(leaf);
}

void bbranch_free(BBranch* branch) {
  if (branch->kids_are_leaves) {
    for (size_t i = 0; i < branch->length; i++)
      bleaf_free((BLeaf*)branch->kids[i]);
  } else {
    for (size_t i = 0; i < branch->length; i++)
      bbranch_free((BBranch*)branch->kids[i]);
  }
  free(branch);
}

void btree_free(BTree* tree) {
  if (tree->root_is_leaf)
    bleaf_free((BLeaf*)tree->root);
  else
    bbranch_free((BBranch*)tree->root);
  free(tree);
}

void btree_insert(int val, int pos, BTree* tree) {
  if (tree->root_is_leaf) {
    BLeaf* new_leaf = bleaf_insert(val, pos, (BLeaf*)tree->root);
    if (new_leaf != NULL) {
      BBranch* new_root = bbranch_init(true);
      new_root->keys[0] = new_leaf->vals[0];
      new_root->kids[0] = tree->root;
      new_root->kids[1] = new_leaf;
      new_root->length = 2;
      new_root->min_val = ((BLeaf*)tree->root)->vals[0];
      tree->root = new_root;
      tree->root_is_leaf = false;
      tree->depth++;
    }
  } else {
    BBranch* new_branch = bbranch_insert(val, pos, (BBranch*)tree->root);
    if (new_branch != NULL) {
      BBranch* new_root = bbranch_init(false);
      new_root->keys[0] = bbranch_min(new_branch);
      new_root->kids[0] = tree->root;
      new_root->kids[1] = new_branch;
      new_root->length = 2;
      new_root->min_val = ((BBranch*)tree->root)->min_val;
      tree->root = new_root;
      tree->depth++;
    }
  }
  tree->size++;
  if (tree->size == 1) {
    tree->min_val = val;
    tree->max_val = val;
  } else if (val < tree->min_val) {
    tree->min_val = val;
  } else if (val > tree->max_val) {
    tree->max_val = val;
  }
}

// argh TODO this shouldn't be necessary. find a better way.
// maybe store min as instance variable.
int bbranch_min(BBranch* branch) {
  if (branch->kids_are_leaves)
    return ((BLeaf*)branch->kids[0])->vals[0];
  else
    return bbranch_min((BBranch*)branch->kids[0]);
}

BBranch* bbranch_insert(int val, int pos, BBranch* branch) {
  // figure out where to insert the new value
  int index = index_to_insert(val, branch->keys, branch->length-1);
  void* old_kid = branch->kids[index];

  // insert the value into the appropriate child node
  void* new_kid = NULL;
  if (branch->kids_are_leaves)
    new_kid = ((void*)bleaf_insert(val, pos, (BLeaf*)old_kid));
  else
    new_kid = ((void*)bbranch_insert(val, pos, (BBranch*)old_kid));

  if (val < branch->min_val)
    branch->min_val = val;

  // if the child node didn't split, we're done
  if (new_kid == NULL)
    return NULL;

  int new_key;
  if (branch->kids_are_leaves)
    new_key = ((BLeaf*)new_kid)->vals[0];
  else
    new_key = ((BBranch*)new_kid)->min_val;

  for (int i = branch->length-1; i > index; i--) {
    branch->kids[i+1] = branch->kids[i];
    branch->keys[i] = branch->keys[i-1];
  }
  branch->kids[index+1] = new_kid;
  branch->keys[index] = new_key;
  branch->length++;

  // if we had enough space, we're done
  if (branch->length < BRANCH_FANOUT)
    return NULL;

  // if not, then we need to split.
  BBranch* new_branch = bbranch_init(branch->kids_are_leaves);
  int full = (int)branch->length;
  int half = (int)branch->length/2;
  for (int i = half; i < full; i++) {
    new_branch->kids[i-half] = branch->kids[i];
    new_branch->length++;
    branch->kids[i] = NULL;
    branch->length--;
  }
  for (int i = half; i < full-1; i++) {
    new_branch->keys[i-half] = branch->keys[i];
    branch->keys[i] = 0;
  }
  if (new_branch->kids_are_leaves)
    new_branch->min_val = ((BLeaf*)new_branch->kids[0])->vals[0];
  else
    new_branch->min_val = ((BBranch*)new_branch->kids[0])->min_val;

  return new_branch;
}

BLeaf* bleaf_insert(int val, int idx, BLeaf* leaf) {
  int index = index_to_insert(val, leaf->vals, leaf->length);

  for (int i = leaf->length-1; i >= index; i--) {
    leaf->vals[i+1] = leaf->vals[i];
    leaf->idxs[i+1] = leaf->idxs[i];
  }
  leaf->vals[index] = val;
  leaf->idxs[index] = idx;
  leaf->length++;

  if (leaf->length < LEAF_FANOUT)
    return NULL;

  BLeaf* new_leaf = bleaf_init();
  int halfway_there = leaf->length/2; // we'll make it i sweaaar
  int fullway_there = leaf->length;
  for (int i = halfway_there; i < fullway_there; i++) {
    new_leaf->vals[i-halfway_there] = leaf->vals[i];
    new_leaf->idxs[i-halfway_there] = leaf->idxs[i];
    new_leaf->length++;
    leaf->vals[i] = 0;
    leaf->idxs[i] = 0;
    leaf->length--;
  }
  return new_leaf;
}

void btree_delete(int val, int pos, BTree* tree) {
  if (tree->root_is_leaf)
    bleaf_delete(val, pos, (BLeaf*)tree->root);
  else
    bbranch_delete(val, pos, (BBranch*)tree->root);
  tree->size--;
}

void bbranch_delete(int val, int pos, BBranch* branch) {
  // figure out where to insert the new value
  int index = index_to_insert(val, branch->keys, branch->length-1);
  void* kid = branch->kids[index];
  if (branch->kids_are_leaves)
    bleaf_delete(val, pos, (BLeaf*)kid);
  else
    bbranch_delete(val, pos, (BBranch*)kid);
}

void bleaf_delete(int val, int idx, BLeaf* leaf) {
  int index = first_index_at_least(val, leaf->vals, leaf->length);
  if (leaf->vals[index] == val && leaf->idxs[index] == idx) {
    for (size_t i = index+1; i < leaf->length; i++) {
      leaf->vals[i-1] = leaf->vals[i];
      leaf->idxs[i-1] = leaf->idxs[i];
    }
    leaf->length--;
  } else {
    log_err("calling bleaf_delete but the value we're trying to delete isn't here\n");
  }
}

void bleaf_print(BLeaf* leaf, size_t level) {
  for (size_t j = 0; j < level; j++) printf("  ");
  size_t i;
  for (i = 0; i < leaf->length; i++)
    printf("%d,", leaf->vals[i]);
  while (i < LEAF_FANOUT) {
    printf("_,");
    i++;
  }
  printf("\n");
}

void bbranch_print(BBranch* branch, size_t level) {
  for (size_t j = 0; j < level; j++) printf("  ");
  printf("br(%d):\n", branch->min_val);
  if (branch->kids_are_leaves) {
    bleaf_print((BLeaf*)branch->kids[0], level+1);
    for (size_t i = 0; i < branch->length-1; i++) {
      for (size_t j = 0; j < level; j++) printf("  ");
      printf("%d\n", branch->keys[i]);
      bleaf_print((BLeaf*)branch->kids[i+1], level+1);
    }
  } else {
    bbranch_print((BBranch*)branch->kids[0], level+1);
    for (size_t i = 0; i < branch->length-1; i++) {
      for (size_t j = 0; j < level; j++) printf(" ");
      printf("%d\n", branch->keys[i]);
      bbranch_print((BBranch*)branch->kids[i+1], level+1);
    }
  }
}

void btree_print(BTree* tree) {
  printf("root(%d,%d):\n", tree->min_val, tree->max_val);
  if (tree->root_is_leaf)
    bleaf_print((BLeaf*)tree->root, 1);
  else
    bbranch_print((BBranch*)tree->root, 1);
  printf("\n");
}

void bulk_grow_branches(BTree* tree, void** kids, int* mins, size_t n_kids, bool kids_are_leaves) {
  size_t n_branches = (2*n_kids + BRANCH_FANOUT - 1)/BRANCH_FANOUT;
  size_t n_per_branch = n_kids / n_branches;
  size_t n_with_one_extra = n_kids - n_per_branch * n_branches; 
  size_t n_copied = 0;

  BBranch** branches = malloc(n_branches * sizeof(void*));
  int* branch_mins = malloc(n_branches * sizeof(int));

  for (size_t i = 0; i < n_branches; i++) {
    size_t n_to_copy = n_per_branch + (i < n_with_one_extra);
    BBranch* branch = bbranch_init(kids_are_leaves);
    branch->length = n_to_copy;
    branch->min_val = mins[n_copied];
    if (kids_are_leaves) {
      memcpy(branch->kids, ((BLeaf**)kids)+n_copied, n_to_copy*sizeof(BLeaf*));
    } else {
      memcpy(branch->kids, ((BBranch**)kids)+n_copied, n_to_copy*sizeof(BBranch*));
    }
    memcpy(branch->keys, mins+n_copied+1, (n_to_copy-1)*sizeof(int));
    branches[i] = branch;
    branch_mins[i] = mins[n_copied];
    n_copied += n_to_copy;
  }

  tree->depth++;

  if (n_branches == 1)
    tree->root = branches[0];
  else
    bulk_grow_branches(tree, (void*)branches, branch_mins, n_branches, false);

  free(branches);
  free(branch_mins);
}

BTree* btree_bulk_load(int* values, size_t length) {
  if (is_already_sorted(values, length)) {
    return btree_bulk_load_sorted(values, NULL, length);
  } else {
    BTree* btree = btree_init();
    for (size_t i = 0; i < length; i++)
      btree_insert(values[i], i, btree);
    return btree;
  }

  /*int* indexes = malloc(length * sizeof(int));*/
  /*int* valcopy = malloc(length * sizeof(int));*/
  /*for (size_t i = 0; i < length; i++)*/
    /*indexes[i] = i;*/
  /*memcpy(valcopy, values, length * sizeof(int));*/
  /*int* arrays[2];*/
  /*arrays[0] = valcopy;*/
  /*arrays[1] = indexes;*/
  /*group_external_sort(arrays, length, 2, 0, 2048);*/
  /*BTree* btree = btree_bulk_load_sorted(valcopy, indexes, length);*/
  /*free(indexes);*/
  /*free(valcopy);*/
  /*return btree;*/
}

BTree* btree_bulk_load_sorted(int* sorted_values, int* indexes, size_t length) {
  BTree* tree = malloc(sizeof(BTree));
  tree->size = length;
  tree->min_val = sorted_values[0];
  tree->max_val = sorted_values[length-1];
  tree->depth = 1;

  size_t n_leaves = (2*length + LEAF_FANOUT - 1)/LEAF_FANOUT;
  size_t n_per_leaf = length / n_leaves;
  size_t n_with_one_extra = length - n_per_leaf * n_leaves;
  size_t n_copied = 0;
  BLeaf** leaves = malloc(n_leaves * sizeof(BLeaf*));
  int* leaf_mins = malloc(n_leaves * sizeof(int));

  for (size_t i = 0; i < n_leaves; i++) {
    size_t n_to_copy = n_per_leaf + (i < n_with_one_extra);
    BLeaf* leaf = bleaf_init();
    memcpy(leaf->vals, sorted_values + n_copied, n_to_copy * sizeof(int));
    if (indexes == NULL) {
      for (size_t j = 0; j < n_to_copy; j++)
        leaf->idxs[j] = j + n_copied;
    } else {
      memcpy(leaf->idxs, indexes + n_copied, n_to_copy * sizeof(int));
    }
    leaf->length = n_to_copy;
    leaves[i] = leaf;
    leaf_mins[i] = leaf->vals[0];
    n_copied += n_to_copy;
  }

  if (n_leaves == 1) {
    tree->root_is_leaf = true;
    tree->root = leaves[0];
  } else {
    tree->root_is_leaf = false;
    bulk_grow_branches(tree, (void**)leaves, leaf_mins, n_leaves, true);
  }

  free(leaves);
  free(leaf_mins);

  return tree;
}
