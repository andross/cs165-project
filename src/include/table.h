#ifndef TABLE_H
#define TABLE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "c_sv.h"
#include "column.h"
#include "bitvector.h"

#define MAX_REPLICAS 4
#define MAX_SIZE_NAME 64
#define DEFAULT_COLUMN_CAPACITY 100
#define DEFAULT_ROW_CAPACITY 5000

typedef struct Table {
    char name[MAX_SIZE_NAME];
    Column** columns;
    size_t column_count;
    size_t column_capacity;
    size_t row_count;
    size_t row_capacity;
    int is_sorted;
    int leader;
    struct Table* replicas[MAX_REPLICAS];
    size_t replica_count;

    int* pending_deletes;
    size_t pending_delete_count;
} Table;

Table* init_table(char* table_name, size_t column_capacity, size_t row_capacity);
Table* load_table(char* db_dir, char* table_name, size_t column_capacity, size_t row_count, size_t row_capacity, size_t replica_count);
Table* replicate_table(Table* table);
Table* best_replica_for(char* column_name, Table* table);
void load_data_payload(Table* table, size_t row_count, int* payload);
void rebuild_table_indexes(Table* table);
void sync_table(Table* table, char* db_dir);
void free_table(Table* table);
void relational_insert(Table* table, int* values);
Column* create_column(Table* table, char* column_name);
Column* find_column(Table* table, char* column_name);
int find_column_index(Table* table, char* column_name);
int create_secondary_index(Table* table, char* column_name);
int create_primary_index(Table* table, char* column_name);

void relational_delete(Table* table, int* positions, size_t n_positions);
void relational_update(Table* table, char* column_name, int* positions, size_t n_positions, int new_value);

void rebuild_before_syncing_if_necessary(Table* table);

#endif /* TABLE_H */
