// utils.h
// CS165 Fall 2015
//
// Provides utility and helper functions that may be useful throughout.
// Includes debugging tools.

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdarg.h>
#include <stdio.h>

#ifndef MIN
#define MIN(a,b) ((a < b) ? a : b)
#endif

#ifndef MAX
#define MAX(a,b) ((a > b) ? a : b)
#endif

int is_already_sorted(int* values, size_t length);
void shuffle(int* array, size_t length);
void quicksort(int* array, size_t length);
void group_quicksort(int* array, int** arrays, int n_arrays, size_t length);
void group_quicksort_chunk(int* array, int** arrays, int n_arrays, long int l, long int r);
int index_to_insert(int value, int* values, int length);
int last_index_less_than(int value, int* values, int length);
int first_index_at_least(int value, int* values, int length);

int file_exists(char* filename);
void system_mkdir_p(char* dirname);

/**
 * joins a series of n string arguments together with "/"s, loading
 * the resulting string into filename.
 **/
void filename_join(char* filename, int n, ...);

/**
 * trims newline characters from a string (in place)
 **/

char* trim_newline(char *str);

/**
 * trims parenthesis characters from a string (in place)
 **/

char* trim_parenthesis(char *str);

/**
 * trims whitespace characters from a string (in place)
 **/

char* trim_whitespace(char *str);

/**
 * trims quotations characters from a string (in place)
 **/

char* trim_quotes(char *str);

// cs165_log(out, format, ...)
// Writes the string from @format to the @out pointer, extendable for
// additional parameters.
//
// Usage: cs165_log(stderr, "%s: error at line: %d", __func__, __LINE__);
void cs165_log(FILE* out, const char *format, ...);

// log_err(format, ...)
// Writes the string from @format to stderr, extendable for
// additional parameters. Like cs165_log, but specifically to stderr.
//
// Usage: log_err("%s: error at line: %d", __func__, __LINE__);
void log_err(const char *format, ...);

// log_info(format, ...)
// Writes the string from @format to stdout, extendable for
// additional parameters. Like cs165_log, but specifically to stdout.
// Only use this when appropriate (e.g., denoting a specific checkpoint),
// else defer to using printf.
//
// Usage: log_info("Command received: %s", command_string);
void log_info(const char *format, ...);

#endif /* __UTILS_H__ */
