#ifndef COMPARATOR_H
#define COMPARATOR_H

typedef enum ComparatorType {
    NO_COMPARISON = 0,
    LESS_THAN = 1,
    GREATER_THAN = 2,
    EQUAL = 4,
    LESS_THAN_OR_EQUAL = 5,
    GREATER_THAN_OR_EQUAL = 6
} ComparatorType;

typedef struct Comparator {
    long int min_value; // used in equality and ranges.
    long int max_value; // used in range compares. 
    ComparatorType min_comparator_type;
    ComparatorType max_comparator_type;
} Comparator;

#endif
