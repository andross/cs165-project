#ifndef BTREE_H
#define BTREE_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"
#include "comparator.h"
#include "external_sort.h"

#ifdef IS_TEST
#define LEAF_FANOUT 512
#define BRANCH_FANOUT 512
#else
#define LEAF_FANOUT 8192
#define BRANCH_FANOUT 8192
#endif

typedef struct BTree {
  void* root;
  bool root_is_leaf;
  size_t size;
  size_t depth;
  int min_val;
  int max_val;
} BTree;

typedef struct BLeaf {
  size_t length;
  int vals[LEAF_FANOUT];
  int idxs[LEAF_FANOUT+1];
} BLeaf;

typedef struct BBranch {
  size_t length;
  int keys[BRANCH_FANOUT];
  void* kids[BRANCH_FANOUT+1];
  bool kids_are_leaves;
  int min_val;
} BBranch;

BTree* btree_init();
BLeaf* bleaf_init();
BBranch* bbranch_init(bool kids_are_leaves);

BTree* btree_bulk_load(int* values, size_t length);
BTree* btree_bulk_load_sorted(int* sorted_values, int* indexes, size_t length);

float btree_selectivity(BTree* tree, Comparator comp);
void btree_sync(BTree* tree, char* filename);
void bleaf_sync(BLeaf* leaf, FILE* file);
void bbranch_sync(BBranch* branch, FILE* file);
BTree* btree_load(char* filename);
BLeaf* bleaf_load(FILE* file);
BBranch* bbranch_load(FILE* file);

void btree_insert(int val, int idx, BTree* tree);
BLeaf* bleaf_insert(int val, int idx, BLeaf* leaf);
BBranch* bbranch_insert(int val, int idx, BBranch* branch);

void btree_delete(int val, int idx, BTree* tree);
void bleaf_delete(int val, int idx, BLeaf* leaf);
void bbranch_delete(int val, int idx, BBranch* branch);

void btree_free(BTree* tree);
void bleaf_free(BLeaf* leaf);
void bbranch_free(BBranch* branch);

void btree_print(BTree* tree);
void bleaf_print(BLeaf* leaf, size_t level);
void bbranch_print(BBranch* branch, size_t level);

int bbranch_min(BBranch* branch);

size_t btree_scan_between(BTree* tree, int* results, int min, int max);
size_t btree_scan_at_least(BTree* tree, int* results, int min);
size_t btree_scan_less_than(BTree* tree, int* results, int max);

size_t bleaf_scan_all(BLeaf* leaf, int* results);
size_t bleaf_scan_between(BLeaf* leaf, int* results, int min, int max);
size_t bleaf_scan_at_least(BLeaf* leaf, int* results, int min);
size_t bleaf_scan_less_than(BLeaf* leaf, int* results, int max);

size_t bbranch_scan_all(BBranch* branch, int* results);
size_t bbranch_scan_between(BBranch* branch, int* results, int min, int max);
size_t bbranch_scan_at_least(BBranch* branch, int* results, int min);
size_t bbranch_scan_less_than(BBranch* branch, int* results, int max);

//  Leaves:
//  
// idxs
//  |
//  v
//  a  b  c  d  e  f  g  h  k  j 
//  1  3  4  7  9 10 16 18 22 22
//
//  Insertion:
//    find the first index greater than or eq to the value (could be 1+length)
//    shift everything starting there up one
//    put the value in
//    
//  Scan:
//    Values >=5 and <18:
//      First qualifying index from left = 3    (7)
//      Last qualifying index from right = 6  (16)
//      Select from idxs+3 with size 4
//  
//    Values >= 5:
//      First qualifying index from left = 3   (7)
//      Select from idxs+3 with size 7
//  
//    Values < 7:
//      First qualifying index from right = 2  (4)
//      Select from idxs+0 with size 3
//  
//    Values >= 1 and <23:
//      First qualifying index from left = 0
//      Last qualifying index from right = 9
//      Select from idxs+0 with size 10
//  
//    Values >= 1 and <22:
//      First qualifying index from left = 0
//      Last qualifying index from right = 7
//      Select from idxs+0 with size 8
//
//
//  Branches:
//
//    keys
//     |
//     v
//     5  10  15  20  25
//   a  b   c   d   e    f
//   ^
//   |
//  kids
//
//  Insertion(v):
//    We want to find the last keys index less than or equal to the key.
//    If we insert 3, that should be -1.
//    If we insert 5, that should be 0.
//    If we insert 6, that should be 0.
//    If we insert 10, that should be 1.
//    Etc.
//    Then add 1 to get the kid index.
//    Each key points to the minimum value in the next bucket.
//
//  If we select values <3,

#endif
