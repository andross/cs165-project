#ifndef COLUMN_H
#define COLUMN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "btree.h"

#define MAX_SIZE_NAME 64

typedef struct Column {
    char name[MAX_SIZE_NAME]; 
    int* data;
    size_t length;
    size_t sorted_until;
    bool is_sorted;
    BTree* btree;
} Column;

Column* init_column(char* column_name, size_t row_capacity);
Column* load_column(char* table_dir, char* column_name, size_t row_count, size_t row_capacity, int is_sorted, int has_btree);
Column* replicate_column(Column* column, size_t row_capacity);
void sync_column(Column* column, char* table_dir, size_t row_count);
void free_column(Column* column);
void rebuild_column_index(Column* column);

#endif /* COLUMN_H */
