#ifndef HASH_JOIN_H
#define HASH_JOIN_H

#include "result.h"
#include "hash_table.h"

void hash_join(
    Result* Lval, Result* Lpos,
    Result* Rval, Result* Rpos,
    Result* Lres, Result* Rres);

#endif
