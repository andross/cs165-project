#ifndef EXTERNAL_SORT_H
#define EXTERNAL_SORT_H

#include <stdlib.h>
#include <string.h>
#include "utils.h"

typedef struct ExternalSortRun {
  size_t length;
  size_t width;
  size_t leader;
  int** arrays;
} ExternalSortRun;

void group_external_sort(int** arrays, size_t length, size_t width, size_t leader, size_t runsize);
ExternalSortRun* init_external_sort_run(size_t length, size_t width, size_t leader);
ExternalSortRun* merge_external_sort_runs(ExternalSortRun* run1, ExternalSortRun* run2); 
void free_external_sort_run(ExternalSortRun* run);

#endif
