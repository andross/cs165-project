#ifndef C_SV_H
#define C_SV_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#define MAX_VALUE_CHARS 256
#define MAX_COLUMN_COUNT 64
#define MAX_LINE_LENGTH 1024

typedef struct CsvReader {
  FILE* file;
  char headers[MAX_COLUMN_COUNT][MAX_VALUE_CHARS];
  char current_row[MAX_LINE_LENGTH];
  int n_columns;
  int n_rows_read;
} CsvReader;

typedef struct CsvWriter {
  FILE* file;
  char headers[MAX_COLUMN_COUNT][MAX_VALUE_CHARS];
  char rowvals[MAX_COLUMN_COUNT][MAX_VALUE_CHARS];
  int n_columns;
} CsvWriter;

CsvReader* csv_reader(char* filename);
void free_csv_reader(CsvReader* csv);
void read_csv_cell(char* result, char* cell_name, CsvReader* csv);
char* read_csv_row(CsvReader* csv);

CsvWriter* csv_writer(char* filename, int n_columns, ...);
void free_csv_writer(CsvWriter* csv);
void write_csv_cell(char* value, char* cell_name, CsvWriter* csv);
void write_csv_row(CsvWriter* csv);

#endif /* C_SV_H */
