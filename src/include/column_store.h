#ifndef COLUMN_STORE_H
#define COLUMN_STORE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "c_sv.h"
#include "db.h"

#define DEFAULT_DB_CAPACITY 10

typedef struct ColumnStore {
    Db** dbs;
    size_t db_count;
    size_t db_capacity;
    char data_dir[1024];
} ColumnStore;

ColumnStore* load_column_store(char* data_dir);
void sync_column_store(ColumnStore* store);
void free_column_store(ColumnStore* store);
Db* create_db(ColumnStore* store, char* db_name);
Db* find_db(ColumnStore* store, char* db_name);
void load_data_csv(ColumnStore* store, char* filename);

#endif /* COLUMN_STORE_H */
