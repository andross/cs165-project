#ifndef SELECT_H
#define SELECT_H

#include <stdlib.h>
#include "comparator.h"
#include "result.h"

size_t vanilla_select(int* values, int* results, Comparator comp, size_t max_value_count, size_t offset);
size_t position_select(int* values, int* positions, int* results, Comparator comp, size_t max_value_count);
void execute_select(int* values, int* positions, Result* result, Comparator comp, size_t max_value_count);
size_t ifselect_less_than(long int max, int* values, int* results, size_t n, size_t offset);

#endif
