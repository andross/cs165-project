#ifndef SHARED_SCAN_H
#define SHARED_SCAN_H

#include "db_operator.h"
#include "utils.h"
#include "select.h"
#include <pthread.h>

#define SHARED_SCAN_MAX_SCANNERS 32
#define SHARED_SCAN_VECTOR_SIZE 1024
#define N_SCANNING_THREADS 32

typedef struct SharedScan {
  Column* column;
  size_t scanner_count;
  size_t vector_size;
  DbOperator* scanners[SHARED_SCAN_MAX_SCANNERS];
} SharedScan; 

SharedScan* init_shared_scan(Column* column, size_t vector_size);
void add_shared_scanner(SharedScan* ss, DbOperator* select_dbo);
void do_shared_scan(SharedScan* ss);
void do_threaded_scan(SharedScan* ss);
void free_shared_scan(SharedScan* ss);
void scan_vector(int* vector, size_t n, DbOperator* dbo, size_t offset);

#endif
