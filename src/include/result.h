#ifndef RESULT_H 
#define RESULT_H

#include <stdlib.h>
#include "table.h"

typedef enum DataType {
     INT,
     LONG,
     FLOAT
} DataType;

typedef struct Result {
    DataType value_type;
    size_t value_count;
    void *values;
    int used_btree_scan;
    int used_array_scan;
    int used_sorted_scan;
    Table* relative_to;
} Result;

#endif
