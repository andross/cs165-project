#ifndef DB_H
#define DB_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "c_sv.h"
#include "table.h"

#define MAX_SIZE_NAME 64
#define DEFAULT_TABLE_CAPACITY 100

typedef struct Db {
    char name[MAX_SIZE_NAME]; 
    Table **tables;
    size_t table_count;
    size_t table_capacity;
} Db;

Db* init_db(char* db_name, size_t table_capacity);
Db* load_db(char* store_dir, char* db_name, size_t table_capacity);
void sync_db(Db* db, char* store_dir);
void free_db(Db* db);
Table* create_table(Db* db, char* table_name, size_t column_capacity);
Table* find_table(Db* db, char* table_name);

#endif /* DB_H */
