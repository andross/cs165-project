#ifndef GENERALIZED_COLUMN_H 
#define GENERALIZED_COLUMN_H

#include "column.h"
#include "result.h"

typedef enum GeneralizedColumnType {
    RESULT,
    COLUMN
} GeneralizedColumnType;

typedef union GeneralizedColumnPointer {
    Result* result;
    Column* column;
} GeneralizedColumnPointer;

typedef struct GeneralizedColumn {
    GeneralizedColumnType column_type;
    GeneralizedColumnPointer column_pointer;
} GeneralizedColumn;

int* generalized_column_values(GeneralizedColumn c);
size_t generalized_column_length(GeneralizedColumn c);

#endif
