#ifndef BITVECTOR_H
#define BITVECTOR_H

#define WORDSIZE 32
#define BITS_WS 5
#define MASK 0x1f

#define bitvector_get(bv, i) ((bv[i>>BITS_WS] & (1<<(i & MASK))) > 0)
#define bitvector_set(bv, i) (bv[i>>BITS_WS] |= (1<<(i & MASK)))
#define bitvector_init(l) calloc(l/WORDSIZE+1, sizeof(int))

#endif
