#ifndef DB_OPERATOR_H
#define DB_OPERATOR_H

#include "result.h"
#include "table.h"
#include "comparator.h"
#include "generalized_column.h"
#include "select.h"
#include "nested_loop_join.h"
#include "hash_join.h"
#include "bitvector.h"

#define PRINT_RESULT_CAPACITY 32
#define BTREE_MAX_SELECTIVITY 0.25

typedef enum DbOperationStatus {
  DB_OPERATION_OK,
  DB_OPERATION_ERROR
} DbOperationStatus;

typedef struct DbOperationResult {
  DbOperationStatus status;
  char* message;
  size_t message_length;
} DbOperationResult;

typedef struct InsertOperator {
  Table* table;
  int* values;
} InsertOperator;

typedef struct FetchOperator {
  GeneralizedColumn value_column;
  Result* index_vector;
  Result* result;
} FetchOperator;

typedef struct SelectOperator {
  GeneralizedColumn value_column;
  Result* index_vector;
  Result* result;
  Comparator comparator;
  int* pending_deletes;
} SelectOperator;

typedef struct PrintOperator {
  size_t result_count;
  size_t result_capacity;
  Result** results;
} PrintOperator;

typedef struct AvgOperator {
  GeneralizedColumn value_column;
  Result* result;
} AvgOperator;

typedef struct SumOperator {
  GeneralizedColumn value_column;
  Result* result;
} SumOperator;

typedef struct AddOperator {
  GeneralizedColumn value_column1;
  GeneralizedColumn value_column2;
  Result* result;
} AddOperator;

typedef struct SubOperator {
  GeneralizedColumn value_column1;
  GeneralizedColumn value_column2;
  Result* result;
} SubOperator;

typedef struct MaxOperator {
  GeneralizedColumn value_column;
  Result* result;
} MaxOperator;

typedef struct MinOperator {
  GeneralizedColumn value_column;
  Result* result;
} MinOperator;

typedef enum JoinType {
  HASH,
  NESTED_LOOP,
} JoinType;

typedef struct JoinOperator {
  Result* pos1;
  Result* val1;
  Result* pos2;
  Result* val2;
  JoinType join_type;
  Result* res1;
  Result* res2;
} JoinOperator;

typedef struct DeleteOperator {
  Table* table;
  Result* index_vector;
} DeleteOperator;

typedef struct UpdateOperator {
  Table* table;
  Column* column;
  Result* index_vector;
  int new_value;
} UpdateOperator;

typedef enum OperatorType {
  CREATE,
  INSERT,
  UPDATE,
  DELETE,
  SELECT,
  FETCH,
  PRINT,
  AVG,
  SUM,
  ADD,
  SUB,
  MAX,
  MIN,
  JOIN,
} OperatorType;

typedef union OperatorFields {
  InsertOperator insert_operator;
  UpdateOperator update_operator;
  DeleteOperator delete_operator;
  SelectOperator select_operator;
  FetchOperator fetch_operator;
  PrintOperator print_operator;
  AvgOperator avg_operator;
  SumOperator sum_operator;
  AddOperator add_operator;
  SubOperator sub_operator;
  MaxOperator max_operator;
  MinOperator min_operator;
  JoinOperator join_operator;
} OperatorFields;

typedef struct DbOperator {
  OperatorType type;
  OperatorFields operator_fields;
} DbOperator;

DbOperationResult execute_db_operator(DbOperator* dbo);

void free_db_operator(DbOperator* dbo);

Column* column_for_select(DbOperator* dbo);

void execute_sorted_scan(Column* col, Result* result, Comparator comp);
void execute_btree_scan(Column* col, Result* result, Comparator comp);
void execute_array_scan(SelectOperator sop);

#endif
