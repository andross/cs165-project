#ifndef HASH_TABLE_GUARD
#define HASH_TABLE_GUARD

#include <stdio.h>
#include <stdlib.h>

typedef int keyType;
typedef int valType;

#define VALS_PER_NODE 2048
#define LISTS_PER_HASH 512
#define MAX_VALS_PER_LIST VALS_PER_NODE*4

// define list and hash structs
typedef struct _ValueListStruct {
  int n_vals;
  keyType keys[VALS_PER_NODE];
  valType vals[VALS_PER_NODE];
  struct _ValueListStruct* next;
} ValueList;

typedef struct _HashtableStruct {
  int n_vals;
  int n_lists;
  ValueList** lists;
} Hashtable;

// some constructor/destructor functions for them
Hashtable* newHashtable();
ValueList* newValueList();
void freeHashtable(Hashtable* hash);
void freeValueList(ValueList* hash);

// functions to add/remove/get
void valueListPush(ValueList* node, keyType key, valType val);
void hashtablePut(Hashtable* hash, keyType key, valType val);
void hashtableErase(Hashtable* hash, keyType keyToErase);
int hashtableGet(Hashtable* hash, keyType key, valType *result, int n_to_copy);

// the hash function
int hashIndex(keyType key, int n_lists);

// printers for debugging
void printValueList();
void printHashtable();

#endif
