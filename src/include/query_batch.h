#ifndef QUERY_BATCH_H
#define QUERY_BATCH_H

#define MAX_BATCH_QUERIES 32

#include "db_operator.h"
#include "shared_scan.h"

typedef struct QueryBatch {
  DbOperator* query_queue[MAX_BATCH_QUERIES];
  size_t query_count;
} QueryBatch;

QueryBatch* init_query_batch();
void free_query_batch(QueryBatch* qb);
void batch_execute(QueryBatch* qb);
void batch_enqueue(QueryBatch* qb, DbOperator* dbo);

#endif
