#ifndef CLIENT_CONTEXT_H
#define CLIENT_CONTEXT_H

#include "result.h"
#include "generalized_column.h"
#include "query_batch.h"

#define VARIABLE_NAME_LENGTH 64
#define DEFAULT_VARIABLE_CAPACITY 128

typedef struct ClientVariable {
    char name[VARIABLE_NAME_LENGTH];
    Result* value;
} ClientVariable;
/*
 * holds the information necessary to refer to generalized columns (results or columns)
 */
typedef struct ClientContext {
    ClientVariable** variables;
    size_t variable_count;
    size_t variable_capacity;
    QueryBatch* current_batch;
} ClientContext;

ClientContext* init_client_context(void);

Result* get_client_variable(ClientContext* c, char* variable_name);

void add_client_variable(ClientContext* c, ClientVariable* variable);

void set_client_variable(ClientContext* c, char* variable_name, Result* res);

int has_client_variable(ClientContext* c, char* variable_name);

void free_client_context(ClientContext* c);

#endif
