#ifndef NESTED_LOOP_JOIN_H
#define NESTED_LOOP_JOIN_H

#include "result.h"

void nested_loop_join(
    Result* Lval, Result* Lpos,
    Result* Rval, Result* Rpos,
    Result* Lres, Result* Rres);

void block_nested_loop_join(
    Result* Lval, Result* Lpos,
    Result* Rval, Result* Rpos,
    Result* Lres, Result* Rres,
    size_t pagesize);

#endif
