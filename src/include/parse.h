#ifndef PARSE_H__
#define PARSE_H__
#include "cs165_api.h"
#include "message.h"
#include "client_context.h"
#include "db_operator.h"
#include "utils.h"

DbOperator* parse_command(char* query_command, ColumnStore* store, message* send_message, int client, ClientContext* context);

// want to test some of the more private methods in semi-isolation
DbOperator* parse_create(char* create_arguments, ColumnStore* store, message* send_message);
DbOperator* parse_insert(char* insert_arguments, ColumnStore* store, message* send_message);

#endif
