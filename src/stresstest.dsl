-- Load+create Data and shut down of tbl1 which has 1 attribute only
create(db,"db1")
create(tbl,"tbl6",db1,1)
create(col,"col1",db1.tbl6)
create(col,"col2",db1.tbl6)
create(col,"col3",db1.tbl6)
create(col,"col4",db1.tbl6)
load("../data6.csv")
s1=select(db1.tbl6.col1,null,null)
avg=avg(s1)
print(avg)
shutdown

