#include "shared_scan.h"

SharedScan* init_shared_scan(Column* column, size_t vector_size) {
  SharedScan* ss = malloc(sizeof(SharedScan));
  ss->column = column;
  ss->scanner_count = 0;
  ss->vector_size = vector_size;
  return ss;
}

void add_shared_scanner(SharedScan* ss, DbOperator* dbo) {
  if (ss->scanner_count >= SHARED_SCAN_MAX_SCANNERS) {
    log_err("trying to add too many shared scanners\n");
  } else if (column_for_select(dbo) != ss->column) {
    log_err("column for select does not match shared scan column!\n");
  } else {
    ss->scanners[ss->scanner_count] = dbo;
    ss->scanner_count++;
  }
}

int n_scanned;

void* _scan_next(void* args) {
  SharedScan* ss = (SharedScan*)args;

  int i = __sync_fetch_and_add(&n_scanned, 1);
  while (i < (int)ss->scanner_count) {
    execute_array_scan(ss->scanners[i]->operator_fields.select_operator);
    i = __sync_fetch_and_add(&n_scanned, 1);
  }

  return NULL;
}

void do_threaded_scan(SharedScan* ss) {
  n_scanned = 0;
  int n_threads = MIN(N_SCANNING_THREADS, (int)ss->scanner_count);
  pthread_t threads[n_threads];
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  for (int s = 0; s < n_threads; s++)
    pthread_create(&threads[s], &attr, _scan_next, ss);
  for (int s = 0; s < n_threads; s++)
    pthread_join(threads[s], NULL);
}

void do_shared_scan(SharedScan* ss) {
  int* array = ss->column->data;
  size_t todo = ss->column->length;
  size_t chunk;
  size_t i = 0;
  while (todo > 0) {
    chunk = MIN(ss->vector_size, todo);
    for (size_t s = 0; s < ss->scanner_count; s++)
      scan_vector(array+i, chunk, ss->scanners[s], i);
    i += chunk;
    todo -= chunk;
  }
}

void scan_vector(int* vec, size_t n, DbOperator* dbo, size_t offset) {
  SelectOperator sop = dbo->operator_fields.select_operator;
  Result* r = sop.result;
  int* vals = (int*)r->values + r->value_count;
  r->value_count += vanilla_select(vec, vals, sop.comparator, n, offset);
}

void free_shared_scan(SharedScan* ss) {
  free(ss);
}
