#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/stat.h>
#include "utils.h"

/*#define LOG_ERR 1*/
/*#define LOG_INFO 1*/

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int is_already_sorted(int* values, size_t length) {
  if (length == 0) return 1;
  int prev = values[0];
  for (size_t i = 1; i < length; i++) {
    if (prev > values[i]) return 0;
    else prev = values[i];
  }
  return 1;
}

size_t group_partition(int* array, int** arrays, int n_arrays, long int l, long int r) {
  long int p = l - 1;
  int tmp;
  for (long int i = l; i <= r; i++) {
    if (array[i] <= array[r]) {
      p++;
      for (int j = 0; j < n_arrays; j++) {
        tmp = arrays[j][p];
        arrays[j][p] = arrays[j][i];
        arrays[j][i] = tmp;
      }
    }
  }
  return p;
}

void group_quicksort_chunk(int* array, int** arrays, int n_arrays, long int l, long int r) {
  if (l < r) {
    long int p = group_partition(array, arrays, n_arrays, l, r);
    group_quicksort_chunk(array, arrays, n_arrays, l, p-1);
    group_quicksort_chunk(array, arrays, n_arrays, p+1, r);
  }
}

void group_quicksort(int* array, int** arrays, int n_arrays, size_t length) {
  group_quicksort_chunk(array, arrays, n_arrays, 0, (long int)length-1);
}

void quicksort(int* array, size_t length) {
  int* arrays[1];
  arrays[0] = array;
  group_quicksort(array, arrays, 1, length);
}

void shuffle(int* array, size_t length) {
  for (size_t i = 0; i < length - 1; i++) {
    size_t j = i + rand() / (RAND_MAX / (length - i) + 1);
    int temp = array[j];
    array[j] = array[i];
    array[i] = temp;
  }
}

int bisect_right(int value, int* values, int length) {
  int min = 0;
  int max = length;
  int mid;

  while (min < max) {
    mid = (min + max) / 2;
    if (value < values[mid]) max = mid;
    else min = mid+1;
  }

  return min;
}

int bisect_left(int value, int* values, int length) {
  int min = 0;
  int max = length;
  int mid;

  while (min < max) {
    mid = (min + max) / 2;
    if (value > values[mid]) min = mid+1;
    else max = mid;
  }

  return min;
}

int index_to_insert(int value, int* values, int length) {
  return bisect_right(value, values, length);
}

int first_index_at_least(int value, int* values, int length) {
  return bisect_left(value, values, length);
}

int last_index_less_than(int value, int* values, int length) {
  return bisect_left(value, values, length) - 1;
}

void system_mkdir_p(char* dirname) {
  char command[1024];
  sprintf(command, "mkdir -p %s", dirname);
  system(command);
}

int file_exists(char* filename) {
  struct stat s;
  return (stat(filename, &s) == 0);
}

void filename_join(char* name, int n, ...) {
  va_list args;
  va_start(args, n);
  strcpy(name, va_arg(args, char*));
  strcat(name, "/");
  for (int i = 1; i < n-1; i++) {
    strcat(name, va_arg(args, char*));
    strcat(name, "/");
  }
  strcat(name, va_arg(args, char*));
  va_end(args);
}

char* trim_newline(char *str) {
    int length = strlen(str);
    int current = 0;
    for (int i = 0; i < length; ++i) {
        if (!(str[i] == '\r' || str[i] == '\n')) {
            str[current++] = str[i];
        }
    }

    // Write new null terminator
    str[current] = '\0';
    return str;
}

char* trim_whitespace(char *str)
{
    int length = strlen(str);
    int current = 0;
    for (int i = 0; i < length; ++i) {
        if (!isspace(str[i])) {
            str[current++] = str[i];
        }
    }

    // Write new null terminator
    str[current] = '\0';
    return str;
}

char* trim_parenthesis(char *str) {
    int length = strlen(str);
    int current = 0;
    for (int i = 0; i < length; ++i) {
        if (!(str[i] == '(' || str[i] == ')')) {
            str[current++] = str[i];
        }
    }

    // Write new null terminator
    str[current] = '\0';
    return str;
}

char* trim_quotes(char *str) {
    int length = strlen(str);
    int current = 0;
    for (int i = 0; i < length; ++i) {
        if (str[i] != '\"') {
            str[current++] = str[i];
        }
    }

    // Write new null terminator
    str[current] = '\0';
    return str;
}

void cs165_log(FILE* out, const char *format, ...) {
#ifdef LOG
    va_list v;
    va_start(v, format);
    vfprintf(out, format, v);
    va_end(v);
#else
    (void) out;
    (void) format;
#endif
}

void log_err(const char *format, ...) {
#ifdef LOG_ERR
    va_list v;
    va_start(v, format);
    fprintf(stderr, ANSI_COLOR_RED);
    vfprintf(stderr, format, v);
    fprintf(stderr, ANSI_COLOR_RESET);
    va_end(v);
#else
    (void) format;
#endif
}

void log_info(const char *format, ...) {
#ifdef LOG_INFO
    va_list v;
    va_start(v, format);
    fprintf(stdout, ANSI_COLOR_GREEN);
    vfprintf(stdout, format, v);
    fprintf(stdout, ANSI_COLOR_RESET);
    fflush(stdout);
    va_end(v);
#else
    (void) format;
#endif
}


