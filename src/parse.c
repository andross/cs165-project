#define _BSD_SOURCE
#include <string.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <ctype.h>
#include "cs165_api.h"
#include "parse.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

// macro that will return early (or late) from any parse function with the
// right status, freeing the right objects, returning the operator or null
#define EXIT_WITH(s) do {\
  msg->status = s;\
  free(to_free);\
  if (s == OK_DONE || s == OK_WAIT_FOR_RESPONSE) {\
    return dbo;\
  } else {\
    free_db_operator(dbo);\
    return NULL;\
  }\
} while(0)

// macro that will split a `string` by `sep` and assign its results to `target`,
// failing if the assignment is null
#define SPLIT_STRING(string, target, sep)\
  char* target;\
  do {\
    target = strsep(&string, sep);\
    if (target == NULL)\
      EXIT_WITH(INCORRECT_FORMAT);\
  } while(0)

// macro to copy a string and assign that to `target`, and also record it as
// needing to be freed.
#define COPY_STRING(target, source)\
  char *target, *to_free; do {\
    target = to_free = malloc((strlen(source)+1) * sizeof(char));\
    strcpy(target, source);\
  } while(0)

// macro that will cause an early return if a token has too many segments
#define ASSERT_NO_MORE(string, sep)\
  if (strsep(&string, sep) != NULL)\
    EXIT_WITH(INCORRECT_FORMAT);

// macro to remove starting and ending parens, failing if these are not present
#define STRIP_PARENS(string) do {\
  if (strncmp(string, "(", 1) != 0)\
    EXIT_WITH(UNKNOWN_COMMAND);\
  string++;\
  int last_char = strlen(string) - 1;\
  if (last_char < 0 || string[last_char] != ')')\
    EXIT_WITH(INCORRECT_FORMAT);\
  string[last_char] = '\0';\
} while(0)

// macro to find a table by name and assign its results to a variable, failing
// with the appropriate error if something doesn't work
#define RESOLVE_TABLE(table, name)\
  Table* table; do {\
    char* db_name = strsep(&name, ".");  if (db_name == NULL)    EXIT_WITH(INCORRECT_FORMAT);\
    char* tbl_name = strsep(&name, "."); if (tbl_name == NULL)   EXIT_WITH(INCORRECT_FORMAT);\
    if (strsep(&name, ".") != NULL) EXIT_WITH(INCORRECT_FORMAT);\
    Db* db = find_db(store, db_name); if (db == NULL)     EXIT_WITH(OBJECT_NOT_FOUND);\
    table = find_table(db, tbl_name); if (table == NULL)  EXIT_WITH(OBJECT_NOT_FOUND);\
  } while(0)

// similar macro for columns, but without the assignment
#define RESOLVE_COLUMN(column, name) do {\
  char* db_name = strsep(&name, ".");  if (db_name == NULL)    EXIT_WITH(INCORRECT_FORMAT);\
  char* tbl_name = strsep(&name, "."); if (tbl_name == NULL)   EXIT_WITH(INCORRECT_FORMAT);\
  char* col_name = strsep(&name, "."); if (col_name == NULL)   EXIT_WITH(INCORRECT_FORMAT);\
  if (strsep(&name, ".") != NULL) EXIT_WITH(INCORRECT_FORMAT);\
  Db* db = find_db(store, db_name);        if (db == NULL)     EXIT_WITH(OBJECT_NOT_FOUND);\
  Table* table = find_table(db, tbl_name); if (table == NULL)  EXIT_WITH(OBJECT_NOT_FOUND);\
  column = find_column(table, col_name);   if (column == NULL) EXIT_WITH(OBJECT_NOT_FOUND);\
} while(0)

#define RESOLVE_ALL(name)\
  Db* db; Table* table; Column* column; do {\
  char* db_name = strsep(&name, ".");  if (db_name == NULL)    EXIT_WITH(INCORRECT_FORMAT);\
  char* tbl_name = strsep(&name, "."); if (tbl_name == NULL)   EXIT_WITH(INCORRECT_FORMAT);\
  char* col_name = strsep(&name, "."); if (col_name == NULL)   EXIT_WITH(INCORRECT_FORMAT);\
  if (strsep(&name, ".") != NULL) EXIT_WITH(INCORRECT_FORMAT);\
  db = find_db(store, db_name);        if (db == NULL)     EXIT_WITH(OBJECT_NOT_FOUND);\
  table = find_table(db, tbl_name); if (table == NULL)  EXIT_WITH(OBJECT_NOT_FOUND);\
  column = find_column(table, col_name);   if (column == NULL) EXIT_WITH(OBJECT_NOT_FOUND);\
} while(0)

// combined macro to resolve a generalized column, which could be either a real
// column or a client variable.
#define RESOLVE_GENERALIZED_COLUMN(gcol, name) do {\
  if (strchr(name, '.') == NULL) {\
    if (!has_client_variable(context, name))\
      EXIT_WITH(OBJECT_NOT_FOUND);\
    gcol.column_type = RESULT;\
    gcol.column_pointer.result = get_client_variable(context, name);\
  } else {\
    Column* column = NULL;\
    RESOLVE_COLUMN(column, name);\
    gcol.column_type = COLUMN;\
    gcol.column_pointer.column = column;\
  }\
} while(0)

// macro to initialize a result variable with a given type and link it to both
// an operator and a client context.
#define SETUP_RESULT(variable_name, variable_type, operator) do {\
  Result* result = malloc(sizeof(Result));\
  result->value_type = variable_type;\
  result->value_count = (size_t)0;\
  result->values = NULL;\
  operator.result = result;\
  set_client_variable(context, variable_name, result);\
} while(0)

DbOperator* parse_create_db(char* _args, ColumnStore* store, message* msg) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  SPLIT_STRING(arguments, db_name, ",");
  ASSERT_NO_MORE(arguments, ",");

  db_name = trim_quotes(db_name);
  Db* existing_db = find_db(store, db_name);
  if (existing_db != NULL)
    EXIT_WITH(OBJECT_ALREADY_EXISTS);

  create_db(store, db_name);

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_create_tbl(char* _args, ColumnStore* store, message* msg) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  SPLIT_STRING(arguments, table_name, ",");
  SPLIT_STRING(arguments, db_name, ",");
  SPLIT_STRING(arguments, col_count_string, ",");
  ASSERT_NO_MORE(arguments, ",");

  Db* db = find_db(store, db_name);
  if (db == NULL)
    EXIT_WITH(OBJECT_NOT_FOUND);

  table_name = trim_quotes(table_name);
  if (find_table(db, table_name) != NULL)
    EXIT_WITH(OBJECT_ALREADY_EXISTS);

  size_t column_count = (size_t)atoi(col_count_string);
  if (column_count < 1)
    EXIT_WITH(INCORRECT_FORMAT);

  create_table(db, table_name, column_count);

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_create_col(char* _args, ColumnStore* store, message* msg) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  SPLIT_STRING(arguments, column_name, ",");
  SPLIT_STRING(arguments, joint_name, ",");
  ASSERT_NO_MORE(arguments, ",");
  RESOLVE_TABLE(table, joint_name);

  column_name = trim_quotes(column_name);
  Column* existing_column = find_column(table, column_name);
  if (existing_column != NULL)
    EXIT_WITH(OBJECT_ALREADY_EXISTS);

  create_column(table, column_name);

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_create_idx(char* _args, ColumnStore* store, message* msg) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  SPLIT_STRING(arguments, joint_name, ",");
  // ignore for now, always use sorted (btree) for clustered (unclustered)
  SPLIT_STRING(arguments, sorted_or_btree, ","); 
  SPLIT_STRING(arguments, clustered_or_not, ",");
  ASSERT_NO_MORE(arguments, ",");

  RESOLVE_ALL(joint_name);
  int success;
  if (strcmp(clustered_or_not, "clustered") == 0)
    success = create_primary_index(table, column->name);
  else if (strcmp(clustered_or_not, "unclustered") == 0)
    success = create_secondary_index(table, column->name);
  else
    EXIT_WITH(INCORRECT_FORMAT);

  if (!success) EXIT_WITH(OBJECT_ALREADY_EXISTS);

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_create(char* _args, ColumnStore* store, message* msg) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, create_type, ",");

  if (strcmp(create_type, "db") == 0)
    dbo = parse_create_db(arguments, store, msg);
  else if (strcmp(create_type, "tbl") == 0)
    dbo = parse_create_tbl(arguments, store, msg);
  else if (strcmp(create_type, "col") == 0)
    dbo = parse_create_col(arguments, store, msg);
  else if (strcmp(create_type, "idx") == 0)
    dbo = parse_create_idx(arguments, store, msg);
  else
    EXIT_WITH(UNKNOWN_COMMAND);

  free(to_free);
  return dbo;
}

DbOperator* parse_select(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = SELECT;
  dbo->operator_fields.select_operator.pending_deletes = NULL;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, arg1, ",");
  SPLIT_STRING(arguments, arg2, ",");
  SPLIT_STRING(arguments, arg3, ",");
  char* arg4 = strsep(&arguments, ",");

  char* value_name = NULL;
  char* index_name = NULL;
  char* min_string = NULL;
  char* max_string = NULL;

  SETUP_RESULT(result_variable_name, INT, dbo->operator_fields.select_operator);
  Result* result = dbo->operator_fields.select_operator.result;

  if (arg4 == NULL) {
    value_name = arg1; // just selecting all values from a column
    min_string = arg2;
    max_string = arg3;
    RESOLVE_ALL(value_name);
    table = best_replica_for(column->name, table);
    column = find_column(table, column->name);
    result->relative_to = table;
    result->values = malloc(column->length * sizeof(int));
    dbo->operator_fields.select_operator.value_column.column_type = COLUMN;
    dbo->operator_fields.select_operator.value_column.column_pointer.column = column;
    dbo->operator_fields.select_operator.index_vector = NULL;
    dbo->operator_fields.select_operator.pending_deletes = table->pending_deletes;
  } else {
    ASSERT_NO_MORE(arguments, ",");
    index_name = arg1;
    value_name = arg2;
    min_string = arg3;
    max_string = arg4;
    if (!has_client_variable(context, index_name))
      EXIT_WITH(OBJECT_NOT_FOUND);
    Result* index_result = get_client_variable(context, index_name);
    result->relative_to = index_result->relative_to;
    result->values = malloc(index_result->value_count * sizeof(int));
    if (has_client_variable(context, value_name)) {
      Result* value_result = get_client_variable(context, value_name);
      if (value_result->relative_to != index_result->relative_to) {
        log_err("trying to select values from column with bad positions\n");
        EXIT_WITH(QUERY_UNSUPPORTED);
      }
      dbo->operator_fields.select_operator.value_column.column_type = RESULT;
      dbo->operator_fields.select_operator.value_column.column_pointer.result = value_result;
    } else {
      RESOLVE_ALL(value_name);
      column = find_column(result->relative_to, column->name);
      dbo->operator_fields.select_operator.value_column.column_type = COLUMN;
      dbo->operator_fields.select_operator.value_column.column_pointer.column = column;
      dbo->operator_fields.select_operator.pending_deletes = result->relative_to->pending_deletes;
    }
    dbo->operator_fields.select_operator.index_vector = index_result;
  }

  ComparatorType min_ctype = NO_COMPARISON;
  ComparatorType max_ctype = NO_COMPARISON;
  long int min_value = 0;
  long int max_value = 0;
  if (strcmp(min_string, "null") != 0) {
    min_value = (long int)atoi(min_string);
    min_ctype = GREATER_THAN_OR_EQUAL;
  }
  if (strcmp(max_string, "null") != 0) {
    max_value = (long int)atoi(max_string);
    max_ctype = LESS_THAN;
  }
  dbo->operator_fields.select_operator.comparator.min_comparator_type = min_ctype;
  dbo->operator_fields.select_operator.comparator.max_comparator_type = max_ctype;
  dbo->operator_fields.select_operator.comparator.min_value = min_value;
  dbo->operator_fields.select_operator.comparator.max_value = max_value;

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_fetch(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = FETCH;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, value_name, ",");
  SPLIT_STRING(arguments, index_name, ",");
  ASSERT_NO_MORE(arguments, ",");

  if (!has_client_variable(context, index_name))
    EXIT_WITH(OBJECT_NOT_FOUND);

  Result* index_result = get_client_variable(context, index_name);
  dbo->operator_fields.fetch_operator.index_vector = index_result;

  SETUP_RESULT(result_variable_name, INT, dbo->operator_fields.fetch_operator);
  Result* result = dbo->operator_fields.fetch_operator.result;
  result->relative_to = index_result->relative_to;

  if (has_client_variable(context, value_name)) {
    Result* value_result = get_client_variable(context, value_name);
    if (value_result->relative_to != index_result->relative_to) {
      log_err("fetch values and indexes from different table orderings\n");
      EXIT_WITH(QUERY_UNSUPPORTED);
    }
    dbo->operator_fields.fetch_operator.value_column.column_type = RESULT;
    dbo->operator_fields.fetch_operator.value_column.column_pointer.result = value_result;
  } else {
    RESOLVE_ALL(value_name);
    column = find_column(result->relative_to, column->name);
    dbo->operator_fields.select_operator.value_column.column_type = COLUMN;
    dbo->operator_fields.select_operator.value_column.column_pointer.column = column;
  }

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_insert(char* _args, ColumnStore* store, message* msg) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, joint_name, ",");
  RESOLVE_TABLE(table, joint_name);

  dbo = malloc(sizeof(DbOperator));
  dbo->type = INSERT;
  dbo->operator_fields.insert_operator.table = table;
  dbo->operator_fields.insert_operator.values = malloc(sizeof(int) * table->column_count);

  size_t i = 0;
  char* token;
  while ((token = strsep(&arguments, ",")) != NULL)
    dbo->operator_fields.insert_operator.values[i++] = atoi(token);
  if (i != table->column_count)
    EXIT_WITH(INCORRECT_FORMAT);

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_delete(char* _args, ColumnStore* store, message* msg, ClientContext* context) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, joint_name, ",");
  RESOLVE_TABLE(table, joint_name);
  SPLIT_STRING(arguments, index_name, ",");
  ASSERT_NO_MORE(arguments, ",");

  if (!has_client_variable(context, index_name))
    EXIT_WITH(OBJECT_NOT_FOUND);

  Result* index_result = get_client_variable(context, index_name);

  dbo = malloc(sizeof(DbOperator));
  dbo->type = DELETE;
  dbo->operator_fields.delete_operator.table = table;
  dbo->operator_fields.delete_operator.index_vector = index_result;

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_update(char* _args, ColumnStore* store, message* msg, ClientContext* context) {
  DbOperator* dbo = NULL;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, value_name, ",");
  SPLIT_STRING(arguments, index_name, ",");
  SPLIT_STRING(arguments, new_value, ",");
  ASSERT_NO_MORE(arguments, ",");

  RESOLVE_ALL(value_name);

  if (!has_client_variable(context, index_name))
    EXIT_WITH(OBJECT_NOT_FOUND);
  Result* index_result = get_client_variable(context, index_name);

  dbo = malloc(sizeof(DbOperator));
  dbo->type = UPDATE;
  dbo->operator_fields.update_operator.index_vector = index_result;
  dbo->operator_fields.update_operator.table = table;
  dbo->operator_fields.update_operator.column = column;
  dbo->operator_fields.update_operator.new_value = atoi(new_value);

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_print(char* _args, message* msg, ClientContext* context) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = PRINT;
  dbo->operator_fields.print_operator.result_capacity = PRINT_RESULT_CAPACITY;
  dbo->operator_fields.print_operator.results = malloc(PRINT_RESULT_CAPACITY * sizeof(Result*));

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);

  size_t i = 0;
  char* variable_name;
  while ((variable_name = strsep(&arguments, ",")) != NULL) {
    if (!has_client_variable(context, variable_name))
      EXIT_WITH(OBJECT_NOT_FOUND);
    dbo->operator_fields.print_operator.results[i++] = get_client_variable(context, variable_name);
  }
  dbo->operator_fields.print_operator.result_count = i;

  EXIT_WITH(OK_WAIT_FOR_RESPONSE);
}

DbOperator* parse_load(char* _args, ColumnStore* store, message* msg, int client_socket) {
  // `load` on the client side will transform itself into, e.g,:
  //
  //      load(10000,2,db.tb.column1,db.tb.column2)
  //
  // so the arguments we receive are different than what the user types.
  //
  // We'll expect to receive a follow-up message with the actual payload
  // within this function.
  DbOperator* dbo = NULL;
  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);

  SPLIT_STRING(arguments, row_count_str, ",");
  SPLIT_STRING(arguments, col_count_str, ",");
  long int row_count = atoi(row_count_str);
  int col_count = atoi(col_count_str);

  Db* db = NULL;
  Table* table = NULL;

  for (int i = 0; i < col_count; i++) {
    SPLIT_STRING(arguments, joint_name, ",");
    SPLIT_STRING(joint_name, db_name, ".");
    SPLIT_STRING(joint_name, tbl_name, ".");
    SPLIT_STRING(joint_name, col_name, ".");

    if (table == NULL) {
      // on our first pass, find the db/table
      db = find_db(store, db_name);
      if (db == NULL) EXIT_WITH(OBJECT_NOT_FOUND);
      table = find_table(db, tbl_name);
      if (table == NULL) EXIT_WITH(OBJECT_NOT_FOUND);
    } else {
      // on subsequent passes, ensure it's the same db/table
      if (strcmp(db_name, db->name) != 0)
        EXIT_WITH(INCORRECT_FILE_FORMAT);
      if (strcmp(tbl_name, table->name) != 0)
        EXIT_WITH(INCORRECT_FILE_FORMAT);
    }

    Column* column = find_column(table, col_name);
    if (column == NULL)
      EXIT_WITH(OBJECT_NOT_FOUND);
  }

  // make sure we got the right number of columns (regardless of order)
  if ((int)table->column_count != col_count)
    EXIT_WITH(INCORRECT_FILE_FORMAT);

  // actually go ahead and receive the payload right here
  size_t payload_size = row_count * col_count * sizeof(int);
  int* payload = malloc(payload_size);
  ssize_t received = recv(client_socket, payload, payload_size, MSG_WAITALL);
  if (received <= 0) {
    log_err("didn't receive a CSV file!\n");
    EXIT_WITH(OBJECT_NOT_FOUND);
  }

  load_data_payload(table, row_count, payload);

  free(payload);
  EXIT_WITH(OK_DONE);
}

DbOperator* parse_avg(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = AVG;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.avg_operator.value_column, arguments);
  SETUP_RESULT(result_variable_name, FLOAT, dbo->operator_fields.avg_operator);
  EXIT_WITH(OK_DONE);
}

DbOperator* parse_sum(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = SUM;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.sum_operator.value_column, arguments);
  SETUP_RESULT(result_variable_name, LONG, dbo->operator_fields.sum_operator);
  EXIT_WITH(OK_DONE);
}

DbOperator* parse_add(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = ADD;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, v1, ",");
  SPLIT_STRING(arguments, v2, ",");
  ASSERT_NO_MORE(arguments, ",");
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.add_operator.value_column1, v1);
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.add_operator.value_column2, v2);
  SETUP_RESULT(result_variable_name, INT, dbo->operator_fields.add_operator);
  EXIT_WITH(OK_DONE);
}

DbOperator* parse_sub(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = SUB;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, v1, ",");
  SPLIT_STRING(arguments, v2, ",");
  ASSERT_NO_MORE(arguments, ",");
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.sub_operator.value_column1, v1);
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.sub_operator.value_column2, v2);
  SETUP_RESULT(result_variable_name, INT, dbo->operator_fields.sub_operator);
  EXIT_WITH(OK_DONE);
}

DbOperator* parse_min(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = MIN;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.min_operator.value_column, arguments);
  SETUP_RESULT(result_variable_name, INT, dbo->operator_fields.min_operator);
  EXIT_WITH(OK_DONE);
}

DbOperator* parse_max(char* _args, ColumnStore* store, message* msg, ClientContext* context, char* result_variable_name) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = MAX;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  RESOLVE_GENERALIZED_COLUMN(dbo->operator_fields.max_operator.value_column, arguments);
  SETUP_RESULT(result_variable_name, INT, dbo->operator_fields.max_operator);
  EXIT_WITH(OK_DONE);
}

#define RESOLVE_RESULT(dest, name) do {\
  if (has_client_variable(context, name))\
    dest = get_client_variable(context, name);\
  else\
    EXIT_WITH(OBJECT_NOT_FOUND);\
} while(0);

DbOperator* parse_join(char* _args, message* msg, ClientContext* context, char* result_variable_names) {
  DbOperator* dbo = malloc(sizeof(DbOperator));
  dbo->type = JOIN;

  COPY_STRING(arguments, _args);
  STRIP_PARENS(arguments);
  SPLIT_STRING(arguments, val1_name, ",");
  SPLIT_STRING(arguments, pos1_name, ",");
  SPLIT_STRING(arguments, val2_name, ",");
  SPLIT_STRING(arguments, pos2_name, ",");
  SPLIT_STRING(arguments, join_type, ",");
  ASSERT_NO_MORE(arguments, ",");

  SPLIT_STRING(result_variable_names, res1_name, ",");
  SPLIT_STRING(result_variable_names, res2_name, ",");
  ASSERT_NO_MORE(result_variable_names, ",");

  if (strcmp(join_type, "hash") == 0)
    dbo->operator_fields.join_operator.join_type = HASH;
  else if (strcmp(join_type, "nested-loop") == 0)
    dbo->operator_fields.join_operator.join_type = NESTED_LOOP;
  else
    EXIT_WITH(QUERY_UNSUPPORTED);

  RESOLVE_RESULT(dbo->operator_fields.join_operator.val1, val1_name);
  RESOLVE_RESULT(dbo->operator_fields.join_operator.pos1, pos1_name);
  RESOLVE_RESULT(dbo->operator_fields.join_operator.val2, val2_name);
  RESOLVE_RESULT(dbo->operator_fields.join_operator.pos2, pos2_name);
  
  size_t max_size = (dbo->operator_fields.join_operator.pos1->value_count *
                     dbo->operator_fields.join_operator.pos2->value_count);
  Result* res1 = malloc(sizeof(Result));
  Result* res2 = malloc(sizeof(Result));
  res1->values = malloc(sizeof(int) * max_size);
  res2->values = malloc(sizeof(int) * max_size);
  res1->value_count = 0;
  res2->value_count = 0;
  res1->value_type = INT;
  res2->value_type = INT;
  res1->relative_to = dbo->operator_fields.join_operator.pos1->relative_to;
  res2->relative_to = dbo->operator_fields.join_operator.pos2->relative_to;
  dbo->operator_fields.join_operator.res1 = res1;
  dbo->operator_fields.join_operator.res2 = res2;
  set_client_variable(context, res1_name, res1);
  set_client_variable(context, res2_name, res2);

  EXIT_WITH(OK_DONE);
}

DbOperator* parse_command(char* command, ColumnStore* store, message* msg, int client_socket, ClientContext* context) {
  DbOperator *dbo = NULL;

  COPY_STRING(arguments, command);

  if (strncmp(arguments, "--", 2) == 0)
    EXIT_WITH(OK_DONE);

  char* handle = NULL;
  if (strchr(arguments, '=') != NULL)
    handle = strsep(&arguments, "=");

  if (strncmp(arguments, "create", 6) == 0)
    dbo = parse_create(arguments+6, store, msg);
  else if (strncmp(arguments, "load", 4) == 0)
    dbo = parse_load(arguments+4, store, msg, client_socket);
  else if (strncmp(arguments, "avg", 3) == 0)
    dbo = parse_avg(arguments+3, store, msg, context, handle);
  else if (strncmp(arguments, "add", 3) == 0)
    dbo = parse_add(arguments+3, store, msg, context, handle);
  else if (strncmp(arguments, "sub", 3) == 0)
    dbo = parse_sub(arguments+3, store, msg, context, handle);
  else if (strncmp(arguments, "sum", 3) == 0)
    dbo = parse_sum(arguments+3, store, msg, context, handle);
  else if (strncmp(arguments, "min", 3) == 0)
    dbo = parse_min(arguments+3, store, msg, context, handle);
  else if (strncmp(arguments, "max", 3) == 0)
    dbo = parse_max(arguments+3, store, msg, context, handle);
  else if (strncmp(arguments, "join", 4) == 0)
    dbo = parse_join(arguments+4, msg, context, handle);
  else if (strncmp(arguments, "select", 6) == 0)
    dbo = parse_select(arguments+6, store, msg, context, handle);
  else if (strncmp(arguments, "fetch", 5) == 0)
    dbo = parse_fetch(arguments+5, store, msg, context, handle);
  else if (strncmp(arguments, "print", 5) == 0)
    dbo = parse_print(arguments+5, msg, context);
  else if (strncmp(arguments, "relational_insert", 17) == 0)
    dbo = parse_insert(arguments+17, store, msg);
  else if (strncmp(arguments, "relational_update", 17) == 0)
    dbo = parse_update(arguments+17, store, msg, context);
  else if (strncmp(arguments, "relational_delete", 17) == 0)
    dbo = parse_delete(arguments+17, store, msg, context);

  free(to_free);
  return dbo;
}
