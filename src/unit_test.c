#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "cs165_api.h"
#include "parse.h"
#include "c_utest.h"
#include "bitvector.h"
#include "external_sort.h"

#include "test/milestone_1_tests.h"
#include "test/milestone_2_tests.h"
#include "test/milestone_3_tests.h"
#include "test/milestone_4_tests.h"
#include "test/milestone_5_tests.h"
#include "test/utils_tests.h"

int main() {
  // Milestone 1
  TEST_THAT(load_and_sync_work_properly);
  TEST_THAT(we_parse_and_immediately_execute_create_operations);
  TEST_THAT(we_parse_and_execute_insert_operations);
  TEST_THAT(we_parse_and_execute_select_and_fetch_operations);
  TEST_THAT(we_parse_and_execute_print_operations);
  TEST_THAT(we_can_increase_table_row_capacity);

  // Milestone 2
  TEST_THAT(btree_works);
  TEST_THAT(btree_works_with_random_insertions);
  TEST_THAT(binary_search_works);
  TEST_THAT(index_helpers_work);
  TEST_THAT(group_quicksort_works);
  TEST_THAT(we_pick_the_right_scan_type);
  TEST_THAT(we_support_multiple_clustered_indexes);
  TEST_THAT(sorted_columns_work);
  TEST_THAT(btree_bulk_load_works);
  TEST_THAT(group_external_sort_works);

  // Milestone 3
  TEST_THAT(shared_scan_backend_works);

  // Milestone 4
  TEST_THAT(joins_work);
  TEST_THAT(join_parsing_works);

  // Milestone 5
  TEST_THAT(bitvectors_work);
  TEST_THAT(deletes_work);
  TEST_THAT(mixed_sorted_deletes_updates_and_inserts_work);

  RUN_TESTS();
}
