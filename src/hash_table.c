#include "hash_table.h"

Hashtable* newHashtable(void) {
  Hashtable* hash = malloc(sizeof(Hashtable));
  hash->n_vals = 0;
  hash->n_lists = LISTS_PER_HASH;
  hash->lists = malloc(LISTS_PER_HASH * sizeof(ValueList*));
  for (int i = 0; i < LISTS_PER_HASH; i++)
    hash->lists[i] = newValueList();
  return hash;
}

ValueList* newValueList(void) {
  ValueList* list = malloc(sizeof(ValueList));
  list->n_vals = 0;
  list->next = NULL;
  // don't really need to initialize the list values,
  // but Valgrind complains otherwise...
  for (int i = 0; i < VALS_PER_NODE; i++) {
    list->keys[i] = 0;
    list->vals[i] = 0;
  }
  return list;
}

void freeHashtable(Hashtable* hash) {
  for (int i = 0; i < hash->n_lists; i++)
    freeValueList(hash->lists[i]);
  free(hash->lists);
  free(hash);
}

void freeValueList(ValueList* list) {
  if (list->next != NULL)
    freeValueList(list->next);
  free(list);
}

void valueListPush(ValueList* node, keyType key, valType val) {
  if (node->n_vals < VALS_PER_NODE) {
    node->keys[node->n_vals] = key;
    node->vals[node->n_vals] = val;
    node->n_vals++;
  } else {
    if (node->next == NULL)
      node->next = newValueList();
    valueListPush(node->next, key, val);
  }
}

void hashtablePut(Hashtable* hash, keyType key, valType val) {
  ValueList* node = hash->lists[hashIndex(key, hash->n_lists)];
  valueListPush(node, key, val);
  hash->n_vals++;

  if (hash->n_vals > hash->n_lists * MAX_VALS_PER_LIST) {
    int old_n_lists = hash->n_lists;
    int new_n_lists = 2 * hash->n_lists;
    ValueList** old_lists = hash->lists;
    ValueList** new_lists = malloc(new_n_lists * sizeof(ValueList*));
    for (int i = 0; i < new_n_lists; i++)
      new_lists[i] = newValueList();

    hash->lists = new_lists;
    hash->n_lists = new_n_lists;
    hash->n_vals = 0;

    for (int i = 0; i < old_n_lists; i++) {
      ValueList* old_node = old_lists[i];
      while (old_node != NULL) {
        for (int j = 0; j < old_node->n_vals; j++) {
          hashtablePut(hash, old_node->keys[j], old_node->vals[j]);
        }
        old_node = old_node->next;
      }
      freeValueList(old_lists[i]);
    }
  }
}

int hashtableGet(Hashtable* hash, keyType key, valType *result, int n_to_copy) {
  int n_matches = 0;
  ValueList* node = hash->lists[hashIndex(key, hash->n_lists)];
  while (node != NULL) {
    for (int i = 0; i < node->n_vals; i++) {
      if (node->keys[i] == key) {
        if (n_matches < n_to_copy)
          result[n_matches] = node->vals[i];
        n_matches++;
      }
    }
    node = node->next;
  }
  return n_matches;
}

int hashIndex(keyType key, int n_lists) {
  if (key >= 0)
    return key % n_lists;
  else
    return -key % n_lists;
}

void hashtableErase(Hashtable* hash, keyType key) {
  // Because we have multiple keys/vals per node, we can't just erase
  // nodes with the matching key. We can remove individual values and
  // try to allow sparse nodes, but it's complicated.
  //
  // The simplest solution, though not the fastest, is to copy and reallocate.
  int index = hashIndex(key, hash->n_lists);

  ValueList* new_list = newValueList();
  ValueList* node = hash->lists[index];

  while (node != NULL) {
    for (int i = 0; i < node->n_vals; i++)
      if (node->keys[i] != key)
        valueListPush(new_list, node->keys[i], node->vals[i]);
    node = node->next;
  }

  freeValueList(hash->lists[index]);
  hash->lists[index] = new_list;
}

void printValueList(ValueList* list) {
  if (list == NULL) {
    printf("ø");
  } else {
    for (int i = 0; i < list->n_vals; i++)
      printf("(%d,%d) ", list->keys[i], list->vals[i]);
    printf("-> ");
    printValueList(list->next);
  }
}

void printHashtable(Hashtable* hash) {
  for (int i = 0; i < hash->n_lists; i++) {
    printf("%d: ", i);
    printValueList(hash->lists[i]);
    printf("\n");
  }
}
