#define _XOPEN_SOURCE
/**
 * client.c
 *  CS165 Fall 2015
 *
 * This file provides a basic unix socket implementation for a client
 * used in an interactive client-server database.
 * The client receives input from stdin and sends it to the server.
 * No pre-processing is done on the client-side.
 *
 * For more information on unix sockets, refer to:
 * http://beej.us/guide/bgipc/output/html/multipage/unixsock.html
 **/
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "common.h"
#include "message.h"
#include "utils.h"
#include "c_sv.h"

#define DEFAULT_STDIN_BUFFER_SIZE 1024

/**
 * connect_client()
 *
 * This sets up the connection on the client side using unix sockets.
 * Returns a valid client socket fd on success, else -1 on failure.
 *
 **/
int connect_client() {
    int client_socket;
    size_t len;
    struct sockaddr_un remote;

    if ((client_socket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        log_err("L%d: Failed to create socket.\n", __LINE__);
        return -1;
    }

    remote.sun_family = AF_UNIX;
    strncpy(remote.sun_path, SOCK_PATH, strlen(SOCK_PATH) + 1);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family) + 1;
    if (connect(client_socket, (struct sockaddr *)&remote, len) == -1) {
        perror("client connect failed: ");
        return -1;
    }

    return client_socket;
}

int main(void)
{
    int client_socket = connect_client();
    if (client_socket < 0) {
        exit(1);
    }

    message send_message;
    message recv_message;
    send_message.status = OK_DONE;

    // Always output an interactive marker at the start of each command if the
    // input is from stdin. Do not output if piped in from file or from other fd
    char* prefix = "";
    if (isatty(fileno(stdin))) {
        prefix = "db_client > ";
    }

    char *output_str = NULL;
    int len = 0;

    // Continuously loop and wait for input. At each iteration:
    // 1. output interactive marker
    // 2. read from stdin until eof.
    char read_buffer[DEFAULT_STDIN_BUFFER_SIZE];
    send_message.payload = read_buffer;

    while (printf("%s", prefix), output_str = fgets(read_buffer,
           DEFAULT_STDIN_BUFFER_SIZE, stdin), !feof(stdin)) {
        if (output_str == NULL) {
            log_err("fgets failed.\n");
            break;
        }

        int length = strlen(read_buffer);

        if (strncmp(read_buffer, "load(", 5) == 0) {
          // we have a load command to munge
          char filename[DEFAULT_STDIN_BUFFER_SIZE];
          strncpy(filename, read_buffer+6, length-9);
          filename[length-9] = '\0';

          CsvReader* csv = csv_reader(filename);
          int col_count = csv->n_columns;

          int* col_data[col_count];
          size_t max_rows = 10000;
          for (int i = 0; i < col_count; i++) {
            col_data[i] = malloc(max_rows * sizeof(int));
          }

          char value[64];
          while (read_csv_row(csv)) {
            for (int i = 0; i < col_count; i++) {
              read_csv_cell(value, csv->headers[i], csv);
              col_data[i][csv->n_rows_read-1] = atoi(value);
            }
            if ((size_t)csv->n_rows_read >= max_rows) {
              max_rows *= 2;
              for (int i = 0; i < col_count; i++) {
                int* bigger = malloc(max_rows * sizeof(int));
                memcpy(bigger, col_data[i], csv->n_rows_read*sizeof(int));
                free(col_data[i]);
                col_data[i] = bigger;
              }
            }
          }
          int row_count = csv->n_rows_read;

          char new_command[DEFAULT_STDIN_BUFFER_SIZE];
          int new_comm_len = 0;
          new_comm_len += sprintf(new_command, "load(%d,%d", row_count, col_count);
          for (int i = 0; i < col_count; i++)
            new_comm_len += sprintf(new_command + new_comm_len, ",%s", csv->headers[i]);
          new_comm_len += sprintf(new_command + new_comm_len, ")");
          new_command[new_comm_len] = '\0';
          strcpy(read_buffer, new_command);
          send_message.length = new_comm_len;
          send(client_socket, &(send_message), sizeof(message), 0);
          send(client_socket, send_message.payload, send_message.length, 0);

          long int payload_size = col_count * row_count * sizeof(int);
          int* csv_payload = malloc(payload_size);
          for (int i = 0; i < col_count; i++) {
            memcpy(csv_payload+i*row_count, col_data[i], row_count*sizeof(int));
            free(col_data[i]);
          }

          send(client_socket, csv_payload, payload_size, 0);
          free(csv_payload);

          recv(client_socket, &(recv_message), sizeof(message), 0);
          free_csv_reader(csv);

        } else if (length > 1) {
            send_message.length = length;
            // Send the message_header, which tells server payload size
            if (send(client_socket, &(send_message), sizeof(message), 0) == -1) {
                log_err("Failed to send message header.");
                exit(1);
            }

            // Send the payload (query) to server
            if (send(client_socket, send_message.payload, send_message.length, 0) == -1) {
                log_err("Failed to send query payload.");
                exit(1);
            }

            // Always wait for server response (even if it is just an OK message)
            if ((len = recv(client_socket, &(recv_message), sizeof(message), 0)) > 0) {
                if (recv_message.status == OK_WAIT_FOR_RESPONSE &&
                    (int) recv_message.length > 0) {
                    // Calculate number of bytes in response package
                    int num_bytes = (int) recv_message.length;
                    char payload[num_bytes + 1];

                    // Receive the payload and print it out
                    if ((len = recv(client_socket, payload, num_bytes, 0)) > 0) {
                        payload[num_bytes] = '\0';
                        printf("%s\n", payload);
                    }
                }
            }
            else {
                if (len < 0) {
                    log_err("Failed to receive message.");
                }
                else {
		            log_info("Server closed connection\n");
		        }
                exit(1);
            }
        }
    }
    close(client_socket);
    return 0;
}
