#include "db_operator.h"

void free_db_operator(DbOperator* dbo) {
  if (dbo == NULL) {
    // nada
  } else if (dbo->type == INSERT) {
    free(dbo->operator_fields.insert_operator.values);
    free(dbo);
  } else if (dbo->type == PRINT) {
    free(dbo->operator_fields.print_operator.results);
    free(dbo);
  } else {
    free(dbo);
  }
}

void execute_avg_operator(AvgOperator op) {
  int* values = generalized_column_values(op.value_column);
  size_t length = generalized_column_length(op.value_column);
  long int sum = 0;
  for (size_t i = 0; i < length; i++) sum += (long int)values[i];
  op.result->value_count = (size_t)1;
  op.result->values = malloc(sizeof(double));
  ((double*)op.result->values)[0] = (double)sum / length;
}

void execute_sum_operator(SumOperator op) {
  int* values = generalized_column_values(op.value_column);
  size_t length = generalized_column_length(op.value_column);
  long int sum = 0;
  for (size_t i = 0; i < length; i++) sum += (long int)values[i];
  op.result->value_count = (size_t)1;
  op.result->values = malloc(sizeof(long int));
  ((long int*)op.result->values)[0] = sum;
}

void execute_add_operator(AddOperator op) {
  int* v1s = generalized_column_values(op.value_column1);
  int* v2s = generalized_column_values(op.value_column2);
  size_t n = generalized_column_length(op.value_column2);
  op.result->value_count = n;
  op.result->values = malloc(n * sizeof(int)); // TODO: non-ints are possible?
  for (size_t i = 0; i < n; i++)
    ((int*)op.result->values)[i] = v1s[i] + v2s[i];
}

void execute_sub_operator(SubOperator op) {
  int* v1s = generalized_column_values(op.value_column1);
  int* v2s = generalized_column_values(op.value_column2);
  size_t n = generalized_column_length(op.value_column2);
  op.result->value_count = n;
  op.result->values = malloc(n * sizeof(int)); // TODO: non-ints are possible?
  for (size_t i = 0; i < n; i++)
    ((int*)op.result->values)[i] = v1s[i] - v2s[i];
}

void execute_max_operator(MaxOperator op) {
  int* values = generalized_column_values(op.value_column);
  size_t length = generalized_column_length(op.value_column);
  int max = values[0];
  for (size_t i = 1; i < length; i++)
    if (values[i] > max)
      max = values[i];
  op.result->value_count = (size_t)1;
  op.result->values = malloc(sizeof(int)); // TODO: non-ints are possible?
  ((int*)op.result->values)[0] = max;
}

void execute_min_operator(MinOperator op) {
  int* values = generalized_column_values(op.value_column);
  size_t length = generalized_column_length(op.value_column);
  int min = values[0];
  for (size_t i = 1; i < length; i++)
    if (values[i] < min)
      min = values[i];
  op.result->value_count = (size_t)1;
  op.result->values = malloc(sizeof(int)); // TODO: non-ints are possible?
  ((int*)op.result->values)[0] = min;
}

void execute_btree_scan(Column* col, Result* result, Comparator comp) {
  size_t value_count = 0;
  ComparatorType minc = comp.min_comparator_type;
  ComparatorType maxc = comp.max_comparator_type;
  if (minc == NO_COMPARISON && maxc == NO_COMPARISON) {
    log_err("we shouldn't be here!\n");
  } else if (minc == NO_COMPARISON && maxc == LESS_THAN) {
    value_count = btree_scan_less_than(col->btree, (int*)result->values, comp.max_value);
  } else if (minc == GREATER_THAN_OR_EQUAL && maxc == NO_COMPARISON) {
    value_count = btree_scan_at_least(col->btree, (int*)result->values, comp.min_value);
  } else if (minc == GREATER_THAN_OR_EQUAL && maxc == LESS_THAN) {
    value_count = btree_scan_between(col->btree, (int*)result->values, comp.min_value, comp.max_value);
  }
  result->used_btree_scan = true;
  result->value_count = value_count;
  // TODO: realloc?
}

Column* column_for_select(DbOperator* dbo) {
  if (dbo->type != SELECT) return NULL;
  SelectOperator sop = dbo->operator_fields.select_operator;
  if (sop.index_vector != NULL) return NULL;
  if (sop.value_column.column_type != COLUMN) return NULL;
  return sop.value_column.column_pointer.column;
}

void execute_sorted_scan(Column* col, Result* result, Comparator comp) {
  ComparatorType minc = comp.min_comparator_type;
  ComparatorType maxc = comp.max_comparator_type;
  size_t min_index = 0;
  size_t max_index = col->sorted_until - 1;
  if (minc == NO_COMPARISON && maxc == LESS_THAN) {
    max_index = last_index_less_than(comp.max_value, col->data, col->sorted_until);
  } else if (minc == GREATER_THAN_OR_EQUAL && maxc == NO_COMPARISON) {
    min_index = first_index_at_least(comp.min_value, col->data, col->sorted_until);
  } else if (minc == GREATER_THAN_OR_EQUAL && maxc == LESS_THAN) {
    min_index = first_index_at_least(comp.min_value, col->data, col->sorted_until);
    max_index = last_index_less_than(comp.max_value, col->data, col->sorted_until);
  }
  result->used_sorted_scan = true;
  result->value_count = max_index - min_index + 1;
  for (size_t i = 0; i < result->value_count; i++)
    ((int*)result->values)[i] = (int)(min_index + i);

  if (col->length > col->sorted_until) {
    result->value_count += vanilla_select(
        col->data + col->sorted_until,
        ((int*)result->values) + result->value_count,
        comp,
        col->length - col->sorted_until,
        col->sorted_until);
  }
}

void execute_array_scan(SelectOperator sop) {
  int* positions = NULL;
  int* values = generalized_column_values(sop.value_column);
  size_t max_value_count = generalized_column_length(sop.value_column);

  if (sop.index_vector != NULL) {
    positions = (int*)sop.index_vector->values;
    max_value_count = sop.index_vector->value_count;
  } else {
    positions = NULL;
  }

  sop.result->used_array_scan = true;
  execute_select(values, positions, sop.result, sop.comparator, max_value_count);
}

int should_use_btree(BTree* btree, Comparator comp) {
  return btree_selectivity(btree, comp) <= BTREE_MAX_SELECTIVITY;
}

void execute_select_operator(SelectOperator sop) {
  ComparatorType minc = sop.comparator.min_comparator_type;
  ComparatorType maxc = sop.comparator.max_comparator_type;
  if (sop.index_vector == NULL && sop.value_column.column_type == COLUMN &&
      !(minc == NO_COMPARISON && maxc == NO_COMPARISON)) {
    Column* col = sop.value_column.column_pointer.column;
    if (col->is_sorted) {
      log_info("selecting using sorted column\n");
      execute_sorted_scan(col, sop.result, sop.comparator);
    } else if (col->btree != NULL && should_use_btree(col->btree, sop.comparator)) {
      log_info("selecting using btree\n");
      execute_btree_scan(col, sop.result, sop.comparator);
    } else {
      log_info("selecting using array scan\n");
      execute_array_scan(sop);
    }
  } else {
    execute_array_scan(sop);
  }

  if (sop.pending_deletes != NULL) {
    int* positions = (int*)sop.result->values;
    size_t n_kept = 0;
    for (size_t i = 0; i < sop.result->value_count; i++) {
      positions[n_kept] = positions[i];
      n_kept += !bitvector_get(sop.pending_deletes, positions[n_kept]);
    }
    sop.result->value_count = n_kept;
    /*realloc(positions, n_kept * sizeof(int));*/
  }
}

void execute_join_operator(JoinOperator j) {
  if (j.pos1->value_count > j.pos2->value_count) {
    if (j.join_type == HASH) hash_join(j.val1, j.pos1, j.val2, j.pos2, j.res1, j.res2);
    else        block_nested_loop_join(j.val1, j.pos1, j.val2, j.pos2, j.res1, j.res2, 2048);
  } else {
    if (j.join_type == HASH) hash_join(j.val2, j.pos2, j.val1, j.pos1, j.res2, j.res1);
    else        block_nested_loop_join(j.val2, j.pos2, j.val1, j.pos1, j.res2, j.res1, 2048);
  }
}

void execute_fetch_operator(FetchOperator fop) {
  int* positions = (int*)fop.index_vector->values;
  int* values = generalized_column_values(fop.value_column);

  // TODO: error if the length of the value vector is less than our max position?
  size_t n = fop.index_vector->value_count;
  int* results = malloc(n * sizeof(int));

  for (size_t i = 0; i < n; i++)
    results[i] = values[positions[i]];

  fop.result->value_count = n;
  fop.result->values = results;
}

void execute_insert_operator(InsertOperator iop) {
  relational_insert(iop.table, iop.values);
}

void execute_update_operator(UpdateOperator op) {
  Result* r = op.index_vector;
  relational_update(op.table, op.column->name, (int*)r->values, r->value_count, op.new_value);
}

void execute_delete_operator(DeleteOperator op) {
  Result* r = op.index_vector;
  relational_delete(op.table, (int*)r->values, r->value_count);
}

void execute_print_operator(PrintOperator pop, DbOperationResult* dbres) {
  Result* result;

  size_t values_per_line = pop.result_count;
  size_t number_of_lines = pop.results[0]->value_count;

  int maxlen = 32 * values_per_line * number_of_lines;

  // TODO: update the string size if this won't be large enough
  char* msg = malloc(maxlen * sizeof(char));
  int len = 0;

  for (size_t i = 0; i < number_of_lines; i++) {
    if (i > 0)
      len += sprintf(msg + len, "\n");

    for (size_t j = 0; j < values_per_line; j++) {
      if (j > 0)
        len += sprintf(msg + len, ",");

      result = pop.results[j];
      switch (result->value_type) {
        case INT:
          len += sprintf(msg + len, "%d", ((int*)result->values)[i]);
          break;
        case LONG:
          len += sprintf(msg + len, "%ld", ((long int*)result->values)[i]);
          break;
        case FLOAT:
          len += sprintf(msg + len, "%.2f", ((double*)result->values)[i]);
          break;
      }
    }

    if (len > maxlen - 2)
      log_err("about to exceed max string length\n");
  }

  msg[len++] = '\0';
  msg = realloc(msg, len * sizeof(char)); // TODO: is this a good idea?

  dbres->message = msg;
  dbres->message_length = (size_t)len;
}

DbOperationResult execute_db_operator(DbOperator* dbo) {
  DbOperationResult dbres; // = malloc(sizeof(DbOperationResult));
  dbres.status = DB_OPERATION_OK;
  dbres.message_length = 0;
  dbres.message = NULL;

  if (dbo == NULL) {
    // nada
  } else if (dbo->type == CREATE) {
    // nada
  } else if (dbo->type == INSERT) {
    execute_insert_operator(dbo->operator_fields.insert_operator);
  } else if (dbo->type == UPDATE) {
    execute_update_operator(dbo->operator_fields.update_operator);
  } else if (dbo->type == DELETE) {
    execute_delete_operator(dbo->operator_fields.delete_operator);
  } else if (dbo->type == SELECT) {
    execute_select_operator(dbo->operator_fields.select_operator);
  } else if (dbo->type == FETCH) {
    execute_fetch_operator(dbo->operator_fields.fetch_operator);
  } else if (dbo->type == PRINT) {
    execute_print_operator(dbo->operator_fields.print_operator, &dbres);
  } else if (dbo->type == AVG) {
    execute_avg_operator(dbo->operator_fields.avg_operator);
  } else if (dbo->type == SUM) {
    execute_sum_operator(dbo->operator_fields.sum_operator);
  } else if (dbo->type == ADD) {
    execute_add_operator(dbo->operator_fields.add_operator);
  } else if (dbo->type == SUB) {
    execute_sub_operator(dbo->operator_fields.sub_operator);
  } else if (dbo->type == MAX) {
    execute_max_operator(dbo->operator_fields.max_operator);
  } else if (dbo->type == MIN) {
    execute_min_operator(dbo->operator_fields.min_operator);
  } else if (dbo->type == JOIN) {
    execute_join_operator(dbo->operator_fields.join_operator);
  }

  free_db_operator(dbo);

  return dbres;
}
