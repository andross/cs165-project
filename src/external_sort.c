#include "external_sort.h"

void group_external_sort(int** arrays, size_t length, size_t width, size_t leader, size_t runsize) {
  size_t val_index = 0;
  size_t run_index = 0;
  size_t n_runs = (length+runsize-1)/runsize;
  ExternalSortRun** runs = malloc(n_runs * sizeof(ExternalSortRun*));

  // quicksort each run and register it
  while (val_index < length) {
    size_t next_index = MIN(val_index+runsize, length);
    size_t run_length = next_index - val_index;
    group_quicksort_chunk(arrays[leader], arrays, width, val_index, next_index-1);
    ExternalSortRun* run = init_external_sort_run(run_length, width, leader);
    for (size_t a = 0; a < width; a++)
      memcpy(run->arrays[a], arrays[a]+val_index, run_length * sizeof(int));
    runs[run_index++] = run;
    val_index = next_index;
  }

  // continually squash down pairs of runs
  while (n_runs > 1) {
    ExternalSortRun** new_runs = malloc((n_runs+1/2) * sizeof(ExternalSortRun*));
    size_t i;
    for (i = 0; i < (n_runs+1)/2; i++) {
      if (2*i+1 < n_runs)
        new_runs[i] = merge_external_sort_runs(runs[2*i], runs[2*i+1]);
      else
        new_runs[i] = runs[2*i];
    }
    free(runs);
    runs = new_runs;
    n_runs = i;
  }

  // copy back to original
  for (size_t a = 0; a < width; a++)
    memcpy(arrays[a], runs[0]->arrays[a], length * sizeof(int));

  // free everything else we used
  free_external_sort_run(runs[0]);
  free(runs);
}

ExternalSortRun* init_external_sort_run(size_t length, size_t width, size_t leader) {
  ExternalSortRun* run = malloc(sizeof(ExternalSortRun));
  run->leader = leader;
  run->length = length;
  run->width = width;
  run->arrays = malloc(width * sizeof(int*));
  for (size_t a = 0; a < width; a++)
    run->arrays[a] = malloc(length * sizeof(int));
  return run;
}

ExternalSortRun* merge_external_sort_runs(ExternalSortRun* run1, ExternalSortRun* run2) {
  // setup
  size_t length = run1->length + run2->length;
  size_t width = run1->width;
  size_t leader = run1->leader;
  ExternalSortRun* result = init_external_sort_run(length, width, leader);

  // merge
  size_t i = 0;
  size_t j = 0;
  size_t k = 0;
  while (i < run1->length && j < run2->length) {
    if (run1->arrays[leader][i] < run2->arrays[leader][j]) {
      for (size_t a = 0; a < width; a++) result->arrays[a][k] = run1->arrays[a][i];
      i++;
    } else {
      for (size_t a = 0; a < width; a++) result->arrays[a][k] = run2->arrays[a][j];
      j++;
    }
    k++;
  }
  while (i < run1->length) {
    for (size_t a = 0; a < width; a++) result->arrays[a][k] = run1->arrays[a][i];
    i++;
    k++;
  }
  while (j < run2->length) {
    for (size_t a = 0; a < width; a++) result->arrays[a][k] = run2->arrays[a][j];
    j++;
    k++;
  }

  // free the old runs
  free_external_sort_run(run1);
  free_external_sort_run(run2);

  // return the new run
  return result;
}

void free_external_sort_run(ExternalSortRun* run) {
  for (size_t a = 0; a < run->width; a++)
    free(run->arrays[a]);
  free(run->arrays);
  free(run);
}
