void load_and_sync_work_properly() {
  // Setup. TODO: setup everything here, rather than relying on existing files
  FILE* df1 = fopen("testdata/deebee/taybull_one/callem_one/data", "w");
  int c1_data[2] = { 5, 3 };
  fwrite(c1_data, sizeof(int), 2, df1);
  fclose(df1);

  FILE* df2 = fopen("testdata/deebee/taybull_one/callem_two/data", "w");
  int c2_data[2] = { 6, 4 };
  fwrite(c2_data, sizeof(int), 2, df2);
  fclose(df2);

  system("rm -rf testdata2");

  // Read data from directories
  ColumnStore* store = load_column_store("testdata");
  ASSERT_EQUAL_INTS(store->db_count, 1);
  ASSERT_EQUAL_INTS(store->db_capacity, DEFAULT_DB_CAPACITY);

  Db* db = store->dbs[0];
  ASSERT_EQUAL_STRS(db->name, "deebee");
  ASSERT_EQUAL_INTS(db->table_capacity, 100);
  ASSERT_EQUAL_INTS(db->table_count, 2);

  Table* tbl1 = db->tables[0];
  ASSERT_EQUAL_STRS(tbl1->name, "taybull_one");
  ASSERT_EQUAL_INTS(tbl1->column_capacity, 8);
  ASSERT_EQUAL_INTS(tbl1->column_count, 2);
  ASSERT_EQUAL_INTS(tbl1->row_count, 2);

  Table* tbl2 = db->tables[1];
  ASSERT_EQUAL_STRS(tbl2->name, "taybull_two");
  ASSERT_EQUAL_INTS(tbl2->column_capacity, 4);
  ASSERT_EQUAL_INTS(tbl2->column_count, 0);
  ASSERT_EQUAL_INTS(tbl2->row_count, 0);

  Column* col1 = tbl1->columns[0];
  ASSERT_EQUAL_STRS(col1->name, "callem_one");
  ASSERT_EQUAL_INTS(col1->data[0], 5);
  ASSERT_EQUAL_INTS(col1->data[1], 3);

  Column* col2 = tbl1->columns[1];
  ASSERT_EQUAL_STRS(col2->name, "callem_two");
  ASSERT_EQUAL_INTS(col2->data[0], 6);
  ASSERT_EQUAL_INTS(col2->data[1], 4);

  // Assert some basic finder methods work
  ASSERT_EQUAL_PTRS(db, find_db(store, "deebee"));
  ASSERT_EQUAL_PTRS(NULL, find_db(store, "foo"));

  ASSERT_EQUAL_PTRS(tbl1, find_table(db, "taybull_one"));
  ASSERT_EQUAL_PTRS(NULL, find_table(db, "taybull_foo"));

  ASSERT_EQUAL_PTRS(col2, find_column(tbl1, "callem_two"));
  ASSERT_EQUAL_PTRS(NULL, find_column(tbl1, "callem_foo"));

  // Update the data in memory
  Db* db2 = create_db(store, "deebee2");
  Table* tbl3 = create_table(db, "taybull_three", (size_t)2);
  Column* col3 = create_column(tbl3, "callem_three");
  Column* col4 = create_column(tbl3, "callem_four");
  ASSERT_EQUAL_INTS(db2->table_capacity, 100);
  ASSERT_EQUAL_INTS(db2->table_count, 0);
  ASSERT_EQUAL_INTS(tbl3->column_capacity, 2);
  ASSERT_EQUAL_INTS(tbl3->column_count, 2);
  ASSERT_EQUAL_INTS(tbl3->row_count, 0);

  int tbl1_row3[] = { 4, 7 };
  relational_insert(tbl1, tbl1_row3);
  int tbl3_row1[] = { 0, 1 };
  int tbl3_row2[] = { 10, 2345 };
  relational_insert(tbl3, tbl3_row1);
  relational_insert(tbl3, tbl3_row2);

  // Assert in-memory variables are updated
  ASSERT_EQUAL_INTS(tbl1->row_count, 3);
  ASSERT_EQUAL_INTS(col1->data[0], 5);
  ASSERT_EQUAL_INTS(col1->data[1], 3);
  ASSERT_EQUAL_INTS(col1->data[2], 4);
  ASSERT_EQUAL_INTS(col2->data[0], 6);
  ASSERT_EQUAL_INTS(col2->data[1], 4);
  ASSERT_EQUAL_INTS(col2->data[2], 7);

  ASSERT_EQUAL_INTS(tbl3->row_count, 2);
  ASSERT_EQUAL_INTS(col3->data[0], 0);
  ASSERT_EQUAL_INTS(col3->data[1], 10);
  ASSERT_EQUAL_INTS(col4->data[0], 1);
  ASSERT_EQUAL_INTS(col4->data[1], 2345);

  // create a clustered index
  ASSERT(!tbl1->is_sorted);
  ASSERT(!col1->is_sorted);
  ASSERT(!col2->is_sorted);
  create_primary_index(tbl1, "callem_two");
  ASSERT_EQUAL_INTS(tbl1->replica_count, 0);
  ASSERT_EQUAL_INTS(tbl1->leader, 1);
  ASSERT(tbl1->is_sorted);
  ASSERT(!col1->is_sorted);
  ASSERT(col2->is_sorted);

  // create a btree index
  ASSERT_EQUAL_PTRS(col1->btree, NULL);
  create_secondary_index(tbl1, "callem_one");
  ASSERT_DIFFN_PTRS(col1->btree, NULL);
  ASSERT_EQUAL_INTS(col1->btree->size, 3);
  ASSERT_EQUAL_INTS(col1->btree->depth, 1);

  // create another clustered index; this should create a replica
  create_primary_index(tbl1, "callem_one");
  ASSERT_EQUAL_INTS(tbl1->replica_count, 1);
  ASSERT_EQUAL_INTS(tbl1->replicas[0]->leader, 0);
  ASSERT(tbl1->replicas[0]->is_sorted);
  ASSERT(tbl1->replicas[0]->columns[0]->is_sorted);
  ASSERT(!tbl1->replicas[0]->columns[1]->is_sorted);

  // 5 3 4
  // 6 4 7
  // 
  //  => 4 6 7
  //     3 5 4
  //
  //  => 3 4 5
  //     4 7 6
  ASSERT_EQUAL_INTS(col1->data[0], 3);
  ASSERT_EQUAL_INTS(col1->data[1], 5);
  ASSERT_EQUAL_INTS(col1->data[2], 4);

  ASSERT_EQUAL_INTS(col2->data[0], 4);
  ASSERT_EQUAL_INTS(col2->data[1], 6);
  ASSERT_EQUAL_INTS(col2->data[2], 7);

  ASSERT_EQUAL_INTS(tbl1->replicas[0]->columns[0]->data[0], 3);
  ASSERT_EQUAL_INTS(tbl1->replicas[0]->columns[0]->data[1], 4);
  ASSERT_EQUAL_INTS(tbl1->replicas[0]->columns[0]->data[2], 5);

  ASSERT_EQUAL_INTS(tbl1->replicas[0]->columns[1]->data[0], 4);
  ASSERT_EQUAL_INTS(tbl1->replicas[0]->columns[1]->data[1], 7);
  ASSERT_EQUAL_INTS(tbl1->replicas[0]->columns[1]->data[2], 6);

  // Write data back to a different directory, assert changes get persisted
  strcpy(store->data_dir, "testdata2"); // change directory!
  sync_column_store(store);

  FILE* f1 = fopen("testdata2/dbs.csv", "r");
  char db_l0[1024]; fgets(db_l0, 1024, f1); ASSERT_EQUAL_STRS(db_l0, "db_name,table_capacity\n");
  char db_l1[1024]; fgets(db_l1, 1024, f1); ASSERT_EQUAL_STRS(db_l1, "deebee,100\n");
  char db_l2[1024]; fgets(db_l2, 1024, f1); ASSERT_EQUAL_STRS(db_l2, "deebee2,100\n");
  fclose(f1);

  FILE* f2 = fopen("testdata2/deebee/tables.csv", "r");
  char tbl_l0[1024]; fgets(tbl_l0, 1024, f2); ASSERT_EQUAL_STRS(tbl_l0, "table_name,column_capacity,row_count,row_capacity,replica_count\n");
  char tbl_l1[1024]; fgets(tbl_l1, 1024, f2); ASSERT_EQUAL_STRS(tbl_l1, "taybull_one,8,3,1000,1\n");
  char tbl_l2[1024]; fgets(tbl_l2, 1024, f2); ASSERT_EQUAL_STRS(tbl_l2, "taybull_two,4,0,1000,0\n");
  fclose(f2);

  FILE* f3 = fopen("testdata2/deebee/taybull_one/columns.csv", "r");
  char col_l0[1024]; fgets(col_l0, 1024, f3); ASSERT_EQUAL_STRS(col_l0, "column_name,is_sorted,has_btree\n");
  char col_l1[1024]; fgets(col_l1, 1024, f3); ASSERT_EQUAL_STRS(col_l1, "callem_one,0,1\n");
  char col_l2[1024]; fgets(col_l2, 1024, f3); ASSERT_EQUAL_STRS(col_l2, "callem_two,1,0\n");
  fclose(f3);

  FILE* f35 = fopen("testdata2/deebee/taybull_one/replicas/replica0/columns.csv", "r");
  char rcol_l0[1024]; fgets(rcol_l0, 1024, f35); ASSERT_EQUAL_STRS(rcol_l0, "column_name,is_sorted,has_btree\n");
  char rcol_l1[1024]; fgets(rcol_l1, 1024, f35); ASSERT_EQUAL_STRS(rcol_l1, "callem_one,1,0\n");
  char rcol_l2[1024]; fgets(rcol_l2, 1024, f35); ASSERT_EQUAL_STRS(rcol_l2, "callem_two,0,0\n");
  fclose(f35);

  FILE* f41 = fopen("testdata2/deebee/taybull_one/callem_one/data", "r");
  int written_c1_data[3]; fread(written_c1_data, sizeof(int), 3, f41);
  ASSERT_EQUAL_INTS(3, written_c1_data[0]);
  ASSERT_EQUAL_INTS(5, written_c1_data[1]);
  ASSERT_EQUAL_INTS(4, written_c1_data[2]);
  fclose(f41);

  FILE* f42 = fopen("testdata2/deebee/taybull_one/callem_two/data", "r");
  int written_c2_data[3]; fread(written_c2_data, sizeof(int), 3, f42);
  ASSERT_EQUAL_INTS(4, written_c2_data[0]);
  ASSERT_EQUAL_INTS(6, written_c2_data[1]);
  ASSERT_EQUAL_INTS(7, written_c2_data[2]);
  fclose(f42);

  FILE* f43 = fopen("testdata2/deebee/taybull_one/replicas/replica0/callem_one/data", "r");
  int written_c1_data2[3]; fread(written_c1_data2, sizeof(int), 3, f43);
  ASSERT_EQUAL_INTS(3, written_c1_data2[0]);
  ASSERT_EQUAL_INTS(4, written_c1_data2[1]);
  ASSERT_EQUAL_INTS(5, written_c1_data2[2]);
  fclose(f43);

  FILE* f44 = fopen("testdata2/deebee/taybull_one/replicas/replica0/callem_two/data", "r");
  int written_c2_data2[3]; fread(written_c2_data2, sizeof(int), 3, f44);
  ASSERT_EQUAL_INTS(4, written_c2_data2[0]);
  ASSERT_EQUAL_INTS(7, written_c2_data2[1]);
  ASSERT_EQUAL_INTS(6, written_c2_data2[2]);
  fclose(f44);

  FILE* f5 = fopen("testdata2/deebee/taybull_three/callem_four/data", "r");
  int written_c4_data[2]; fread(written_c4_data, sizeof(int), 2, f5);
  ASSERT_EQUAL_INTS(1, written_c4_data[0]);
  ASSERT_EQUAL_INTS(2345, written_c4_data[1]);
  fclose(f5);

  BTree* btree = btree_load("testdata2/deebee/taybull_one/callem_one/index");
  ASSERT_EQUAL_INTS(btree->size, 3);
  ASSERT_EQUAL_INTS(btree->depth, 1);

  btree_free(btree);
  free_column_store(store);
}

void we_can_increase_table_row_capacity() {
  ColumnStore* store = load_column_store("testdata");
  Db* db = create_db(store, "db");
  Table* table = create_table(db, "table", (size_t)2);
  Column* column1 = create_column(table, "column1");
  Column* column2 = create_column(table, "column2");
  int values[2];

  ASSERT_EQUAL_INTS(table->row_capacity, DEFAULT_ROW_CAPACITY);
  ASSERT_EQUAL_INTS(table->row_count, 0);
  for (int i = 0; i < DEFAULT_ROW_CAPACITY; i++) {
    values[0] = i;
    values[1] = i+1;
    relational_insert(table, values);
  }

  ASSERT_EQUAL_INTS(table->row_capacity, DEFAULT_ROW_CAPACITY);
  ASSERT_EQUAL_INTS(table->row_count, DEFAULT_ROW_CAPACITY);

  values[0] = DEFAULT_ROW_CAPACITY;
  values[1] = DEFAULT_ROW_CAPACITY+1;
  relational_insert(table, values);

  ASSERT_EQUAL_INTS(table->row_capacity, DEFAULT_ROW_CAPACITY * 2);
  ASSERT_EQUAL_INTS(table->row_count, DEFAULT_ROW_CAPACITY + 1);

  ASSERT_EQUAL_INTS(column1->data[DEFAULT_ROW_CAPACITY-1], DEFAULT_ROW_CAPACITY-1);
  ASSERT_EQUAL_INTS(column1->data[DEFAULT_ROW_CAPACITY], DEFAULT_ROW_CAPACITY);
  ASSERT_EQUAL_INTS(column2->data[DEFAULT_ROW_CAPACITY-1], DEFAULT_ROW_CAPACITY);
  ASSERT_EQUAL_INTS(column2->data[DEFAULT_ROW_CAPACITY], DEFAULT_ROW_CAPACITY+1);

  free_column_store(store);
}

void we_parse_and_immediately_execute_create_operations() {
  ColumnStore* store = load_column_store("testdata");
  struct message msg;

  // we should be able to create a database
  ASSERT_EQUAL_PTRS(NULL, find_db(store, "db1"));

  parse_create("(db,\"db1\")", store, &msg);
  ASSERT(msg.status == OK_DONE);

  Db* db = find_db(store, "db1");
  ASSERT_DIFFN_PTRS(NULL, db);

  // creating the same database again should throw an error
  parse_create("(db,\"db1\")", store, &msg);
  ASSERT(msg.status == OBJECT_ALREADY_EXISTS);

  // we should be able to create a table
  ASSERT_EQUAL_PTRS(NULL, find_table(db, "tbl1"));
  ASSERT_EQUAL_INTS(db->table_count, 0);

  parse_create("(tbl,\"tbl1\",db1,1)", store, &msg);
  ASSERT(msg.status == OK_DONE);

  Table* table = find_table(db, "tbl1");
  ASSERT_DIFFN_PTRS(NULL, table);
  ASSERT_EQUAL_INTS(table->column_capacity, 1);
  ASSERT_EQUAL_INTS(db->table_count, 1);

  // we should not be able to create a table for an unknown db
  parse_create("(tbl,\"tbl1\",db2,1)", store, &msg);
  ASSERT(msg.status == OBJECT_NOT_FOUND);

  // or double-create a table within the same db
  parse_create("(tbl,\"tbl1\",db1,1)", store, &msg);
  ASSERT(msg.status == OBJECT_ALREADY_EXISTS);

  // we should be able to create a column
  ASSERT_EQUAL_INTS(table->column_count, 0);
  ASSERT_EQUAL_PTRS(NULL, find_column(table, "col1"));

  parse_create("(col,\"col1\",db1.tbl1)", store, &msg);
  ASSERT(msg.status == OK_DONE);

  Column* column = find_column(table, "col1");
  ASSERT_DIFFN_PTRS(NULL, column);
  ASSERT_EQUAL_INTS(table->column_count, 1);

  // same error checking as before should apply
  parse_create("(col,\"col1\",db2.tbl1)", store, &msg);
  ASSERT(msg.status == OBJECT_NOT_FOUND);

  parse_create("(col,\"col1\",db1.tbl2)", store, &msg);
  ASSERT(msg.status == OBJECT_NOT_FOUND);

  parse_create("(col,\"col1\",db1.tbl1)", store, &msg);
  ASSERT(msg.status == OBJECT_ALREADY_EXISTS);

  free_column_store(store);
}

void we_parse_and_execute_insert_operations() {
  ColumnStore* store = load_column_store("testdata");

  // first let's check some error cases
  struct message msg;

  ASSERT_EQUAL_PTRS(NULL, parse_insert("yo", store, &msg));
  ASSERT(msg.status == UNKNOWN_COMMAND);

  ASSERT_EQUAL_PTRS(NULL, parse_insert("(yoohoo)", store, &msg));
  ASSERT(msg.status == INCORRECT_FORMAT);

  ASSERT_EQUAL_PTRS(NULL, parse_insert("(deebee.taybull_one.foo,1)", store, &msg));
  ASSERT(msg.status == INCORRECT_FORMAT);

  ASSERT_EQUAL_PTRS(NULL, parse_insert("(deebee2.taybull_one,1)", store, &msg));
  ASSERT(msg.status == OBJECT_NOT_FOUND);

  ASSERT_EQUAL_PTRS(NULL, parse_insert("(deebee.taybull_three,1)", store, &msg));
  ASSERT(msg.status == OBJECT_NOT_FOUND);

  ASSERT_EQUAL_PTRS(NULL, parse_insert("(deebee.taybull_one,100)", store, &msg));
  ASSERT(msg.status == INCORRECT_FORMAT);

  DbOperator* dbo = parse_insert("(deebee.taybull_one,100,2400)", store, &msg);
  ASSERT_DIFFN_PTRS(NULL, dbo);
  ASSERT(msg.status == OK_DONE);

  Db* db = find_db(store, "deebee");
  Table* table = find_table(db, "taybull_one");

  ASSERT_EQUAL_INTS(100, dbo->operator_fields.insert_operator.values[0]);
  ASSERT_EQUAL_INTS(2400, dbo->operator_fields.insert_operator.values[1]);
  ASSERT_EQUAL_PTRS(table, dbo->operator_fields.insert_operator.table);

  ASSERT_EQUAL_INTS(table->row_count, 2);

  execute_db_operator(dbo);

  ASSERT_EQUAL_INTS(table->row_count, 3);
  ASSERT_EQUAL_INTS(table->columns[0]->data[2], 100);
  ASSERT_EQUAL_INTS(table->columns[1]->data[2], 2400);

  free_column_store(store);
}

void we_parse_and_execute_select_and_fetch_operations() {
  // setup our db
  ColumnStore* store = load_column_store("testdata");

  // initialize a client context and message
  ClientContext* context = init_client_context();
  struct message msg;

  // for isolation, let's set up the data we need here
  Db* db = create_db(store, "foo");
  Table* table = create_table(db, "bar", 2);
  Column* col_a = create_column(table, "a");
  Column* col_b = create_column(table, "b");

  int row1[2] = { 0, 0 };
  int row2[2] = { 1, -1 };
  int row3[2] = { 2, -2 };
  int row4[2] = { 3, -3 };
  int row5[2] = { 4, -4 };
  relational_insert(table, row1);
  relational_insert(table, row2);
  relational_insert(table, row3);
  relational_insert(table, row4);
  relational_insert(table, row5);

  // -- query1 = select values from column A with some constraints, return qualifying positions
  // -- query2 = fetch values from entire column B at positions from query1
  // -- query3 = select values from values from query2, return qualifying positions

  // parse query1
  char* query1 = "indexes=select(foo.bar.a,1,4)";
  DbOperator* select1 = parse_command(query1, store, &msg, 0, context);

  ASSERT_DIFFN_PTRS(select1, NULL);
  ASSERT(select1->type == SELECT);

  SelectOperator s = select1->operator_fields.select_operator;

  ASSERT(s.comparator.min_comparator_type == GREATER_THAN_OR_EQUAL);
  ASSERT(s.comparator.max_comparator_type == LESS_THAN);
  ASSERT_EQUAL_INTS(s.comparator.min_value, 1);
  ASSERT_EQUAL_INTS(s.comparator.max_value, 4);

  ASSERT(s.value_column.column_type == COLUMN);
  ASSERT_EQUAL_PTRS(col_a, s.value_column.column_pointer.column);
  ASSERT_EQUAL_PTRS(NULL, s.index_vector);

  ASSERT_EQUAL_INTS(1, context->variable_count);
  ASSERT_EQUAL_STRS("indexes", context->variables[0]->name);
  Result* indexes = context->variables[0]->value;
  ASSERT(indexes->value_type == INT);
  ASSERT_EQUAL_INTS(0, indexes->value_count);

  // execute and check result
  DbOperationResult r1 = execute_db_operator(select1);

  ASSERT(r1.status == DB_OPERATION_OK);
  ASSERT_EQUAL_INTS(indexes->value_count, 3);
  ASSERT_EQUAL_INTS(((int*)indexes->values)[0], 1);
  ASSERT_EQUAL_INTS(((int*)indexes->values)[1], 2);
  ASSERT_EQUAL_INTS(((int*)indexes->values)[2], 3);

  // parse query2
  char* query2 = "bvalues=fetch(foo.bar.b,indexes)";
  DbOperator* fetch2 = parse_command(query2, store, &msg, 0, context);

  ASSERT_DIFFN_PTRS(fetch2, NULL);
  ASSERT(fetch2->type == FETCH);

  FetchOperator f = fetch2->operator_fields.fetch_operator;

  ASSERT(f.value_column.column_type == COLUMN);
  ASSERT_EQUAL_PTRS(col_b, f.value_column.column_pointer.column);
  ASSERT_EQUAL_PTRS(indexes, f.index_vector);

  ASSERT_EQUAL_INTS(2, context->variable_count);
  ASSERT_EQUAL_STRS("bvalues", context->variables[1]->name);
  Result* bvalues = context->variables[1]->value;
  ASSERT(bvalues->value_type == INT);
  ASSERT_EQUAL_INTS(0, bvalues->value_count);
  ASSERT_EQUAL_PTRS(NULL, bvalues->values);

  // execute and check result
  DbOperationResult r2 = execute_db_operator(fetch2);

  ASSERT(r2.status == DB_OPERATION_OK);
  ASSERT_EQUAL_INTS(bvalues->value_count, 3);
  ASSERT_EQUAL_INTS(((int*)bvalues->values)[0], -1);
  ASSERT_EQUAL_INTS(((int*)bvalues->values)[1], -2);
  ASSERT_EQUAL_INTS(((int*)bvalues->values)[2], -3);

  // parse query3
  char* query3 = "indexes2=select(indexes,bvalues,-2,null)";
  DbOperator* select3 = parse_command(query3, store, &msg, 0, context);

  ASSERT_DIFFN_PTRS(select3, NULL);
  ASSERT(select3->type == SELECT);

  SelectOperator s3 = select3->operator_fields.select_operator;

  ASSERT(s3.comparator.min_comparator_type == GREATER_THAN_OR_EQUAL);
  ASSERT(s3.comparator.max_comparator_type == NO_COMPARISON);
  ASSERT_EQUAL_INTS(s3.comparator.min_value, -2);
  ASSERT_EQUAL_INTS(s3.comparator.max_value, 0); // arbitrary

  ASSERT(s3.value_column.column_type == RESULT);
  ASSERT_EQUAL_PTRS(bvalues, s3.value_column.column_pointer.result);
  ASSERT_EQUAL_PTRS(indexes, s3.index_vector);

  ASSERT_EQUAL_INTS(3, context->variable_count);
  ASSERT_EQUAL_STRS("indexes2", context->variables[2]->name);
  Result* indexes2 = context->variables[2]->value;
  ASSERT(indexes2->value_type == INT);
  ASSERT_EQUAL_INTS(0, indexes2->value_count);

  // execute and check result
  DbOperationResult r3 = execute_db_operator(select3);

  ASSERT(r3.status == DB_OPERATION_OK);
  ASSERT_EQUAL_INTS(indexes2->value_count, 2);
  ASSERT_EQUAL_INTS(((int*)indexes2->values)[0], 1);
  ASSERT_EQUAL_INTS(((int*)indexes2->values)[1], 2);

  free_client_context(context);
  free_column_store(store);
}

void we_parse_and_execute_print_operations() {
  ColumnStore* store = load_column_store("testdata");
  ClientContext* context = init_client_context();
  struct message msg;

  execute_db_operator(parse_command(
      "ps=select(deebee.taybull_one.callem_one,null,null)", store, &msg, 0, context));
  execute_db_operator(parse_command(
      "c1=fetch(deebee.taybull_one.callem_one,ps)", store, &msg, 0, context));
  execute_db_operator(parse_command(
      "c2=fetch(deebee.taybull_one.callem_two,ps)", store, &msg, 0, context));

  DbOperator* print = parse_command(
      "print(c1,c2)", store, &msg, 0, context);

  ASSERT(print->type == PRINT);

  PrintOperator p = print->operator_fields.print_operator;

  ASSERT_EQUAL_INTS(p.result_count, 2);
  ASSERT_EQUAL_PTRS(p.results[0], get_client_variable(context, "c1"));
  ASSERT_EQUAL_PTRS(p.results[1], get_client_variable(context, "c2"));

  DbOperationResult dbres = execute_db_operator(print);

  ASSERT(dbres.status == DB_OPERATION_OK);
  ASSERT_EQUAL_STRS(dbres.message, "5,6\n3,4");
  ASSERT_EQUAL_INTS(dbres.message_length, 8);
  ASSERT_EQUAL_INTS(strlen(dbres.message), 7);

  execute_db_operator(parse_command(
      "add=add(c1,c2)", store, &msg, 0, context));
  execute_db_operator(parse_command(
      "sub=sub(c2,c1)", store, &msg, 0, context));
  DbOperationResult dbres2 = execute_db_operator(parse_command(
      "print(add,sub)", store, &msg, 0, context));

  ASSERT(dbres2.status == DB_OPERATION_OK);
  ASSERT_EQUAL_STRS(dbres2.message, "11,1\n7,1");
  ASSERT_EQUAL_INTS(dbres2.message_length, 9);
  ASSERT_EQUAL_INTS(strlen(dbres2.message), 8);

  execute_db_operator(parse_command(
      "sum=sum(deebee.taybull_one.callem_two)", store, &msg, 0, context));
  execute_db_operator(parse_command(
      "avg=avg(deebee.taybull_one.callem_two)", store, &msg, 0, context));
  DbOperationResult dbres3 = execute_db_operator(parse_command(
      "print(sum,avg)", store, &msg, 0, context));

  ASSERT(dbres3.status == DB_OPERATION_OK);
  ASSERT_EQUAL_STRS(dbres3.message, "10,5.00");
  ASSERT_EQUAL_INTS(dbres3.message_length, 8);
  ASSERT_EQUAL_INTS(strlen(dbres3.message), 7);

  execute_db_operator(parse_command(
      "min=min(deebee.taybull_one.callem_two)", store, &msg, 0, context));
  execute_db_operator(parse_command(
      "max=max(deebee.taybull_one.callem_two)", store, &msg, 0, context));
  DbOperationResult dbres4 = execute_db_operator(parse_command(
      "print(min,max)", store, &msg, 0, context));

  ASSERT(dbres4.status == DB_OPERATION_OK);
  ASSERT_EQUAL_STRS(dbres4.message, "4,6");

  free(dbres.message);
  free(dbres2.message);
  free(dbres3.message);
  free(dbres4.message);
  free_client_context(context);
  free_column_store(store);
}
