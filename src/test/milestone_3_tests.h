void shared_scan_backend_works() {
  ColumnStore* store = load_column_store("testdata");
  Db* db = create_db(store, "db");
  Table* table = create_table(db, "table", (size_t)1);
  Column* column = create_column(table, "column");
  ClientContext* context = init_client_context();
  struct message msg;

  for (int i = 0; i < 80; i++) {
    int data[1] = { i };
    relational_insert(table, data);
  }

  SharedScan* ss = init_shared_scan(column, 20);
  DbOperator* s1 = parse_command("s1=select(db.table.column,null,40)", store, &msg, 0, context);
  DbOperator* s2 = parse_command("s2=select(db.table.column,40,null)", store, &msg, 0, context);
  DbOperator* s3 = parse_command("s3=select(db.table.column,20,60)", store, &msg, 0, context);
  add_shared_scanner(ss, s1);
  add_shared_scanner(ss, s2);
  add_shared_scanner(ss, s3);
  do_shared_scan(ss);

  Result* r1 = get_client_variable(context, "s1");
  Result* r2 = get_client_variable(context, "s2");
  Result* r3 = get_client_variable(context, "s3");

  ASSERT_EQUAL_INTS(r1->value_count, 40);
  ASSERT_EQUAL_INTS(r2->value_count, 40);
  ASSERT_EQUAL_INTS(r3->value_count, 40);

  for (size_t i = 0; i < 40; i++) {
    ASSERT_EQUAL_INTS(((int*)r1->values)[i], i);
    ASSERT_EQUAL_INTS(((int*)r2->values)[i], i+40);
    ASSERT_EQUAL_INTS(((int*)r3->values)[i], i+20);
  }

  free_client_context(context);
  free_column_store(store);
}
