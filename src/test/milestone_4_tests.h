Result* setup_result(int* values, size_t n) {
  Result* res = malloc(sizeof(Result));
  res->value_count = n;
  res->value_type = INT;
  res->values = malloc(n * sizeof(int));
  for (size_t i = 0; i < n; i++)
    ((int*)res->values)[i] = values[i];
  return res;
}

void joins_work() {
  int pos1[8] = { 0, 2, 4, 6, 8, 10, 12, 14 };
  int val1[8] = { 0, 0, 1, 1, 2, 2, 2, 3 };
  int pos2[5] = { 5, 6, 7, 8, 9 };
  int val2[5] = { 0, 1, 1, 2, 2 };
  int exp1[12] = { 0, 2, 4, 4, 6, 6, 8, 8, 10, 10, 12, 12 };
  int exp2[12] = { 5, 5, 6, 7, 6, 7, 8, 9,  8,  9,  8,  9 };

  Result* p1 = setup_result(pos1, 8);
  Result* v1 = setup_result(val1, 8);
  Result* p2 = setup_result(pos2, 5);
  Result* v2 = setup_result(val2, 5);

  Result* res1 = malloc(sizeof(Result));
  res1->values = malloc(40 * sizeof(int));
  Result* res2 = malloc(sizeof(Result));
  res2->values = malloc(40 * sizeof(int));
  int* arrs[2] = { (int*)res1->values, (int*)res2->values };

  nested_loop_join(v1, p1, v2, p2, res1, res2);

  ASSERT_EQUAL_INTS(res1->value_count, 12);
  ASSERT_EQUAL_INTS(res2->value_count, 12);
  for (int i = 0; i < 12; i++) {
    ASSERT_EQUAL_INTS(exp1[i], arrs[0][i]);
    ASSERT_EQUAL_INTS(exp2[i], arrs[1][i]);
  }

  block_nested_loop_join(v1, p1, v2, p2, res1, res2, 4);

  ASSERT_EQUAL_INTS(res1->value_count, 12);
  ASSERT_EQUAL_INTS(res2->value_count, 12);
  group_quicksort(arrs[0], arrs, 2, 12);
  for (int i = 0; i < 12; i++) {
    ASSERT_EQUAL_INTS(exp1[i], arrs[0][i]);
    ASSERT_EQUAL_INTS(exp2[i], arrs[1][i]);
  }

  hash_join(v1, p1, v2, p2, res1, res2);
  ASSERT_EQUAL_INTS(res1->value_count, 12);
  ASSERT_EQUAL_INTS(res2->value_count, 12);
  for (int i = 0; i < 12; i++) {
    ASSERT_EQUAL_INTS(exp1[i], arrs[0][i]);
    ASSERT_EQUAL_INTS(exp2[i], arrs[1][i]);
  }

  free(v1->values); free(v1);
  free(p1->values); free(p1);
  free(v2->values); free(v2);
  free(p2->values); free(p2);
  free(res1->values); free(res1);
  free(res2->values); free(res2);
}

void join_parsing_works() {
  ColumnStore* store = load_column_store("testdata");
  ClientContext* context = init_client_context();
  struct message msg;
  Db* db = create_db(store, "db");
  Table* tbl1 = create_table(db, "tbl1", (size_t)2);
  Table* tbl2 = create_table(db, "tbl2", (size_t)2);
  create_column(tbl1, "col1");
  create_column(tbl1, "col2");
  create_column(tbl2, "col1");
  create_column(tbl2, "col2");

  int values[2];
  for (int i = 0; i < 10; i++) {
    values[0] = i;
    values[1] = 10 - i;
    relational_insert(tbl1, values);
  }
  for (int i = 5; i < 15; i++) {
    values[0] = i;
    values[1] = 15 - i;
    relational_insert(tbl2, values);
  }

  execute_db_operator(parse_command("pos1=select(db.tbl1.col2,null,4)", store, &msg, 0, context));
  execute_db_operator(parse_command("pos2=select(db.tbl2.col2,7,null)", store, &msg, 0, context));
  execute_db_operator(parse_command("val1=fetch(db.tbl1.col1,pos1)", store, &msg, 0, context));
  execute_db_operator(parse_command("val2=fetch(db.tbl2.col1,pos2)", store, &msg, 0, context));
  DbOperator* dbo = parse_command("j1,j2=join(val1,pos1,val2,pos2,hash)", store, &msg, 0, context);
  execute_db_operator(dbo);

  Result* res1 = get_client_variable(context, "j1");
  Result* res2 = get_client_variable(context, "j2");
  ASSERT_EQUAL_INTS(res1->value_count, 2);
  ASSERT_EQUAL_INTS(res2->value_count, 2);
  ASSERT_EQUAL_INTS(((int*)res1->values)[0], 7);
  ASSERT_EQUAL_INTS(((int*)res1->values)[1], 8);
  ASSERT_EQUAL_INTS(((int*)res2->values)[0], 2);
  ASSERT_EQUAL_INTS(((int*)res2->values)[1], 3);

  free_client_context(context);
  free_column_store(store);
}
