void group_quicksort_works() {
  int arr1[5] = { 5, 3, 4, 1, 2 };
  int arr2[5] = { 1, 2, 3, 4, 5 };
  int arr3[5] = { 5, 4, 3, 2, 1 };

  int* arrs[3] = { arr1, arr2, arr3 };

  group_quicksort(arr1, arrs, 3, 5);

  ASSERT_EQUAL_INTS(arr1[0], 1);
  ASSERT_EQUAL_INTS(arr1[1], 2);
  ASSERT_EQUAL_INTS(arr1[2], 3);
  ASSERT_EQUAL_INTS(arr1[3], 4);
  ASSERT_EQUAL_INTS(arr1[4], 5);

  ASSERT_EQUAL_INTS(arr2[0], 4);
  ASSERT_EQUAL_INTS(arr2[1], 5);
  ASSERT_EQUAL_INTS(arr2[2], 2);
  ASSERT_EQUAL_INTS(arr2[3], 3);
  ASSERT_EQUAL_INTS(arr2[4], 1);

  ASSERT_EQUAL_INTS(arr3[0], 2);
  ASSERT_EQUAL_INTS(arr3[1], 1);
  ASSERT_EQUAL_INTS(arr3[2], 4);
  ASSERT_EQUAL_INTS(arr3[3], 3);
  ASSERT_EQUAL_INTS(arr3[4], 5);

  group_quicksort(arr1, arrs, 3, 5);

  ASSERT_EQUAL_INTS(arr1[0], 1);
  ASSERT_EQUAL_INTS(arr1[1], 2);
  ASSERT_EQUAL_INTS(arr1[2], 3);
  ASSERT_EQUAL_INTS(arr1[3], 4);
  ASSERT_EQUAL_INTS(arr1[4], 5);

  ASSERT_EQUAL_INTS(arr2[0], 4);
  ASSERT_EQUAL_INTS(arr2[1], 5);
  ASSERT_EQUAL_INTS(arr2[2], 2);
  ASSERT_EQUAL_INTS(arr2[3], 3);
  ASSERT_EQUAL_INTS(arr2[4], 1);

  ASSERT_EQUAL_INTS(arr3[0], 2);
  ASSERT_EQUAL_INTS(arr3[1], 1);
  ASSERT_EQUAL_INTS(arr3[2], 4);
  ASSERT_EQUAL_INTS(arr3[3], 3);
  ASSERT_EQUAL_INTS(arr3[4], 5);
}

void group_external_sort_works() {
  int arr1[5] = { 5, 3, 4, 1, 2 };
  int arr2[5] = { 1, 2, 3, 4, 5 };
  int arr3[5] = { 5, 4, 3, 2, 1 };

  int* arrs[3] = { arr1, arr2, arr3 };

  group_external_sort(arrs, 5, 3, 0, 2);

  ASSERT_EQUAL_INTS(arr1[0], 1);
  ASSERT_EQUAL_INTS(arr1[1], 2);
  ASSERT_EQUAL_INTS(arr1[2], 3);
  ASSERT_EQUAL_INTS(arr1[3], 4);
  ASSERT_EQUAL_INTS(arr1[4], 5);

  ASSERT_EQUAL_INTS(arr2[0], 4);
  ASSERT_EQUAL_INTS(arr2[1], 5);
  ASSERT_EQUAL_INTS(arr2[2], 2);
  ASSERT_EQUAL_INTS(arr2[3], 3);
  ASSERT_EQUAL_INTS(arr2[4], 1);

  ASSERT_EQUAL_INTS(arr3[0], 2);
  ASSERT_EQUAL_INTS(arr3[1], 1);
  ASSERT_EQUAL_INTS(arr3[2], 4);
  ASSERT_EQUAL_INTS(arr3[3], 3);
  ASSERT_EQUAL_INTS(arr3[4], 5);

  group_external_sort(arrs, 5, 3, 0, 3);

  ASSERT_EQUAL_INTS(arr1[0], 1);
  ASSERT_EQUAL_INTS(arr1[1], 2);
  ASSERT_EQUAL_INTS(arr1[2], 3);
  ASSERT_EQUAL_INTS(arr1[3], 4);
  ASSERT_EQUAL_INTS(arr1[4], 5);

  ASSERT_EQUAL_INTS(arr2[0], 4);
  ASSERT_EQUAL_INTS(arr2[1], 5);
  ASSERT_EQUAL_INTS(arr2[2], 2);
  ASSERT_EQUAL_INTS(arr2[3], 3);
  ASSERT_EQUAL_INTS(arr2[4], 1);

  ASSERT_EQUAL_INTS(arr3[0], 2);
  ASSERT_EQUAL_INTS(arr3[1], 1);
  ASSERT_EQUAL_INTS(arr3[2], 4);
  ASSERT_EQUAL_INTS(arr3[3], 3);
  ASSERT_EQUAL_INTS(arr3[4], 5);
}

void binary_search_works() {
  int data[9] = { 1, 4, 8, 13, 22, 22, 23, 30, 30 };

  ASSERT_EQUAL_INTS(0, index_to_insert(0, data, 8));
  ASSERT_EQUAL_INTS(1, index_to_insert(1, data, 8));
  ASSERT_EQUAL_INTS(6, index_to_insert(22, data, 8));
  ASSERT_EQUAL_INTS(8, index_to_insert(30, data, 8));
  ASSERT_EQUAL_INTS(8, index_to_insert(31, data, 8));
  ASSERT_EQUAL_INTS(9, index_to_insert(31, data, 9));
  ASSERT_EQUAL_INTS(1, index_to_insert(31, data, 1));
  ASSERT_EQUAL_INTS(0, index_to_insert(31, data, 0));
}

void index_helpers_work() {
  int data[5] = { 1, 2, 2, 2, 3 };

  ASSERT_EQUAL_INTS(-1, last_index_less_than(1, data, 5));
  ASSERT_EQUAL_INTS(0, last_index_less_than(2, data, 5));
  ASSERT_EQUAL_INTS(3, last_index_less_than(3, data, 5));
  ASSERT_EQUAL_INTS(4, last_index_less_than(4, data, 5));

  ASSERT_EQUAL_INTS(0, first_index_at_least(-1, data, 5));
  ASSERT_EQUAL_INTS(0, first_index_at_least(1, data, 5));
  ASSERT_EQUAL_INTS(1, first_index_at_least(2, data, 5));
  ASSERT_EQUAL_INTS(4, first_index_at_least(3, data, 5));
  ASSERT_EQUAL_INTS(5, first_index_at_least(4, data, 5));
}
