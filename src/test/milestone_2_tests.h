void btree_works() {
  BTree* btree = btree_init();
  ASSERT(btree->root_is_leaf);
  ASSERT_EQUAL_INTS(((BLeaf*)btree->root)->length, 0);
  ASSERT_EQUAL_INTS(btree->size, 0);
  ASSERT_EQUAL_INTS(btree->min_val, 0);
  ASSERT_EQUAL_INTS(btree->max_val, 0);

  int results0[50];
  ASSERT_EQUAL_INTS(0, btree_scan_between(btree, results0, 0, 10));

  btree_insert(1, 0, btree);
  ASSERT(btree->root_is_leaf);
  ASSERT_EQUAL_INTS(((BLeaf*)btree->root)->length, 1);
  ASSERT_EQUAL_INTS(((BLeaf*)btree->root)->vals[0], 1);
  ASSERT_EQUAL_INTS(((BLeaf*)btree->root)->idxs[0], 0);
  ASSERT_EQUAL_INTS(btree->size, 1);
  ASSERT_EQUAL_INTS(btree->min_val, 1);
  ASSERT_EQUAL_INTS(btree->max_val, 1);

  ASSERT_EQUAL_INTS(1, btree_scan_between(btree, results0, 0, 10));
  ASSERT_EQUAL_INTS(0, results0[0]);

  int i;
  for (i = 0; i < LEAF_FANOUT-2; i++)
    btree_insert(2+i, 1+i, btree);

  ASSERT(btree->root_is_leaf);
  ASSERT_EQUAL_INTS(((BLeaf*)btree->root)->length, LEAF_FANOUT-1);
  ASSERT_EQUAL_INTS(btree->size, LEAF_FANOUT-1);
  ASSERT_EQUAL_INTS(btree->min_val, 1);
  ASSERT_EQUAL_INTS(btree->max_val, LEAF_FANOUT-1);
  
  ASSERT_EQUAL_INTS(10, btree_scan_between(btree, results0, 0, 11));
  ASSERT_EQUAL_INTS(0, results0[0]);
  ASSERT_EQUAL_INTS(9, results0[9]);

  ASSERT_EQUAL_INTS(10, btree_scan_between(btree, results0, 1, 11));
  ASSERT_EQUAL_INTS(0, results0[0]);
  ASSERT_EQUAL_INTS(9, results0[9]);

  ASSERT_EQUAL_INTS(10, btree_scan_between(btree, results0, 11, 21));
  ASSERT_EQUAL_INTS(10, results0[0]);
  ASSERT_EQUAL_INTS(19, results0[9]);

  ASSERT_EQUAL_INTS(10, btree_scan_between(btree, results0, LEAF_FANOUT-10, LEAF_FANOUT));
  ASSERT_EQUAL_INTS(LEAF_FANOUT-11, results0[0]);
  ASSERT_EQUAL_INTS(LEAF_FANOUT-2, results0[9]);

  btree_insert(2+i, 1+i, btree);

  ASSERT(!btree->root_is_leaf);
  BBranch* branch = (BBranch*)btree->root;
  ASSERT(branch->kids_are_leaves);
  ASSERT_EQUAL_INTS(branch->keys[0], LEAF_FANOUT/2 + 1);
  ASSERT_EQUAL_INTS(branch->length, 2);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[0])->length, LEAF_FANOUT/2);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[1])->length, LEAF_FANOUT/2);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[0])->vals[0], 1);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[1])->vals[0], 1+LEAF_FANOUT/2);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[1])->vals[LEAF_FANOUT/2-1], 2+i);
  ASSERT_EQUAL_INTS(btree->size, LEAF_FANOUT);
  ASSERT_EQUAL_INTS(btree->min_val, 1);
  ASSERT_EQUAL_INTS(btree->max_val, LEAF_FANOUT);

  int j = i+1;
  btree_insert(2+j, 1+j, btree);

  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[0])->length, LEAF_FANOUT/2);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[1])->length, 1+LEAF_FANOUT/2);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[1])->vals[LEAF_FANOUT/2], 2+j);

  int k;
  for (k = 1; k < LEAF_FANOUT/2; k++)
    btree_insert(2+j+k, 1+j+k, btree);

  ASSERT_EQUAL_INTS(branch->length, 3);
  ASSERT_EQUAL_INTS(branch->keys[0], LEAF_FANOUT/2 + 1);
  ASSERT_EQUAL_INTS(branch->keys[1], LEAF_FANOUT + 1);

  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[0])->vals[0], 1);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[1])->vals[0], 1+LEAF_FANOUT/2);
  ASSERT_EQUAL_INTS(((BLeaf*)branch->kids[2])->vals[0], 1+LEAF_FANOUT);

  int l;
  for (l = 1; l < LEAF_FANOUT*(LEAF_FANOUT-3)/2; l++)
    btree_insert(2+j+k+l, 1+j+k+l, btree);

  ASSERT_EQUAL_INTS(((BLeaf*)btree->root)->length, BRANCH_FANOUT-1);

  btree_insert(2+j+k+l, 1+j+k+l, btree);

  BBranch* root = (BBranch*)btree->root;
  ASSERT_EQUAL_INTS(root->length, 2);
  ASSERT_EQUAL_INTS(bbranch_min((BBranch*)root->kids[0]), 1);
  ASSERT_EQUAL_INTS(bbranch_min((BBranch*)root->kids[1]), 2+(LEAF_FANOUT/2)*(LEAF_FANOUT/2));

  btree_sync(btree, "testdata2/btree");
  BTree* btree2 = btree_load("testdata2/btree");
  BBranch* root2 = (BBranch*)btree->root;
  ASSERT_EQUAL_INTS(root2->length, 2);
  ASSERT_EQUAL_INTS(bbranch_min((BBranch*)root2->kids[0]), 1);
  ASSERT_EQUAL_INTS(bbranch_min((BBranch*)root2->kids[1]), 2+(LEAF_FANOUT/2)*(LEAF_FANOUT/2));

  size_t n;

  int results1[50];
  // test on original btree
  n = btree_scan_between(btree, results1, 30, 40);
  ASSERT_EQUAL_INTS(n, 10);
  for (int ni = 0; ni < 10; ni++)
    ASSERT_EQUAL_INTS(results1[ni], 29+ni);

  // also test on reloaded btree
  n = btree_scan_between(btree2, results1, 30, 60);
  ASSERT_EQUAL_INTS(n, 30);
  for (int ni = 0; ni < 30; ni++)
    ASSERT_EQUAL_INTS(results1[ni], 29+ni);

  int results2[100];
  n = btree_scan_less_than(btree2, results2, 51);
  ASSERT_EQUAL_INTS(n, 50);
  for (int ni = 0; ni < 50; ni++)
    ASSERT_EQUAL_INTS(results2[ni], ni);

  int results3[3*LEAF_FANOUT];
  int min = 3+j+k+l-LEAF_FANOUT*2;
  n = btree_scan_at_least(btree2, results3, min);
  ASSERT_EQUAL_INTS(n, 2*LEAF_FANOUT);
  for (int ni = 0; ni < (int)n; ni++)
    ASSERT_EQUAL_INTS(results3[ni], min+ni-1);

  ASSERT_EQUAL_INTS(1, btree_scan_between(btree2, results3, 2, 3));
  ASSERT_EQUAL_INTS(1, results3[0]);

  ASSERT_EQUAL_INTS(0, btree_scan_between(btree2, results3, -10, -1));
  ASSERT_EQUAL_INTS(0, btree_scan_at_least(btree2, results3, min+5*LEAF_FANOUT));
  ASSERT_EQUAL_INTS(0, btree_scan_less_than(btree2, results3, -10));

  btree_free(btree);
  btree_free(btree2);
}

void btree_works_with_random_insertions() {
  BTree* btree = btree_init();

  int total_insertions = 4*BRANCH_FANOUT*LEAF_FANOUT;
  int* values = malloc(total_insertions * sizeof(int));

  for (int j = 0; j < 4; j++)
    for (int i = j; i < total_insertions; i += 4)
      btree_insert(i, i, btree);

  ASSERT_EQUAL_INTS(btree->size, total_insertions);
  ASSERT_EQUAL_INTS(btree->depth, 3);

  ASSERT_EQUAL_INTS(total_insertions, btree_scan_at_least(btree, values, -1));
  for (int i = 0; i < total_insertions; i++)
    ASSERT_EQUAL_INTS(values[i], i);

  ASSERT_EQUAL_INTS(total_insertions/2, btree_scan_at_least(btree, values, total_insertions/2));
  for (int i = total_insertions/2; i < total_insertions; i++)
    ASSERT_EQUAL_INTS(values[i-total_insertions/2], i);

  ASSERT_EQUAL_INTS(total_insertions/2, btree_scan_less_than(btree, values, total_insertions/2));
  for (int i = 0; i < total_insertions/2; i++)
    ASSERT_EQUAL_INTS(values[i], i);

  ASSERT_EQUAL_INTS(total_insertions/2, btree_scan_between(btree, values, total_insertions/4, 3*total_insertions/4));
  for (int i = total_insertions/4; i < 3*total_insertions/4; i++)
    ASSERT_EQUAL_INTS(values[i-total_insertions/4], i);

  btree_free(btree);
  free(values);
}

void we_pick_the_right_scan_type() {
  ColumnStore* store = load_column_store("testdata");
  Db* db = create_db(store, "db");
  Table* table = create_table(db, "table", (size_t)3);
  Column* column1 = create_column(table, "column1");
  Column* column2 = create_column(table, "column2");
  Column* column3 = create_column(table, "column3");
  int values[3];
  Result* res;

  create_primary_index(table, "column1");

  for (int i = 0; i < 400; i += 2) {
    values[0] = i;
    values[1] = 400-i;
    values[2] = 0;
    relational_insert(table, values);
  }
  for (int i = 1; i < 400; i += 2) {
    values[0] = i;
    values[1] = 400-i;
    values[2] = 1;
    relational_insert(table, values);
  }
  rebuild_table_indexes(table);

  ASSERT_EQUAL_INTS(column1->length, 400);
  ASSERT_EQUAL_INTS(column2->length, 400);
  ASSERT_EQUAL_INTS(column3->length, 400);

  create_secondary_index(table, "column2");

  ASSERT_EQUAL_INTS(column2->btree->size, 400);

  ClientContext* context = init_client_context();
  struct message msg;
  
  DbOperator* op = parse_command("s1=select(db.table.column1,100,200)", store, &msg, 0, context);
  res = op->operator_fields.select_operator.result;
  execute_db_operator(op);
  ASSERT_EQUAL_INTS(res->value_count, 100);
  ASSERT(res->used_sorted_scan);
  for (int i = 0; i < 100; i++)
    ASSERT_EQUAL_INTS(((int*)res->values)[i], 100 + i);

  op = parse_command("s2=select(db.table.column2,190,200)", store, &msg, 0, context);
  res = op->operator_fields.select_operator.result;
  execute_db_operator(op);
  ASSERT_EQUAL_INTS(res->value_count, 10);
  ASSERT(res->used_btree_scan);
  for (int i = 0; i < 10; i++)
    ASSERT_EQUAL_INTS(((int*)res->values)[i], 210 - i);

  op = parse_command("s3=select(db.table.column3,0,1)", store, &msg, 0, context);
  res = op->operator_fields.select_operator.result;
  execute_db_operator(op);
  ASSERT_EQUAL_INTS(res->value_count, 200);
  ASSERT(res->used_array_scan);
  for (int i = 0; i < 200; i++)
    ASSERT_EQUAL_INTS(((int*)res->values)[i], 2*i);

  op = parse_command("s2=select(db.table.column2,-100,500)", store, &msg, 0, context);
  res = op->operator_fields.select_operator.result;
  execute_db_operator(op);
  ASSERT_EQUAL_INTS(res->value_count, 400);
  ASSERT(res->used_array_scan);

  free_client_context(context);
  free_column_store(store);
}

void sorted_columns_work() {
  ColumnStore* store = load_column_store("testdata");
  Db* db = create_db(store, "db");
  Table* table = create_table(db, "table", (size_t)2);
  Column* column1 = create_column(table, "column1");
  Column* column2 = create_column(table, "column2");
  int values[2];

  ASSERT_EQUAL_INTS(table->leader, -1);
  ASSERT_EQUAL_INTS(table->is_sorted, 0);
  create_primary_index(table, "column1");
  ASSERT_EQUAL_INTS(table->leader, 0);
  ASSERT_EQUAL_INTS(table->is_sorted, 1);

  for (int i = 0; i < 400; i += 2) {
    values[0] = i;
    values[1] = 400-i;
    relational_insert(table, values);
  }
  for (int i = 1; i < 400; i += 2) {
    values[0] = i;
    values[1] = 400-i;
    relational_insert(table, values);
  }

  ASSERT_EQUAL_INTS(table->row_count, 400);
  ASSERT_EQUAL_INTS(column1->length, 400);
  ASSERT_EQUAL_INTS(column2->length, 400);

  rebuild_table_indexes(table);

  for (int i = 0; i < 400; i++) {
    ASSERT_EQUAL_INTS(column1->data[i], i);
    ASSERT_EQUAL_INTS(column2->data[i], 400-i);
  }

  strcpy(store->data_dir, "testdata2"); // change directory!
  sync_column_store(store);
  FILE* f = fopen("testdata2/db/table/columns.csv", "r");
  char col_l0[1024]; fgets(col_l0, 1024, f); ASSERT_EQUAL_STRS(col_l0, "column_name,is_sorted,has_btree\n");
  char col_l1[1024]; fgets(col_l1, 1024, f); ASSERT_EQUAL_STRS(col_l1, "column1,1,0\n");
  char col_l2[1024]; fgets(col_l2, 1024, f); ASSERT_EQUAL_STRS(col_l2, "column2,0,0\n");
  fclose(f);

  free_column_store(store);
}

void we_support_multiple_clustered_indexes() {
  ColumnStore* store = load_column_store("testdata");
  Db* db = create_db(store, "db");
  Table* table = create_table(db, "table", (size_t)2);
  Column* column1 = create_column(table, "column1");
  Column* column2 = create_column(table, "column2");
  create_primary_index(table, "column2");
  create_primary_index(table, "column1");
  create_secondary_index(table, "column1");
  Table* replica = table->replicas[0];
  Column* repcol1 = replica->columns[0];
  Column* repcol2 = replica->columns[1];

  ASSERT_EQUAL_INTS(table->row_count, 0);
  ASSERT_EQUAL_INTS(table->column_count, 2);
  ASSERT_EQUAL_INTS(table->column_capacity, 2);
  ASSERT_EQUAL_INTS(replica->row_count, 0);
  ASSERT_EQUAL_INTS(replica->column_count, 2);
  ASSERT_EQUAL_INTS(replica->column_capacity, 2);
  ASSERT_EQUAL_INTS(replica->row_capacity, table->row_capacity);
  ASSERT_EQUAL_STRS(repcol1->name, column1->name);
  ASSERT_EQUAL_STRS(repcol2->name, column2->name);
  ASSERT_EQUAL_INTS(repcol1->length, 0);
  ASSERT_EQUAL_INTS(repcol2->length, 0);
  ASSERT_EQUAL_INTS(column1->length, 0);
  ASSERT_EQUAL_INTS(column2->length, 0);
  ASSERT_EQUAL_INTS(column1->is_sorted, 0);
  ASSERT_EQUAL_INTS(column2->is_sorted, 1);
  ASSERT_EQUAL_INTS(repcol1->is_sorted, 1);
  ASSERT_EQUAL_INTS(repcol2->is_sorted, 0);
  ASSERT_DIFFN_PTRS(column1->btree, NULL);
  ASSERT_EQUAL_PTRS(column2->btree, NULL);
  ASSERT_EQUAL_PTRS(repcol1->btree, NULL);
  ASSERT_EQUAL_PTRS(repcol2->btree, NULL);
  ASSERT_EQUAL_INTS(table->leader, 1);
  ASSERT_EQUAL_INTS(table->is_sorted, 1);
  ASSERT_EQUAL_INTS(replica->leader, 0);
  ASSERT_EQUAL_INTS(replica->is_sorted, 1);

  int values[2];
  int max = 20;

  // load via insert
  for (int i = 0; i < max/2; i++) {
    values[0] = i;
    values[1] = max - i;
    relational_insert(table, values);
  }

  // bulk-load
  int payload[max];
  for (int i = 0; i < max/2; i++) {
    payload[i] = i + max/2;
    payload[i+max/2] = max - i - max/2;
  }
  load_data_payload(table, max/2, payload);

  ASSERT_EQUAL_INTS(max, column1->length);
  ASSERT_EQUAL_INTS(max, column2->length);
  ASSERT_EQUAL_INTS(max, repcol1->length);
  ASSERT_EQUAL_INTS(max, repcol2->length);

  ASSERT_EQUAL_INTS(1, column2->data[0]);
  ASSERT_EQUAL_INTS(max, column2->data[max-1]);
  ASSERT_EQUAL_INTS(0, column1->data[max-1]);
  ASSERT_EQUAL_INTS(max-1, column1->data[0]);

  ASSERT_EQUAL_INTS(0, repcol1->data[0]);
  ASSERT_EQUAL_INTS(max-1, repcol1->data[max-1]);
  ASSERT_EQUAL_INTS(max, repcol2->data[0]);
  ASSERT_EQUAL_INTS(1, repcol2->data[max-1]);

  ClientContext* context = init_client_context();
  struct message msg;

  execute_db_operator(parse_command(
      "s1=select(db.table.column1,2,8)", store, &msg, 0, context));
  execute_db_operator(parse_command(
      "s2=select(db.table.column2,2,8)", store, &msg, 0, context));

  Result* s1 = get_client_variable(context, "s1");
  Result* s2 = get_client_variable(context, "s2");
  ASSERT(s1->used_sorted_scan);
  ASSERT(s2->used_sorted_scan);
  ASSERT_EQUAL_PTRS(s1->relative_to, replica);
  ASSERT_EQUAL_PTRS(s2->relative_to, table);

  execute_db_operator(parse_command(
      "f1=fetch(db.table.column2,s1)", store, &msg, 0, context));
  execute_db_operator(parse_command(
      "f2=fetch(db.table.column1,s2)", store, &msg, 0, context));

  Result* f1 = get_client_variable(context, "f1");
  Result* f2 = get_client_variable(context, "f2");
  ASSERT_EQUAL_PTRS(f1->relative_to, replica);
  ASSERT_EQUAL_PTRS(f2->relative_to, table);

  DbOperationResult res;
  res = execute_db_operator(parse_command("print(f1)", store, &msg, 0, context));
  ASSERT_EQUAL_STRS(res.message, "18\n17\n16\n15\n14\n13");
  free(res.message);
  res = execute_db_operator(parse_command("print(f2)", store, &msg, 0, context));
  ASSERT_EQUAL_STRS(res.message, "18\n17\n16\n15\n14\n13");
  free(res.message);

  free_client_context(context);
  free_column_store(store);
}

void btree_bulk_load_works() {
  size_t length = LEAF_FANOUT * BRANCH_FANOUT * 2;
  int* vals = malloc(length * sizeof(int));
  int* idxs = malloc(length * sizeof(int));
  for (size_t i = 0; i < length; i++) {
    vals[i] = (int)i;
    idxs[i] = (int)i;
  }
  shuffle(vals, length);

  int* arrs[2];
  arrs[0] = vals;
  arrs[1] = idxs;
  group_quicksort(vals, arrs, 2, length);

  BTree* btree = btree_bulk_load_sorted(vals, idxs, length);

  int* results = malloc(length * sizeof(int));
  size_t n_hits = btree_scan_less_than(btree, results, length/2);

  ASSERT_EQUAL_INTS(n_hits, length/2);
  for (size_t i = 0; i < length/2; i++)
    ASSERT_EQUAL_INTS(results[i], idxs[i]);

  free(vals);
  free(idxs);
  free(results);
  btree_free(btree);
}
