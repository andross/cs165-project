void bitvectors_work() {
  int* bitvec = bitvector_init(100);
  for (int i = 0; i < 100; i++)
    ASSERT_EQUAL_INTS(0, bitvector_get(bitvec, i));
  bitvector_set(bitvec, 10);
  for (int i = 0; i < 10; i++)
    ASSERT_EQUAL_INTS(0, bitvector_get(bitvec, i));
  for (int i = 11; i < 100; i++)
    ASSERT_EQUAL_INTS(0, bitvector_get(bitvec, i));
  ASSERT(bitvector_get(bitvec, 10) > 0);
  free(bitvec);
}

void deletes_work() {
  ColumnStore* store = load_column_store("testdata");
  Db* db = create_db(store, "db");
  Table* table = create_table(db, "table", (size_t)2);
  create_column(table, "column1");
  create_column(table, "column2");
  ClientContext* context = init_client_context();
  struct message msg;

  for (int i = 0; i < 80; i++) {
    int data[2] = { i, i+1 };
    relational_insert(table, data);
  }

  int del[5] = { 1, 5, 7, 8, 14 };
  relational_delete(table, del, 5);

  DbOperator* s = parse_command("s=select(db.table.column1,null,40)", store, &msg, 0, context);
  execute_db_operator(s);

  Result* r = get_client_variable(context, "s");

  ASSERT_EQUAL_INTS(r->value_count, 35);
  ASSERT_EQUAL_INTS(((int*)r->values)[0], 0);
  ASSERT_EQUAL_INTS(((int*)r->values)[1], 2);
  ASSERT_EQUAL_INTS(((int*)r->values)[4], 6);

  free_client_context(context);
  free_column_store(store);
}

void mixed_sorted_deletes_updates_and_inserts_work() {
  ColumnStore* store = load_column_store("testdata");
  Db* db = create_db(store, "db");
  Table* table = create_table(db, "table", (size_t)2);
  Column* column1 = create_column(table, "column1");
  Column* column2 = create_column(table, "column2");
  ClientContext* context = init_client_context();
  struct message msg;

  create_primary_index(table, "column1");

  // insert in sorted order
  for (int i = 10; i < 60; i++) {
    int data[2] = { i, i+1 };
    relational_insert(table, data);
  }
  ASSERT_EQUAL_INTS(column1->sorted_until, 50);
  ASSERT_EQUAL_INTS(column2->sorted_until, 0);

  // now throw a wrench
  for (int i = 0; i < 10; i++) {
    int data[2] = { i, i+1 };
    relational_insert(table, data);
  }
  ASSERT_EQUAL_INTS(column1->sorted_until, 50);
  ASSERT_EQUAL_INTS(column1->length, 60);

  // when we select, we should do a sorted scan over the sorted portion
  // as well as an array scan over the non-sorted portion
  execute_db_operator(parse_command(
        "a=select(db.table.column1,null,50)", store, &msg, 0, context));
  ASSERT(get_client_variable(context, "a")->used_sorted_scan);

  // and still get the right results
  execute_db_operator(parse_command(
        "b=fetch(db.table.column2,a)", store, &msg, 0, context));
  execute_db_operator(parse_command(
        "c=sum(b)", store, &msg, 0, context));
  Result* r = get_client_variable(context, "c");
  ASSERT_EQUAL_INTS(((int*)r->values)[0], 25 * 51); // n(n+1)/2
  
  // updates and deletes should be fine
  int positions[1] = { 45 }; // this was previously unselected 
  size_t n_positions = 1;

  // updates to non-sorted columns should not cause any deletes
  relational_update(table, "column2", positions, n_positions, 1000);
  ASSERT_EQUAL_INTS(table->pending_delete_count, 0);

  // but updates to sorted ones should
  relational_update(table, "column1", positions, n_positions, 1);
  ASSERT_EQUAL_INTS(table->pending_delete_count, 1);
  ASSERT_EQUAL_INTS(table->row_count, 61);
  ASSERT_EQUAL_INTS(column1->length, 61);
  ASSERT_EQUAL_INTS(column2->length, 61);
  ASSERT_EQUAL_INTS(column1->sorted_until, 50);

  // we should maintain correctness in query results
  execute_db_operator(parse_command(
        "a2=select(db.table.column1,null,50)", store, &msg, 0, context));
  execute_db_operator(parse_command(
        "b2=fetch(db.table.column2,a2)", store, &msg, 0, context));
  execute_db_operator(parse_command(
        "c2=sum(b2)", store, &msg, 0, context));
  ASSERT(get_client_variable(context, "a2")->used_sorted_scan)
  r = get_client_variable(context, "c2");
  ASSERT_EQUAL_INTS(((int*)r->values)[0], 25 * 51 + 1000);

  // now let's undo what we just did with a delete
  int positions2[1] = { table->row_count - 1 }; // 1000 is at the end
  relational_delete(table, positions2, n_positions);
  ASSERT_EQUAL_INTS(table->pending_delete_count, 2);
  execute_db_operator(parse_command(
        "a3=select(db.table.column1,null,50)", store, &msg, 0, context));
  execute_db_operator(parse_command(
        "b3=fetch(db.table.column2,a3)", store, &msg, 0, context));
  execute_db_operator(parse_command(
        "c3=sum(b3)", store, &msg, 0, context));
  ASSERT(get_client_variable(context, "a3")->used_sorted_scan)
  r = get_client_variable(context, "c3");
  ASSERT_EQUAL_INTS(((int*)r->values)[0], 25 * 51);

  // finally, we want to persist these changes to disk. as a first pass,
  // we really will rebuild everything on shutdown...
  strcpy(store->data_dir, "testdata2");
  sync_column_store(store);

  ColumnStore* store2 = load_column_store("testdata2");
  Db* db2 = find_db(store2, "db");
  Table* table2 = find_table(db2, "table");
  Column* c1 = find_column(table2, "column1");
  ASSERT_EQUAL_INTS(table2->row_count, 59);
  ASSERT_EQUAL_INTS(c1->length, 59);
  ASSERT(c1->is_sorted);
  ASSERT_EQUAL_INTS(c1->sorted_until, 59);
  int val = c1->data[0];
  for (size_t i = 1; i < c1->length; i++)
    ASSERT(val <= c1->data[i++]);

  free_client_context(context);
  free_column_store(store);
  free_column_store(store2);
}
