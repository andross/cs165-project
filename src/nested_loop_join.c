#include "nested_loop_join.h"

void nested_loop_join(Result* Lval, Result* Lpos, Result* Rval, Result* Rpos, Result* Lres, Result* Rres) {
  size_t L_size = Lval->value_count;
  size_t R_size = Rval->value_count;
  int* L_positions = (int*)Lpos->values;
  int* R_positions = (int*)Rpos->values;
  int* L_results = (int*)Lres->values;
  int* R_results = (int*)Rres->values;
  int* L_values = (int*)Lval->values;
  int* R_values = (int*)Rval->values;

  size_t k = 0;
  for (size_t l = 0; l < L_size; l++) {
    for (size_t r = 0; r < R_size; r++) {
      if (L_values[l] == R_values[r]) {
        L_results[k] = L_positions[l];
        R_results[k] = R_positions[r];
        k++;
      }
    }
  }

  Lres->value_count = k;
  Rres->value_count = k;
}

void block_nested_loop_join(Result* Lval, Result* Lpos, Result* Rval, Result* Rpos, Result* Lres, Result* Rres, size_t pagesize) {
  size_t L_size = Lval->value_count;
  size_t R_size = Rval->value_count;
  int* L_positions = (int*)Lpos->values;
  int* R_positions = (int*)Rpos->values;
  int* L_results = (int*)Lres->values;
  int* R_results = (int*)Rres->values;
  int* L_values = (int*)Lval->values;
  int* R_values = (int*)Rval->values;

  size_t k = 0;
  size_t i = 0;
  while (i < L_size) {
    size_t j = 0;
    size_t next_i = MIN(i + pagesize, L_size);
    while (j < R_size) {
      size_t next_j = MIN(j + pagesize, R_size);
      for (size_t l = i; l < next_i; l++) {
        for (size_t r = j; r < next_j; r++) {
          if (L_values[l] == R_values[r]) {
            L_results[k] = L_positions[l];
            R_results[k] = R_positions[r];
            k++;
          }
        }
      }
      j = next_j;
    }
    i = next_i;
  }

  Lres->value_count = k;
  Rres->value_count = k;
}
