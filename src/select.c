#include "select.h"

size_t select_everything(int* results, size_t n, size_t offset) {
  for (size_t i = 0; i < n; i++)
    results[i] = i + offset;
  return n;
}

size_t select_at_least(long int min, int* values, int* results, size_t n, size_t offset) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++) {
    results[p] = i + offset;
    p += (values[i] >= min);
  }
  return p;
}

size_t select_less_than(long int max, int* values, int* results, size_t n, size_t offset) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++) {
    results[p] = i + offset;
    p += (values[i] < max);
  }
  return p;
}

size_t select_between(long int min, long int max, int* values, int* results, size_t n, size_t offset) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++) {
    results[p] = i + offset;
    p += ((values[i] >= min) & (values[i] < max));
  }
  return p;
}

size_t ifselect_at_least(long int min, int* values, int* results, size_t n, size_t offset) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++)
    if (values[i] >= min)
      results[p++] = i + offset;
  return p;
}

size_t ifselect_less_than(long int max, int* values, int* results, size_t n, size_t offset) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++)
    if (values[i] < max)
      results[p++] = i + offset;
  return p;
}

size_t ifselect_between(long int min, long int max, int* values, int* results, size_t n, size_t offset) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++)
    if (values[i] >= min & values[i] < max)
      results[p++] = i + offset;
  return p;
}

size_t pselect_everything(int* positions, int* results, size_t n) {
  for (size_t i = 0; i < n; i++)
    results[i] = positions[i];
  return n;
}

size_t pselect_at_least(long int min, int* values,  int* positions, int* results, size_t n) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++) {
    results[p] = positions[i];
    p += (values[i] >= min);
  }
  return p;
}
size_t pselect_less_than(long int max, int* values,  int* positions, int* results, size_t n) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++) {
    results[p] = positions[i];
    p += (values[i] < max);
  }
  return p;
}

size_t pselect_between(long int min, long int max, int* values, int* positions, int* results, size_t n) {
  size_t p = 0;
  for (size_t i = 0; i < n; i++) {
    results[p] = positions[i];
    p += ((values[i] >= min) & (values[i] < max));
  }
  return p;
}

size_t vanilla_select(int* values, int* results, Comparator comp, size_t n, size_t offset) {
  ComparatorType minc = comp.min_comparator_type;
  ComparatorType maxc = comp.max_comparator_type;
  long int max = comp.max_value;
  long int min = comp.min_value;
  if (minc == NO_COMPARISON && maxc == NO_COMPARISON)
    return select_everything(results, n, offset);
  else if (minc == NO_COMPARISON && maxc == LESS_THAN)
    return select_less_than(max, values, results, n, offset);
  else if (minc == GREATER_THAN_OR_EQUAL && maxc == NO_COMPARISON)
    return select_at_least(min, values, results, n, offset);
  else
    return select_between(min, max, values, results, n, offset);
}

size_t position_select(int* values, int* positions, int* results, Comparator comp, size_t n) {
  ComparatorType minc = comp.min_comparator_type;
  ComparatorType maxc = comp.max_comparator_type;
  long int max = comp.max_value;
  long int min = comp.min_value;
  if (minc == NO_COMPARISON && maxc == NO_COMPARISON)
    return pselect_everything(positions, results, n);
  else if (minc == NO_COMPARISON && maxc == LESS_THAN)
    return pselect_less_than(max, values, positions, results, n);
  else if (minc == GREATER_THAN_OR_EQUAL && maxc == NO_COMPARISON)
    return pselect_at_least(min, values, positions, results, n);
  else
    return pselect_between(min, max, values, positions, results, n);
}

void execute_select(int* values, int* positions, Result* result, Comparator comp, size_t max_value_count) {
  if (positions == NULL)
    result->value_count = vanilla_select(values, (int*)result->values, comp, max_value_count, 0);
  else
    result->value_count = position_select(values, positions, (int*)result->values, comp, max_value_count);
}
