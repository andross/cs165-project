#include "hash_join.h"

void hash_join(
    Result* Lval, Result* Lpos,
    Result* Rval, Result* Rpos,
    Result* Lres, Result* Rres)
{
  size_t L_size = Lval->value_count;
  size_t R_size = Rval->value_count;
  int* L_positions = (int*)Lpos->values;
  int* R_positions = (int*)Rpos->values;
  int* L_results = (int*)Lres->values;
  int* R_results = (int*)Rres->values;
  int* L_values = (int*)Lval->values;
  int* R_values = (int*)Rval->values;

  Hashtable* h =  newHashtable();
  for (size_t r = 0; r < R_size; r++)
    hashtablePut(h, R_values[r], R_positions[r]);

  size_t k = 0;
  int n_hits;
  for (size_t l = 0; l < L_size; l++) {
    n_hits = hashtableGet(h, L_values[l], R_results + k, (int)(R_size - k));
    for (int i = 0; i < n_hits; i++)
      L_results[k + i] = L_positions[l];
    k += n_hits;
  }

  Lres->value_count = k;
  Rres->value_count = k;

  freeHashtable(h);
}
