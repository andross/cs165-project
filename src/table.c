#include "table.h"

Table* init_table(char* table_name, size_t column_capacity, size_t row_capacity) {
  Table* table = malloc(sizeof(Table));
  strcpy(table->name, table_name);

  table->row_count = 0;
  table->row_capacity = row_capacity;
  table->replica_count = 0;
  table->is_sorted = 0;
  table->leader = -1;

  table->column_count = 0;
  table->column_capacity = column_capacity;
  table->columns = malloc(column_capacity * sizeof(Column*));
  table->pending_deletes = NULL;
  table->pending_delete_count = 0;

  return table;
}

Table* load_table(char* db_dir, char* table_name, size_t column_capacity, size_t row_count, size_t row_capacity, size_t replica_count) {
  Table* table = init_table(table_name, column_capacity, row_capacity);
  table->row_count = row_count;
  table->replica_count = replica_count;

  char table_dir[1024];
  filename_join(table_dir, 2, db_dir, table_name);

  char filename[1024];
  filename_join(filename, 2, table_dir, "columns.csv");

  CsvReader* csv = csv_reader(filename);
  while (read_csv_row(csv)) {
    char column_name[64];
    char is_sorted[8];
    char has_btree[8];
    read_csv_cell(column_name, "column_name", csv);
    read_csv_cell(is_sorted, "is_sorted", csv);
    read_csv_cell(has_btree, "has_btree", csv);
    table->columns[csv->n_rows_read-1] = load_column(table_dir, column_name, table->row_count, table->row_capacity, atoi(is_sorted), atoi(has_btree));
    if (atoi(is_sorted)) {
      table->is_sorted = 1;
      table->leader = csv->n_rows_read-1;
    }
  }
  table->column_count = csv->n_rows_read;
  free_csv_reader(csv);

  if (table->replica_count > 0) {
    char replica_dir[1024];
    filename_join(replica_dir, 2, table_dir, "replicas");
    for (size_t i = 0; i < table->replica_count; i++) {
      char replica_name[MAX_SIZE_NAME];
      sprintf(replica_name, "replica%d", (int)i);
      table->replicas[i] = load_table(replica_dir, replica_name, column_capacity, row_count, row_capacity, 0);
    }
  }

  return table;
}

void sync_table(Table* table, char* db_dir) {
  char table_dir[1024];
  filename_join(table_dir, 2, db_dir, table->name);
  system_mkdir_p(table_dir);

  char filename[1024];
  filename_join(filename, 2, table_dir, "columns.csv");

  CsvWriter* csv = csv_writer(filename, 3, "column_name", "is_sorted", "has_btree");
  for (size_t i = 0; i < table->column_count; i++) {
    Column* col = table->columns[i];
    char is_sorted[8]; sprintf(is_sorted, "%d", (int)col->is_sorted);
    char has_btree[8]; sprintf(has_btree, "%d", (int)(col->btree != NULL));
    write_csv_cell(col->name, "column_name", csv);
    write_csv_cell(is_sorted, "is_sorted", csv);
    write_csv_cell(has_btree, "has_btree", csv);
    write_csv_row(csv);
  }
  free_csv_writer(csv);

  for (size_t i = 0; i < table->column_count; i++)
    sync_column(table->columns[i], table_dir, table->row_count);

  char replica_dir[1024];
  filename_join(replica_dir, 2, table_dir, "replicas");
  for (size_t i = 0; i < table->replica_count; i++)
    sync_table(table->replicas[i], replica_dir);
}

void load_data_payload(Table* table, size_t row_count, int* payload) {
  // resize the table's row capacity if necessary
  if (table->row_count + row_count > table->row_capacity) {
    log_info("resizing row capacity for load into %s\n", table->name);
    table->row_capacity = 2 * (table->row_count + row_count);
    for (size_t i = 0; i < table->column_count; i++) {
      Column* col = table->columns[i];
      int* bigger_data = malloc(table->row_capacity * sizeof(int));
      memcpy(bigger_data, col->data, col->length * sizeof(int));
      free(col->data);
      col->data = bigger_data;
    }
  }

  // load the data (assuming it's divided into n_cols (n_rows * integer size)
  // chunks)
  table->row_count += row_count;
  for (size_t i = 0; i < table->column_count; i++) {
    Column* col = table->columns[i];
    int* column_data_ptr = col->data + col->length;
    int* payload_data_ptr = payload + i*row_count;
    memcpy(column_data_ptr, payload_data_ptr, row_count*sizeof(int));
    col->length += row_count;
  }

  for (size_t i = 0; i < table->replica_count; i++)
    load_data_payload(table->replicas[i], row_count, payload);

  rebuild_table_indexes(table);
}

void relational_delete(Table* table, int* positions, size_t n_positions) {
  if (table->pending_delete_count == 0)
    table->pending_deletes = bitvector_init(table->row_capacity);

  table->pending_delete_count += n_positions;

  for (size_t i = 0; i < n_positions; i++)
    bitvector_set(table->pending_deletes, positions[i]);

  for (size_t i = 0; i < table->replica_count; i++)
    relational_delete(table->replicas[i], positions, n_positions);
}


void relational_update(Table* table, char* column_name, int* positions, size_t n_positions, int new_value) {
  Column* column = find_column(table, column_name);
  if (column->is_sorted) {
    //delete
    relational_delete(table, positions, n_positions);
    //then insert
    size_t column_index = find_column_index(table, column_name);
    int values[table->column_count];
    for (size_t i = 0; i < n_positions; i++) {
      for (size_t j = 0; j < table->column_count; j++)
        values[j] = (j == column_index) ? new_value : table->columns[j]->data[positions[i]];
      relational_insert(table, values);
    }
  } else {
    for (size_t i = 0; i < n_positions; i++) {
      int position = positions[i];
      int old_value = column->data[position];
      column->data[position] = new_value;
      if (column->btree != NULL) {
        btree_delete(old_value, position, column->btree);
        btree_insert(new_value, position, column->btree);
      }
    }
  }

  for (size_t i = 0; i < table->replica_count; i++)
    relational_update(table->replicas[i], column_name, positions, n_positions, new_value);
}

void squash_pending_deletes(Table* table) {
  // oof -- this is gonna be expensive.
  if (table->pending_delete_count > 0) {
    size_t n_kept = 0;
    for (size_t j = 0; j < table->row_count; j++) {
      for (size_t i = 0; i < table->column_count; i++) {
        table->columns[i]->data[n_kept] = table->columns[i]->data[j];
      }
      n_kept += !bitvector_get(table->pending_deletes, j);
    }
    for (size_t i = 0; i < table->column_count; i++) {
      table->columns[i]->length = n_kept;
      if (table->columns[i]->is_sorted)
        table->columns[i]->sorted_until -= (table->row_count - n_kept);
    }
    table->row_count = n_kept;
    table->pending_delete_count = 0;
    free(table->pending_deletes);
    table->pending_deletes = NULL;
  }
}

void rebuild_table_indexes(Table* table) {
  squash_pending_deletes(table);

  if (table->is_sorted && table->row_count > 1) {
    int* leader = table->columns[table->leader]->data;
    if (!is_already_sorted(leader, table->row_count)) {
      int* arrays[table->column_count];
      for (size_t i = 0; i < table->column_count; i++)
        arrays[i] = table->columns[i]->data;

      group_quicksort(leader, arrays,
          (int)table->column_count, table->row_count);
    }
    table->columns[table->leader]->sorted_until = table->row_count;
  }

  for (size_t i = 0; i < table->column_count; i++)
    rebuild_column_index(table->columns[i]);
}

void rebuild_before_syncing_if_necessary(Table* table) {
  if (table->pending_delete_count > 0) {
    rebuild_table_indexes(table);
  } else if (table->is_sorted) {
    Column* col = table->columns[table->leader];
    if (col->sorted_until < col->length)
      rebuild_table_indexes(table);
  }

  for (size_t i = 0; i < table->replica_count; i++)
    rebuild_before_syncing_if_necessary(table->replicas[i]);
}

void free_table(Table* table) {
  for (size_t i = 0; i < table->replica_count; i++)
    free_table(table->replicas[i]);
  for (size_t i = 0; i < table->column_count; i++)
    free_column(table->columns[i]);
  if (table->pending_deletes != NULL)
    free(table->pending_deletes);
  free(table->columns);
  free(table);
}

Table* replicate_table(Table* table) {
  if (table->replica_count > MAX_REPLICAS) {
    log_err("too many replicas on table %s\n", table->name);
    return NULL;
  }
  char replica_name[MAX_SIZE_NAME];
  sprintf(replica_name, "replica%d", (int)table->replica_count);
  Table* replica = init_table(replica_name, table->column_capacity, table->row_capacity);
  for (size_t i = 0; i < table->column_count; i++)
    replica->columns[i] = replicate_column(table->columns[i], table->row_capacity);
  replica->column_count = table->column_count;
  replica->row_count = table->row_count;
  table->replicas[table->replica_count] = replica;
  table->replica_count++;
  return replica;
}

Table* best_replica_for(char* column_name, Table* table) {
  int i = find_column_index(table, column_name);
  if (i < 0) {
    log_err("can't find column with name %s\n", column_name);
    return NULL;
  }

  for (size_t j = 0; j < table->replica_count; j++)
    if (table->replicas[j]->leader == i)
      return table->replicas[j];

  return table;
}

int create_primary_index(Table* table, char* column_name) {
  int i = find_column_index(table, column_name);
  if (i < 0) {
    log_err("can't find column with name %s\n", column_name);
    return 0;
  }

  // if we're already sorted...
  if (table->is_sorted) {
    // then first check if we already have a different
    // sorted replica; if so, this could be a duplicate command
    Table* replica;
    for (size_t j = 0; j < table->replica_count; j++) {
      replica = table->replicas[j];
      if (replica->is_sorted && replica->leader == i) {
        log_info("clustered index on %s.%s already exists\n", table->name, column_name);
        return 0;
      }
    }
    // if not, create a new replica sorted by this column
    log_info("creating replica on table %s so we can add a clustered index on %s\n", table->name, column_name);
    replica = replicate_table(table);
    return create_primary_index(replica, column_name);
  // if we're not sorted, then this is our first clustered index, so let's
  // update the main copy of the data
  } else {
    // mark the table/column as being sorted
    table->leader = i;
    table->is_sorted = true;
    table->columns[i]->is_sorted = true;

    rebuild_table_indexes(table);

    log_info("created primary index for %s\n", column_name);
    return 1;
  }
}

int create_secondary_index(Table* table, char* column_name) {
  // secondary indexes will always exist on the primary copy of the
  // data.
  Column* column = find_column(table, column_name);
  if (column == NULL) {
    log_err("can't find column with name %s\n", column_name);
    return 0;
  }
  if (column->btree != NULL) {
    log_err("column %s already has an index\n", column_name);
    return 0;
  }

  if (column->length == 0)
    column->btree = btree_init();
  else
    column->btree = btree_bulk_load(column->data, column->length);

  log_info("created secondary index for %s\n", column_name);

  return 1;
}

void relational_insert(Table* table, int* values) {
  if (table->row_count >= table->row_capacity) {
    table->row_capacity *= 2;
    for (size_t i = 0; i < table->column_count; i++) {
      Column* col = table->columns[i];
      int* bigger_data = malloc(table->row_capacity * sizeof(int));
      memcpy(bigger_data, col->data, col->length * sizeof(int));
      free(col->data);
      col->data = bigger_data;
    }
  }

  if (table->is_sorted) {
    Column* lcol = table->columns[table->leader];
    if (lcol->length == 0 || (lcol->sorted_until == lcol->length && values[table->leader] >= lcol->data[lcol->length-1])) {
      lcol->sorted_until++;
    }
    /*size_t index = index_to_insert(values[table->leader], lcol->data, lcol->length);*/
    /*if (index == lcol->length && lcol->sorted_until == lcol->length)*/
    /*for (size_t i = 0; i < table->column_count; i++) {*/
      /*Column* col = table->columns[i];*/
      /*for (size_t j = table->row_count; j > index; j--)*/
        /*col->data[j] = col->data[j-1];*/
      /*col->data[index] = values[i];*/
      /*col->length++;*/
      /*if (col->btree != NULL) {*/
        /*if (index != table->row_count) {*/
          /*log_info("unnecessarily rebuilding btree\n");*/
          /*rebuild_column_index(col);*/
        /*} else {*/
          /*btree_insert(values[i], index, col->btree);*/
        /*}*/
      /*}*/
    /*}*/
  }
  /*else {*/
  for (size_t i = 0; i < table->column_count; i++) {
    Column* col = table->columns[i];
    col->data[table->row_count] = values[i];
    col->length++;
    if (col->btree != NULL)
      btree_insert(values[i], table->row_count, col->btree);
  }
  /*}*/

  table->row_count++;

  for (size_t i = 0; i < table->replica_count; i++)
    relational_insert(table->replicas[i], values);
}

Column* create_column(Table* table, char* column_name) {
  if (strcmp(column_name, "replicas") == 0) {
    log_err("cannot create column with reserved name 'replicas'\n");
    return NULL;
  }

  if (table->column_count >= table->column_capacity) {
    table->column_capacity *= 2;
    Column** more_columns = malloc(table->column_capacity * sizeof(Column*));
    for (size_t i = 0; i < table->column_count; i++)
      more_columns[i] = table->columns[i];
    free(table->columns);
    table->columns = more_columns;
  }

  Column* column = init_column(column_name, table->row_capacity);
  table->columns[table->column_count] = column;
  table->column_count++;

  for (size_t i = 0; i < table->replica_count; i++)
    create_column(table->replicas[i], column_name);

  return column;
}

int find_column_index(Table* table, char* column_name) {
  for (size_t i = 0; i < table->column_count; i++)
    if (!strcmp(table->columns[i]->name, column_name))
      return i;
  return -1;
}

Column* find_column(Table* table, char* column_name) {
  for (size_t i = 0; i < table->column_count; i++)
    if (!strcmp(table->columns[i]->name, column_name))
      return table->columns[i];
  return NULL;
}
