#include "query_batch.h"

QueryBatch* init_query_batch() {
  QueryBatch* qb = malloc(sizeof(QueryBatch));
  qb->query_count = 0;
  return qb;
}

void free_query_batch(QueryBatch* qb) {
  free(qb);
}

void batch_enqueue(QueryBatch* qb, DbOperator* dbo) {
  if (qb->query_count > MAX_BATCH_QUERIES)
    log_err("enqueuing too many batch queries\n");

  qb->query_queue[qb->query_count] = dbo;
  qb->query_count++;
}

#define DO_SHARED_SCAN_IF_NECESSARY() do {\
  if (scan != NULL) {\
    do_threaded_scan(scan);\
    free_shared_scan(scan);\
    scan = NULL;\
  }\
} while(0);

void batch_execute(QueryBatch* qb) {
  SharedScan* scan = NULL;
  for (size_t i = 0; i < qb->query_count; i++) {
    DbOperator* dbo = qb->query_queue[i];
    Column* col = column_for_select(dbo);
    // If we're part of an ongoing unbroken chain of selects over the
    // same column, add ourselves to the shared scan object
    if (col != NULL && scan != NULL && scan->column == col) {
      add_shared_scanner(scan, dbo);
    // Otherwise,
    } else {
      // Execute the shared scan if one was pending
      DO_SHARED_SCAN_IF_NECESSARY();
      // Then either initialize a new shared scan
      if (col != NULL) {
        scan = init_shared_scan(col, SHARED_SCAN_VECTOR_SIZE);
        add_shared_scanner(scan, dbo);
      } else {
        // Or just execute the operator
        execute_db_operator(dbo);
      }
    }
  }
  // Do any final shared scan that's still pending
  DO_SHARED_SCAN_IF_NECESSARY();
}
