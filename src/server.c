/** server.c
 * CS165 Fall 2015
 *
 * This file provides a basic unix socket implementation for a server
 * used in an interactive client-server database.
 * The server should allow for multiple concurrent connections from clients.
 * Each client should be able to send messages containing queries to the
 * server.  When the server receives a message, it must:
 * 1. Respond with a status based on the query (OK, UNKNOWN_QUERY, etc.)
 * 2. Process any appropriate queries, if applicable.
 * 3. Return the query response to the client.
 *
 * For more information on unix sockets, refer to:
 * http://beej.us/guide/bgipc/output/html/multipage/unixsock.html
 **/
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>

#include "common.h"
#include "parse.h"
#include "cs165_api.h"
#include "message.h"
#include "utils.h"
#include "query_batch.h"

#define DEFAULT_QUERY_BUFFER_SIZE 1024

ColumnStore* store;

/**
 * handle_client(client_socket)
 * This is the execution routine after a client has connected.
 * It will continually listen for messages from the client and execute queries.
 **/
int handle_client(int client_socket, ClientContext* client_context) {
    int done = 0;
    int length = 0;

    log_info("Connected to socket: %d.\n", client_socket);

    // Create two messages, one from which to read and one from which to receive
    message send_message;
    message recv_message;

    // create the client context here

    // Continually receive messages from client and execute queries.
    // 1. Parse the command
    // 2. Handle request if appropriate
    // 3. Send status of the received message (OK, UNKNOWN_QUERY, etc)
    // 4. Send response of request.
    do {
        length = recv(client_socket, &recv_message, sizeof(message), 0);
        if (length < 0) {
            log_info("Client connection closed!\n");
            return 1;
        } else if (length == 0) {
            return 0;
        }

        if (!done) {
            char recv_buffer[recv_message.length];
            length = recv(client_socket, recv_buffer, recv_message.length,0);
            recv_message.payload = recv_buffer;
            recv_message.payload[recv_message.length] = '\0';
            recv_message.payload = trim_whitespace(recv_message.payload);

            if (strcmp(recv_message.payload, "shutdown") == 0) {
              log_info("received shutdown command\n");
              done = 1;
            }

            // 1. Parse command
            log_info("parsing command %s\n", recv_message.payload);
            DbOperator* dbo = NULL;

            if (strcmp(recv_message.payload, "batch_queries()") == 0) {
              log_info("initializing batch queries\n");
              client_context->current_batch = init_query_batch();
            } else if (strcmp(recv_message.payload, "batch_execute()") == 0) {
              log_info("batch executing\n");
              batch_execute(client_context->current_batch);
              client_context->current_batch = NULL;
            } else {
              dbo = parse_command(
                  recv_message.payload, store, &send_message, client_socket, client_context);
            }

            // 2. Handle request
            DbOperationResult dbres;
            if (dbo != NULL && client_context->current_batch != NULL) {
              log_info("enqueueing a batch operation\n");
              batch_enqueue(client_context->current_batch, dbo);
              dbres = execute_db_operator(NULL);
            } else {
              dbres = execute_db_operator(dbo);
            }
            send_message.length = (int)dbres.message_length;

            // 3. Send status of the received message (OK, UNKNOWN_QUERY, etc)
            if (send(client_socket, &(send_message), sizeof(message), 0) == -1) {
                log_err("Failed to send message.");
                return 1;
            }

            // 4. Send response of request
            if (send(client_socket, dbres.message, dbres.message_length, 0) == -1) {
                log_err("Failed to send message.");
                return 1;
            }

            // 5. free the result string
            free(dbres.message);
        }
    } while (!done);

    log_info("Connection closed at socket %d!\n", client_socket);
    close(client_socket);
    return done;
}

/**
 * setup_server()
 *
 * This sets up the connection on the server side using unix sockets.
 * Returns a valid server socket fd on success, else -1 on failure.
 **/
int setup_server() {
    int server_socket;
    size_t len;
    struct sockaddr_un local;

    log_info("Attempting to setup server...\n");

    if ((server_socket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        log_err("L%d: Failed to create socket.\n", __LINE__);
        return -1;
    }

    local.sun_family = AF_UNIX;
    strncpy(local.sun_path, SOCK_PATH, strlen(SOCK_PATH) + 1);
    unlink(local.sun_path);

    len = strlen(local.sun_path) + sizeof(local.sun_family) + 1;
    if (bind(server_socket, (struct sockaddr *)&local, len) == -1) {
        log_err("L%d: Socket failed to bind.\n", __LINE__);
        return -1;
    }

    if (listen(server_socket, 5) == -1) {
        log_err("L%d: Failed to listen on socket.\n", __LINE__);
        return -1;
    }

    return server_socket;
}

int main(void)
{
    int server_socket = setup_server();
    if (server_socket < 0) {
        exit(1);
    }

    struct sockaddr_un remote;
    socklen_t t = sizeof(remote);
    int client_socket = 0;

    ClientContext* client_context = init_client_context();
    store = load_column_store("data");

    int done = 0;
    while (!done) {
      log_info("Waiting for a connection %d ...\n", server_socket);
      if ((client_socket = accept(server_socket, (struct sockaddr *)&remote, &t)) == -1) {
        log_err("L%d: Failed to accept a new connection.\n", __LINE__);
        done = 1;
      } else {
        done = handle_client(client_socket, client_context);
      }
    }

    sync_column_store(store);
    free_client_context(client_context);
    free_column_store(store);

    return 0;
}
